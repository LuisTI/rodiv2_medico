<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Cliente_General extends CI_Controller
{

  function __construct(){
    parent::__construct();
  }

  function index(){
    if ($this->session->userdata('logueado') && $this->session->userdata('tipo') == 1) {
      $id_cliente = $this->uri->segment(3);
      $data['permisos'] = $this->usuario_model->getPermisos($this->session->userdata('id'));
			$data['permisos_subclientes'] = $this->usuario_model->getPermisosSubclientes($this->session->userdata('id'));
      if ($data['permisos']) {
        foreach ($data['permisos'] as $p) {
          if ($p->id_cliente == $id_cliente) {
            $data['cliente'] = $p->nombreCliente;
          }
        }
      }
      $data['accesos'] = $this->usuario_model->getModulos($this->session->userdata('idrol'));
      foreach ($data['accesos'] as $acceso) {
        $items[] = $acceso->id_operaciones;
      }
      $data['acceso'] = $items;
      $info['estados'] = $this->funciones_model->getEstados();
      $info['civiles'] = $this->funciones_model->getEstadosCiviles();
      $data['baterias'] = $this->funciones_model->getBateriasPsicometricas();
      $info['subclientes'] = $this->cliente_general_model->getSubclientes($id_cliente);
      $data['personales'] = $this->funciones_model->getTiposPersona();
      $info['puestos'] = $this->funciones_model->getPuestos();
      $info['grados'] = $this->funciones_model->getGradosEstudio();
      $info['drogas'] = $this->funciones_model->getPaquetesAntidoping();
      $data['parentescos'] = $this->funciones_model->getParentescos();
      $data['escolaridades'] = $this->funciones_model->getEscolaridades();
      $info['zonas'] = $this->funciones_model->getNivelesZona();
      $info['viviendas'] = $this->funciones_model->getTiposVivienda();
      $info['condiciones'] = $this->funciones_model->getTiposCondiciones();
      $data['examenes_doping'] = $this->funciones_model->getExamenDoping($id_cliente);
      $info['studies'] = $this->funciones_model->getTiposEstudios();
      $info['cands'] = $this->cliente_general_model->getCandidatosCliente($id_cliente);
      $info['usuarios'] = $this->funciones_model->getUsuariosParaAsignacion();
      $info['tipos_docs'] = $this->funciones_model->getTiposDocumentos();

      $vista['modals'] = $this->load->view('modals/mdl_clientes_general', $info, TRUE);

      //$cliente = $this->cliente_general_model->getDatosCliente($id_cliente);
      $this->load
      ->view('adminpanel/header', $data)
      ->view('adminpanel/scripts')
      ->view('analista/candidatos_espanol_index', $vista)
      ->view('adminpanel/footer');
    }
  }
  /*----------------------------------------*/
  /*  Consultas 
  /*----------------------------------------*/
    function getCandidatos(){
      $id_cliente = $_GET['id'];
      $cand['recordsTotal'] = $this->cliente_general_model->getTotal($id_cliente, $this->session->userdata('idrol'), $this->session->userdata('id'));
      $cand['recordsFiltered'] = $this->cliente_general_model->getTotal($id_cliente, $this->session->userdata('idrol'), $this->session->userdata('id'));
      $cand['data'] = $this->cliente_general_model->getCandidatos($id_cliente, $this->session->userdata('idrol'), $this->session->userdata('id'));
      $this->output->set_output( json_encode( $cand ) );
    }
    function getHistorialAcademico(){
      $id_candidato = $this->input->post('id_candidato');
      $res = array();
      $dato = $this->candidato_model->getEstudiosCandidato($id_candidato);
      if($dato != null){
        $res = array(
          'primaria_periodo' => $dato->primaria_periodo,
          'primaria_escuela' => $dato->primaria_escuela,
          'primaria_ciudad' => $dato->primaria_ciudad,
          'primaria_certificado' => $dato->primaria_certificado,
          'primaria_promedio' => $dato->primaria_promedio,
          'secundaria_periodo' => $dato->secundaria_periodo,
          'secundaria_escuela' => $dato->secundaria_escuela,
          'secundaria_ciudad' => $dato->secundaria_ciudad,
          'secundaria_certificado' => $dato->secundaria_certificado,
          'secundaria_promedio' => $dato->secundaria_promedio,
          'preparatoria_periodo' => $dato->preparatoria_periodo,
          'preparatoria_escuela' => $dato->preparatoria_escuela,
          'preparatoria_ciudad' => $dato->preparatoria_ciudad,
          'preparatoria_certificado' => $dato->preparatoria_certificado,
          'preparatoria_promedio' => $dato->preparatoria_promedio,
          'licenciatura_periodo' => $dato->licenciatura_periodo,
          'licenciatura_escuela' => $dato->licenciatura_escuela,
          'licenciatura_ciudad' => $dato->licenciatura_ciudad,
          'licenciatura_certificado' => $dato->licenciatura_certificado,
          'licenciatura_promedio' => $dato->licenciatura_promedio,
          'actual_periodo' => $dato->actual_periodo,
          'actual_escuela' => $dato->actual_escuela,
          'actual_ciudad' => $dato->actual_ciudad,
          'actual_certificado' => $dato->actual_certificado,
          'actual_promedio' => $dato->actual_promedio,
          'cedula_profesional' => $dato->cedula_profesional,
          'otros_certificados' => $dato->otros_certificados,
          'comentarios' => $dato->comentarios,
          'carrera_inactivo' => $dato->carrera_inactivo
        );
      }
      echo json_encode($res);
    }
    function getAntecedentesSociales(){
      $id_candidato = $this->input->post('id_candidato');
      $res = array();
      $dato = $this->candidato_model->getAntecedentesSociales($id_candidato);
      if($dato != null){
        $res = array(
          'sindical' => $dato->sindical,
          'sindical_nombre' => $dato->sindical_nombre,
          'sindical_cargo' => $dato->sindical_cargo,
          'partido' => $dato->partido,
          'partido_nombre' => $dato->partido_nombre,
          'partido_cargo' => $dato->partido_cargo,
          'club' => $dato->club,
          'deporte' => $dato->deporte,
          'religion' => $dato->religion,
          'religion_frecuencia' => $dato->religion_frecuencia,
          'bebidas' => $dato->bebidas,
          'bebidas_frecuencia' => $dato->bebidas_frecuencia,
          'fumar' => $dato->fumar,
          'fumar_frecuencia' => $dato->fumar_frecuencia,
          'cirugia' => $dato->cirugia,
          'enfermedades' => $dato->enfermedades,
          'corto_plazo' => $dato->corto_plazo,
          'mediano_plazo' => $dato->mediano_plazo
        );
      }
      echo json_encode($res);
    }
    function getReferenciasPersonales(){
      $id_candidato = $this->input->post('id_candidato');
      $salida = "";
      $data['refs'] = $this->candidato_model->getReferenciasPersonales($id_candidato);
      if($data['refs']){
          foreach($data['refs'] as $ref){
            $salida .= $ref->nombre."@@";
            $salida .= $ref->telefono."@@";
            $salida .= $ref->tiempo_conocerlo."@@";
            $salida .= $ref->id."@@";
            $salida .= $ref->recomienda."@@";
            $salida .= $ref->comentario."###";
          }
          echo $salida;
      }
      else{
          echo $salida = 0;
      }
    }
    function getAntecedentesLaborales(){
      $id_candidato = $this->input->post('id_candidato');
      $salida = "";
      $data['referencias'] = $this->candidato_model->getAntecedentesLaborales($id_candidato);
      if($data['referencias']){
          foreach($data['referencias'] as $ref){
              $salida .= $ref->empresa."@@";
              $salida .= $ref->area."@@";
              $salida .= $ref->calle."@@";
              $salida .= $ref->colonia."@@";
              $salida .= $ref->cp."@@";
              $salida .= $ref->telefono."@@";
              $salida .= $ref->tipo_empresa."@@";
              $salida .= $ref->puesto."@@";
              $salida .= $ref->periodo."@@";
              $salida .= $ref->jefe_nombre."@@";
              $salida .= $ref->jefe_puesto."@@";
              $salida .= $ref->sueldo_inicial."@@";
              $salida .= $ref->sueldo_final."@@";
              $salida .= $ref->actividades."@@";
              $salida .= $ref->causa_separacion."@@";
              $salida .= $ref->trabajo_calidad."@@";
              $salida .= $ref->trabajo_puntualidad."@@";
              $salida .= $ref->trabajo_honesto."@@";
              $salida .= $ref->trabajo_responsabilidad."@@";
              $salida .= $ref->trabajo_adaptacion."@@";
              $salida .= $ref->trabajo_actitud_jefes."@@";
              $salida .= $ref->trabajo_actitud_companeros."@@";
              $salida .= $ref->comentarios."@@";
              $salida .= $ref->id."###";
          }
          
      }
      echo $salida;
    }
    function getInvestigacionLegal(){
      $id_candidato = $this->input->post('id_candidato');
      $res = array();
      $dato = $this->candidato_model->getVerificacionLegal($id_candidato);
      if($dato != null){
        $res = array(
          'penal' => $dato->penal,
          'penal_notas' => $dato->penal_notas,
          'civil' => $dato->civil,
          'civil_notas' => $dato->civil_notas,
          'laboral' => $dato->laboral,
          'laboral_notas' => $dato->laboral_notas,
        );
      }
      echo json_encode($res);
    }
    function getTrabajosNoMencionados(){
      $id_candidato = $this->input->post('id_candidato');
      $res = array();
      $dato = $this->candidato_model->getTrabajosNoMencionados($id_candidato);
      if($dato != null){
        $res = array(
          'no_mencionados' => $dato->no_mencionados,
          'resultado_no_mencionados' => $dato->resultado_no_mencionados,
          'notas_no_mencionados' => $dato->notas_no_mencionados
        );
      }
      echo json_encode($res);
    }
    function getGrupoFamiliar(){
      $id_candidato = $_POST['id_candidato'];
      $salida = "";
      $cont = 1;
      $data['parentescos'] = $this->funciones_model->getParentescos();
      $data['civiles'] = $this->funciones_model->getEstadosCiviles();
      $data['escolaridades'] = $this->funciones_model->getEscolaridades();
      $data['familia'] = $this->candidato_model->getGrupoFamiliar($id_candidato);
      if($data['familia']){
        foreach($data['familia'] as $f){
            $salida .= '<div class="alert alert-secondary text-center">
                            <p><b>Persona #'.$cont.'</b></p>
                        </div>
                        <form id=d_familiar'.$cont.'>
                        <div class="row">
                            <div class="col-md-3">
                                <label>Nombre completo *</label>
                                <input type="text" class="form-control es_persona visita_p_obligado_'.$cont.'" name="p'.$cont.'_nombre" id="p'.$cont.'_nombre" value="'.$f->nombre.'">
                                <br>
                            </div>
                            <div class="col-md-3">
                                <label>Parentesco *</label>
                                <select name="p'.$cont.'_parentesco" id="p'.$cont.'_parentesco" class="form-control es_persona visita_p_obligado_'.$cont.'">';
                                foreach($data['parentescos'] as $parent){
                                  $salida .= '<option value="'.$parent->id.'">'.$parent->nombre.'</option>';
                                }
                                
                    $salida .= '</select> 
                                <br>
                            </div>
                            <div class="col-md-3">
                                <label>Edad *</label>
                                <input type="text" class="form-control solo_numeros es_persona visita_p_obligado_'.$cont.'" name="p'.$cont.'_edad" id="p'.$cont.'_edad" maxlength="2" value="'.$f->edad.'">
                                <br>
                            </div>
                            <div class="col-md-3">
                                <label>Estado civil *</label>
                                <select name="p'.$cont.'_civil" id="p'.$cont.'_civil" class="form-control es_persona visita_p_obligado_'.$cont.'"> ';
                                foreach($data['civiles'] as $civ){
                                  $salida .= '<option value="'.$civ->id.'">'.$civ->nombre.'</option>';
                                }
                    $salida .= '</select>
                                <br>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <label>Escolaridad *</label>
                                <select name="p'.$cont.'_escolaridad" id="p'.$cont.'_escolaridad" class="form-control es_persona visita_p_obligado_'.$cont.'"> ';
                                foreach($data['escolaridades'] as $escolar){
                                  $salida .= '<option value="'.$escolar->id.'">'.$escolar->nombre.'</option>';
                                }
                    $salida .= '</select>
                                <br>
                            </div>
                            <div class="col-md-3">
                                <label>¿Vive con usted? *</label>
                                <select name="p'.$cont.'_vive" id="p'.$cont.'_vive" class="form-control es_persona visita_p_obligado_'.$cont.'">
                                  <option value="0">No</option>
                                  <option value="1">Sí</option>
                                </select>
                                <br>
                            </div>
                            <div class="col-md-3">
                                <label>Empresa *</label>
                                <input type="text" class="form-control es_persona visita_p_obligado_'.$cont.'" name="p'.$cont.'_empresa" id="p'.$cont.'_empresa" value="'.$f->empresa.'">
                                <br>
                            </div>
                            <div class="col-md-3">
                                <label>Puesto *</label>
                                <input type="text" class="form-control es_persona visita_p_obligado_'.$cont.'" name="p'.$cont.'_puesto" id="p'.$cont.'_puesto" value="'.$f->puesto.'">
                                <br>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <label>Antigüedad *</label>
                                <input type="text" class="form-control es_persona visita_p_obligado_'.$cont.'" name="p'.$cont.'_antiguedad" id="p'.$cont.'_antiguedad" value="'.$f->antiguedad.'">
                                <br>
                            </div>
                            <div class="col-md-3">
                                <label>Sueldo *</label>
                                <input type="text" class="form-control solo_numeros es_persona visita_p_obligado_'.$cont.'" name="p'.$cont.'_sueldo" id="p'.$cont.'_sueldo" maxlength="8" value="'.$f->sueldo.'">
                                <br>
                            </div>
                            <div class="col-md-3">
                                <label>Aportación *</label>
                                <input type="text" class="form-control solo_numeros es_persona visita_p_obligado_'.$cont.'" name="p'.$cont.'_aportacion" id="p'.$cont.'_aportacion" maxlength="8" value="'.$f->monto_aporta.'">
                                <br>
                            </div>
                            <div class="col-md-3">
                                <label>Muebles e inmuebles *</label>
                                <input type="text" class="form-control es_persona visita_p_obligado_'.$cont.'" name="p'.$cont.'_muebles" id="p'.$cont.'_muebles" value="'.$f->muebles.'">
                                <br>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <label>Adeudo *</label>
                                <select name="p'.$cont.'_adeudo" id="p'.$cont.'_adeudo" class="form-control"">
                                    <option value="0">No</option>
                                    <option value="1">Sí</option>
                                </select>
                                <br><br><br>
                            </div>
                        </div>
                        </form>
                        <div class="row">
                            <div class="col-md-5 offset-4">
                                <a href="javascript:void(0)" class="btn btn-success" onclick="guardarIntegranteFamiliar('.$f->id.','.$cont.','.$id_candidato.')">Actualizar Persona #'.$cont.'</a>
                                <br><br><br>
                            </div>
                        </div>
                        <div id="familiar_msj_error'.$cont.'" class="alert alert-danger hidden"></div>';
                        $salida .= 
                        '<script>
                        $("#p'.$cont.'_parentesco").val('.$f->id_tipo_parentesco.');
                        $("#p'.$cont.'_civil").val('.$f->id_estado_civil.');
                        $("#p'.$cont.'_escolaridad").val('.$f->id_grado_estudio.');
                        $("#p'.$cont.'_vive").val('.$f->misma_vivienda.');
                        $("#p'.$cont.'_adeudo").val('.$f->adeudo.');
                        </script>';
                        $cont++;
        }
        $salida .= '<hr>';
        $candidato = $this->candidato_model->getInfoCandidatoEspecifico($id_candidato);
        $salida .= '<form id="d_familiar_candidato"> 
                      <div class="row">
                        <div class="col-md-6">
                            <label>Muebles e inmuebles del candidato *</label>
                            <input type="text" class="form-control extra_candidato" name="candidato_muebles" id="candidato_muebles" value="'.$candidato->muebles.'">
                            <br>
                        </div>
                        <div class="col-md-6">
                            <label>Adeudo *</label>
                            <select name="candidato_adeudo" id="candidato_adeudo" class="form-control"">
                              <option value="0">No</option>
                              <option value="1">Sí</option>
                            </select>
                            <br>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-6">
                          <label>Ingresos del candidato *</label>
                          <input type="text" class="form-control extra_candidato" name="candidato_ingresos" id="candidato_ingresos" value="'.$candidato->ingresos.'">
                          <br>
                        </div>
                      </div>
                      <div class="row">
                          <div class="col-md-12">
                              <label>Notas *</label>
                              <textarea class="form-control extra_candidato" name="notas" id="notas" rows="2">'.$candidato->comentario.'</textarea><br><br>
                              <br>
                          </div>
                      </div>
                      </form>
                      <div class="row">
                          <div class="col-md-7 offset-3">
                              <a href="javascript:void(0)" class="btn btn-primary" onclick="guardarExtrasCandidato('.$id_candidato.')">Actualizar mobiliaria y notas del candidato</a>
                              <br><br><br>
                          </div>
                      </div>
                      <div id="mobiliario_msj_error" class="alert alert-danger hidden"></div>';
                      $salida .= 
                      '<script>
                      $("#candidato_adeudo").val('.$candidato->adeudo_muebles.');
                      </script>';
      }
      else{
        $candidato = $this->candidato_model->getInfoCandidatoEspecifico($id_candidato);
        $salida .= '<form id="d_familiar_candidato">
                      <div class="row">
                        <div class="col-md-6">
                          <label>Muebles e inmuebles del candidato *</label>
                          <input type="text" class="form-control extra_candidato" name="candidato_muebles" id="candidato_muebles" value="'.$candidato->muebles.'">
                          <br>
                        </div>
                        <div class="col-md-6">
                          <label>Adeudo *</label>
                          <select name="candidato_adeudo" id="candidato_adeudo" class="form-control"">
                            <option value="0">No</option>
                            <option value="1">Sí</option>
                          </select>
                          <br>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-6">
                          <label>Ingresos del candidato *</label>
                          <input type="text" class="form-control extra_candidato" name="candidato_ingresos" id="candidato_ingresos" value="'.$candidato->ingresos.'">
                          <br>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-12">
                            <label>Notas *</label>
                            <textarea class="form-control extra_candidato" name="notas" id="notas" rows="2">'.$candidato->comentario.'</textarea><br><br>
                            <br>
                        </div>
                      </div>
                    </form>
                    <div class="row">
                      <div class="col-md-7 offset-3">
                          <a href="javascript:void(0)" class="btn btn-success" onclick="guardarExtrasCandidato('.$id_candidato.')">Actualizar mobiliaria y notas del candidato</a>
                          <br><br><br>
                      </div>
                    </div>
                    <div id="mobiliario_msj_error" class="alert alert-danger hidden"></div>';
                    $salida .= 
                    '<script>
                    $("#candidato_adeudo").val('.$candidato->adeudo_muebles.');
                    </script>';
      }
      echo $salida;
    }
    function getComentariosRefPersonales(){
      $id_candidato = $this->input->post('id_candidato');
      $salida = "";
      $data['ref'] = $this->candidato_model->getComentariosRefPersonales($id_candidato);
      if($data['ref']){
          foreach($data['ref'] as $ref){
              $salida .= $ref->comentario.", ";
          }
          $res = trim($salida, ", ");
          echo $res;
      }
      else{
          echo $salida;
      }
    }
    function countAntecedentesLaborales(){
      $id_candidato = $this->input->post('id_candidato');
      $numero = $this->candidato_model->countAntecedentesLaborales($id_candidato);
      echo $numero;
    }
    function getComentariosRefVecinales(){
      $id_candidato = $this->input->post('id_candidato');
      $salida = "";
      $data['ref'] = $this->candidato_model->getComentariosRefVecinales($id_candidato);
      if($data['ref']){
          foreach($data['ref'] as $ref){
              $salida .= $ref->concepto_candidato.", ";
          }
          $res = trim($salida, ", ");
          echo $res;
      }
      else{
          echo $salida;
      }
    }

  /*----------------------------------------*/
  /*  Proceso 
  /*----------------------------------------*/
    function registrar(){
      $this->form_validation->set_rules('nombre', 'Nombre', 'required|trim');
      $this->form_validation->set_rules('paterno', 'Primer apellido', 'required|trim');
      $this->form_validation->set_rules('materno', 'Segundo apellido', 'trim');
      $this->form_validation->set_rules('celular', 'Tel. Celular', 'trim|max_length[16]');
      $this->form_validation->set_rules('correo', 'Correo', 'trim|valid_email');
      $this->form_validation->set_rules('fijo', 'Tel. Casa', 'trim|max_length[16]');
      $this->form_validation->set_rules('puesto', 'Puesto', 'required');
      $this->form_validation->set_rules('socio', 'Socioeconomico', 'required');
      $this->form_validation->set_rules('antidoping', 'Antidoping', 'required');
      $this->form_validation->set_rules('psicometrico', 'Psicométrico', 'required');
      $this->form_validation->set_rules('medico', 'Médico', 'required');
      $this->form_validation->set_rules('proceso', 'Tipo de ESE', 'required');

      $this->form_validation->set_message('required', 'El campo {field} es obligatorio');
      $this->form_validation->set_message('max_length', 'El campo {field} debe tener máximo {param} carácteres');
      $this->form_validation->set_message('valid_email', 'El campo {field} debe ser un correo válido');

      $msj = array();
      if ($this->form_validation->run() == FALSE) {
        $msj = array(
          'codigo' => 0,
          'msg' => validation_errors()
        );
      } else {
        date_default_timezone_set('America/Mexico_City');
        $date = date('Y-m-d H:i:s');
        $nombre = $this->input->post('nombre');
        $paterno = $this->input->post('paterno');
        $materno = $this->input->post('materno');
        $proceso = $this->input->post('proceso');
        $correo = $this->input->post('correo');
        $celular = $this->input->post('celular');
        $fijo = $this->input->post('fijo');
        $id_cliente = $this->input->post('id_cliente');
        if ($this->session->userdata('subcliente') !== null) {
          $id_subcliente = $this->session->userdata('subcliente');
        } else {
          $id_subcliente = $this->input->post('subcliente');
        }
        $id_puesto = $this->input->post('puesto');
        $socio = $this->input->post('socio');
        $medico = $this->input->post('medico');
        $antidoping = $this->input->post('antidoping');
        $psicometrico = $this->input->post('psicometrico');
        $examen = $this->input->post('examen');
        $otro_requerimiento = $this->input->post('otro');
        $existeCandidato = $this->cliente_general_model->checkCandidatoRepetidoSubcliente(strtoupper($nombre), strtoupper($paterno), strtoupper($materno), $id_cliente, $id_subcliente);
        if ($existeCandidato > 0) {
          $msj = array(
            'codigo' => 2,
            'msg' => 'El candidato ya fue registrado previamente'
          );
        } else {
          $usuario = $this->input->post('usuario');
          switch ($usuario) {
            case 1:
              $tipo_usuario = "id_usuario";
              break;
            case 2:
              $tipo_usuario = "id_usuario_cliente";
              break;
            case 3:
              $tipo_usuario = "id_usuario_subcliente";
              break;
          }
          $id_usuario = $this->session->userdata('id');

          if ($usuario == 2 || $usuario == 3) {
            if ($id_subcliente != 180) {
              $configuracion = $this->funciones_model->getConfiguraciones();
              $data = array(
                'creacion' => $date,
                'edicion' => $date,
                $tipo_usuario => $id_usuario,
                'id_usuario' => $configuracion->usuario_lider_espanol,
                'id_cliente' => $id_cliente,
                'id_subcliente' => $id_subcliente,
                'id_tipo_proceso' => $proceso,
                'id_puesto' => $id_puesto,
                'fecha_alta' => $date,
                'nombre' => strtoupper($nombre),
                'paterno' => strtoupper($paterno),
                'materno' => strtoupper($materno),
                'correo' => $correo,
                'celular' => $celular,
                'telefono_casa' => $fijo
              );
            }
            if ($id_subcliente == 180) {
              $configuracion = $this->funciones_model->getConfiguraciones();
              $data = array(
                'creacion' => $date,
                'edicion' => $date,
                $tipo_usuario => $id_usuario,
                'id_usuario' => $configuracion->usuario_lider_ingles,
                'id_cliente' => $id_cliente,
                'id_subcliente' => $id_subcliente,
                'id_tipo_proceso' => $proceso,
                'id_puesto' => $id_puesto,
                'fecha_alta' => $date,
                'nombre' => strtoupper($nombre),
                'paterno' => strtoupper($paterno),
                'materno' => strtoupper($materno),
                'correo' => $correo,
                'celular' => $celular,
                'telefono_casa' => $fijo
              );
            }
          } else {
            $data = array(
              'creacion' => $date,
              'edicion' => $date,
              $tipo_usuario => $id_usuario,
              'id_cliente' => $id_cliente,
              'id_subcliente' => $id_subcliente,
              'id_tipo_proceso' => $proceso,
              'id_puesto' => $id_puesto,
              'fecha_alta' => $date,
              'nombre' => strtoupper($nombre),
              'paterno' => strtoupper($paterno),
              'materno' => strtoupper($materno),
              'correo' => $correo,
              'celular' => $celular,
              'telefono_casa' => $fijo
            );
          }
          $id_candidato = $this->candidato_model->registrarRetornaCandidato($data);

          //Subida y Registro de CV
          if ($this->input->post('hay_cvs') == 1) {
            $countfiles = count($_FILES['cvs']['name']);

            for ($i = 0; $i < $countfiles; $i++) {
              if (!empty($_FILES['cvs']['name'][$i])) {
                // Define new $_FILES array - $_FILES['file']
                $_FILES['file']['name'] = $_FILES['cvs']['name'][$i];
                $_FILES['file']['type'] = $_FILES['cvs']['type'][$i];
                $_FILES['file']['tmp_name'] = $_FILES['cvs']['tmp_name'][$i];
                $_FILES['file']['error'] = $_FILES['cvs']['error'][$i];
                $_FILES['file']['size'] = $_FILES['cvs']['size'][$i];
                $temp = str_replace(' ', '', $_FILES['cvs']['name'][$i]);
                $extension = pathinfo($_FILES['cvs']['name'][$i], PATHINFO_EXTENSION);

                $nombre_cv = $id_candidato . "_CV_" . $i . '.' . $extension;
                // Set preference
                $config['upload_path'] = './_docs/';
                $config['allowed_types'] = 'pdf|jpeg|jpg|png';
                //$config['max_size'] = '15000'; // max_size in kb
                $config['file_name'] = $nombre_cv;
                //Load upload library
                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                // File upload
                if ($this->upload->do_upload('file')) {
                  $data = $this->upload->data();
                  //$salida = 1; 
                }
                $documento = array(
                  'creacion' => $date,
                  'edicion' => $date,
                  'id_candidato' => $id_candidato,
                  'id_tipo_documento' => 16,
                  'archivo' => $nombre_cv
                );
                $this->candidato_model->registrarDocumento($documento);
              }
            }
          }

          if ($socio == 1) {
            if ($antidoping == 1) {
              if ($examen == 0) {
                $msj = array(
                  'codigo' => 3,
                  'msg' => 'El campo Examen Antidoping es obligatorio'
                );
              } else {
                $drogas = $examen;
                $tipo_antidoping = 1;
                $pruebas = array(
                  'creacion' => $date,
                  'edicion' => $date,
                  $tipo_usuario => $id_usuario,
                  'id_candidato' => $id_candidato,
                  'id_cliente' => $id_cliente,
                  'socioeconomico' => $socio,
                  'tipo_antidoping' => $tipo_antidoping,
                  'antidoping' => $drogas,
                  'tipo_psicometrico' => $psicometrico,
                  'psicometrico' => $psicometrico,
                  'medico' => $medico,
                  'buro_credito' => 0,
                  'sociolaboral' => 0,
                  'otro_requerimiento' => $otro_requerimiento
                );
                $this->candidato_model->crearPruebas($pruebas);

                if ($id_subcliente != 180) {
                  $visita = array(
                    'creacion' => $date,
                    'edicion' => $date,
                    $tipo_usuario => $id_usuario,
                    'id_cliente' => $id_cliente,
                    'id_subcliente' => $id_subcliente,
                    'id_candidato' => $id_candidato,
                    'id_tipo_formulario' => 4

                  );
                  $this->candidato_model->crearVisita($visita);
                }
                if ($usuario == 2 || $usuario == 3) {
                  $from = $this->config->item('smtp_user');
                  $info_cliente = $this->cliente_general_model->getDatosCliente($this->input->post('id_cliente'));
                  $to = "bjimenez@rodi.com.mx";
                  $subject = " Nuevo candidato en la plataforma del cliente " . $info_cliente->nombre;
                  $message = "Se ha agregado a " . strtoupper($this->input->post('nombre')) . " " . strtoupper($this->input->post('paterno')) . " " . strtoupper($this->input->post('materno')) . " del cliente " . $info_cliente->nombre . " en la plataforma";
                  $this->load->library('phpmailer_lib');
                  $mail = $this->phpmailer_lib->load();
                  $mail->isSMTP();
                  $mail->Host     = 'rodi.com.mx';
                  $mail->SMTPAuth = true;
                  $mail->Username = 'rodicontrol@rodi.com.mx';
                  $mail->Password = 'RRodi#2019@';
                  $mail->SMTPSecure = 'ssl';
                  $mail->Port     = 465;
                  $mail->setFrom('rodicontrol@rodi.com.mx', 'Rodi');
                  $mail->addAddress($to);
                  $mail->Subject = $subject;
                  $mail->isHTML(true);
                  $mailContent = $message;
                  $mail->Body = $mailContent;

                  if (!$mail->send()) {
                    $enviado = 1;
                  } else {
                    $enviado = 0;
                  }
                }
                $msj = array(
                  'codigo' => 1,
                  'msg' => 'Success'
                );
              }
            } else {
              $drogas = 0;
              $tipo_antidoping = 0;
              $pruebas = array(
                'creacion' => $date,
                'edicion' => $date,
                $tipo_usuario => $id_usuario,
                'id_candidato' => $id_candidato,
                'id_cliente' => $id_cliente,
                'socioeconomico' => $socio,
                'tipo_antidoping' => $tipo_antidoping,
                'antidoping' => $drogas,
                'tipo_psicometrico' => $psicometrico,
                'psicometrico' => $psicometrico,
                'medico' => $medico,
                'buro_credito' => 0,
                'sociolaboral' => 0,
                'otro_requerimiento' => $otro_requerimiento
              );
              $this->candidato_model->crearPruebas($pruebas);

              if ($id_subcliente != 180) {
                $visita = array(
                  'creacion' => $date,
                  'edicion' => $date,
                  $tipo_usuario => $id_usuario,
                  'id_cliente' => $id_cliente,
                  'id_subcliente' => $id_subcliente,
                  'id_candidato' => $id_candidato,
                  'id_tipo_formulario' => 4

                );
                $this->candidato_model->crearVisita($visita);
              }

              if ($usuario == 2 || $usuario == 3) {
                $from = $this->config->item('smtp_user');
                $info_cliente = $this->cliente_general_model->getDatosCliente($this->input->post('id_cliente'));
                $to = "bjimenez@rodi.com.mx";
                $subject = " Nuevo candidato en la plataforma del cliente " . $info_cliente->nombre;
                $message = "Se ha agregado a " . strtoupper($this->input->post('nombre')) . " " . strtoupper($this->input->post('paterno')) . " " . strtoupper($this->input->post('materno')) . " del cliente " . $info_cliente->nombre . " en la plataforma";
                $this->load->library('phpmailer_lib');
                $mail = $this->phpmailer_lib->load();
                $mail->isSMTP();
                $mail->Host     = 'rodi.com.mx';
                $mail->SMTPAuth = true;
                $mail->Username = 'rodicontrol@rodi.com.mx';
                $mail->Password = 'RRodi#2019@';
                $mail->SMTPSecure = 'ssl';
                $mail->Port     = 465;
                $mail->setFrom('rodicontrol@rodi.com.mx', 'Rodi');
                $mail->addAddress($to);
                $mail->Subject = $subject;
                $mail->isHTML(true);
                $mailContent = $message;
                $mail->Body = $mailContent;

                if (!$mail->send()) {
                  $enviado = 1;
                } else {
                  $enviado = 0;
                }
              }
              $msj = array(
                'codigo' => 1,
                'msg' => 'Success'
              );
            }
          } else {
            if ($antidoping == 0) {
              $msj = array(
                'codigo' => 4,
                'msg' => 'Debe aplicarse el estudio socioeconomico y/o el examen antidoping'
              );
            } else {
              if ($examen == 0) {
                $msj = array(
                  'codigo' => 3,
                  'msg' => 'El campo Examen Antidoping es obligatorio'
                );
              } else {
                $pruebas = array(
                  'creacion' => $date,
                  'edicion' => $date,
                  $tipo_usuario => $id_usuario,
                  'id_candidato' => $id_candidato,
                  'id_cliente' => $id_cliente,
                  'socioeconomico' => $socio,
                  'tipo_antidoping' => 1,
                  'antidoping' => $examen,
                  'tipo_psicometrico' => 0,
                  'psicometrico' => 0,
                  'medico' => 0,
                  'buro_credito' => 0,
                  'sociolaboral' => 0,
                  'otro_requerimiento' => $otro_requerimiento
                );
                $this->candidato_model->crearPruebas($pruebas);
                $msj = array(
                  'codigo' => 1,
                  'msg' => 'Success'
                );
              }
            }
          }
        }
      }
      echo json_encode($msj);
    }
    function guardarDatosGenerales(){
      $this->form_validation->set_rules('nombre', 'Nombre', 'required|trim');
      $this->form_validation->set_rules('paterno', 'Primer apellido', 'required|trim|alpha');
      $this->form_validation->set_rules('materno', 'Segundo apellido', 'trim|alpha');
      $this->form_validation->set_rules('fecha_nacimiento', 'Fecha de nacimiento', 'required|trim');
      $this->form_validation->set_rules('lugar', 'Lugar de nacimiento', 'required|trim|alpha');
      $this->form_validation->set_rules('puesto', 'Puesto', 'required|trim');
      $this->form_validation->set_rules('genero', 'Género', 'required|trim');
      $this->form_validation->set_rules('calle', 'Calle', 'required|trim');
      $this->form_validation->set_rules('exterior', 'No. Exterior', 'required|trim|max_length[8]');
      $this->form_validation->set_rules('interior', 'No. Interior', 'trim|max_length[8]');
      $this->form_validation->set_rules('colonia', 'Colonia', 'required|trim');
      $this->form_validation->set_rules('estado', 'Estado', 'required|trim|numeric');
      $this->form_validation->set_rules('municipio', 'Municipio', 'required|trim|numeric');
      $this->form_validation->set_rules('cp', 'Código postal', 'required|trim|numeric|max_length[5]');
      $this->form_validation->set_rules('civil', 'Civil', 'required|trim');
      $this->form_validation->set_rules('celular', 'Tel. Celular', 'required|trim|max_length[16]');
      $this->form_validation->set_rules('tel_casa', 'Tel. Casa', 'trim|max_length[16]');
      $this->form_validation->set_rules('grado_estudios', 'Grado máximo de estudios', 'trim|required');
      $this->form_validation->set_rules('correo', 'Correo', 'required|trim|valid_email');

      $this->form_validation->set_message('required','El campo {field} es obligatorio');
      $this->form_validation->set_message('valid_email','El campo {field} debe ser un correo válido');
      $this->form_validation->set_message('max_length','El campo {field} debe tener máximo {param} carácteres');
      $this->form_validation->set_message('alpha','El campo {field} debe contener solo carácteres alfabéticos y sin acentos');
      $this->form_validation->set_message('numeric','El campo {field} debe ser numérico');


      $msj = array();
      if ($this->form_validation->run() == FALSE) {
          $msj = array(
              'codigo' => 0,
              'msg' => validation_errors()
          );
      } 
      else{
          date_default_timezone_set('America/Mexico_City');
          $date = date('Y-m-d H:i:s');
          $id_candidato = $this->input->post('id_candidato');
          $id_usuario = $this->session->userdata('id');
          $fecha = fecha_espanol_bd($this->input->post('fecha_nacimiento'));
          $edad = calculaEdad($fecha);

          $candidato = array(
              'edicion' => $date,
              'id_usuario' => $id_usuario,
              'nombre' => $this->input->post('nombre'),
              'paterno' => $this->input->post('paterno'),
              'materno' => $this->input->post('materno'),
              'fecha_nacimiento' => $fecha,
              'edad' => $edad,
              'id_puesto' => $this->input->post('puesto'),
              'lugar_nacimiento' => $this->input->post('lugar'),
              'genero' => $this->input->post('genero'),
              'id_grado_estudio' => $this->input->post('grado_estudios'),
              'calle' => $this->input->post('calle'),
              'exterior' => $this->input->post('exterior'),
              'interior' => $this->input->post('interior'),
              'entre_calles' => $this->input->post('entre_calles'),
              'colonia' => $this->input->post('colonia'),
              'id_estado' => $this->input->post('estado'),
              'id_municipio' => $this->input->post('municipio'),
              'cp' => $this->input->post('cp'),
              'id_estado_civil' => $this->input->post('civil'),
              'celular' => $this->input->post('celular'),
              'telefono_casa' => $this->input->post('tel_casa'),
              'correo' => $this->input->post('correo')
          );
          $this->candidato_model->editarCandidato($candidato, $id_candidato);
          $visita = array(
            'edicion' => $date,
            'id_usuario' => $id_usuario,
            'calle' => $this->input->post('calle'),
            'exterior' => $this->input->post('exterior'),
            'interior' => $this->input->post('interior'),
            'colonia' => $this->input->post('colonia'),
            'id_estado' => $this->input->post('estado'),
            'id_municipio' => $this->input->post('municipio'),
            'cp' => $this->input->post('cp'),
            'celular' => $this->input->post('celular_general'),
            'telefono_casa' => $this->input->post('tel_casa'),
          );
          $this->candidato_model->editarVisita($visita, $id_candidato);
          $msj = array(
              'codigo' => 1,
              'msg' => 'success'
          );
      }
      echo json_encode($msj);
    }
    function guardarHistorialAcademico(){
      $this->form_validation->set_rules('prim_promedio', 'Promedio de Primaria', 'numeric|trim');
      $this->form_validation->set_rules('sec_promedio', 'Promedio de Secundaria', 'numeric|trim');
      $this->form_validation->set_rules('prep_promedio', 'Promedio de Bachillerato', 'numeric|trim');
      $this->form_validation->set_rules('lic_promedio', 'Promedio de Licenciatura', 'numeric|trim');
      $this->form_validation->set_rules('actual_promedio', 'Promedio de Estudios actuales', 'numeric|trim');
      $this->form_validation->set_rules('cedula', 'Cédula profesional', 'required|trim');
      $this->form_validation->set_rules('otro_certificado', 'Otros certificados/cursos', 'required|trim');
      $this->form_validation->set_rules('carrera_inactivo', 'Periodos inactivos', 'required|trim');
      $this->form_validation->set_rules('estudios_comentarios', 'Comentarios', 'required|trim');

      $this->form_validation->set_message('required','El campo {field} es obligatorio');
      $this->form_validation->set_message('numeric','El campo {field} debe ser numérico');

      $msj = array();
      if ($this->form_validation->run() == FALSE) {
          $msj = array(
              'codigo' => 0,
              'msg' => validation_errors()
          );
      } 
      else{
          date_default_timezone_set('America/Mexico_City');
          $date = date('Y-m-d H:i:s');
          $id_candidato = $this->input->post('id_candidato');
          $id_usuario = $this->session->userdata('id');

          $data['estudios'] = $this->candidato_model->revisionEstudios($id_candidato);
          if($data['estudios']){
              $estudios = array(
                  'edicion' => $date,
                  'id_usuario' => $id_usuario,
                  'primaria_periodo' => $this->input->post('prim_periodo'),
                  'primaria_escuela' => $this->input->post('prim_escuela'),
                  'primaria_ciudad' => $this->input->post('prim_ciudad'),
                  'primaria_certificado' => $this->input->post('prim_certificado'),
                  'primaria_promedio' => $this->input->post('prim_promedio'),
                  'secundaria_periodo' => $this->input->post('sec_periodo'),
                  'secundaria_escuela' => $this->input->post('sec_escuela'),
                  'secundaria_ciudad' => $this->input->post('sec_ciudad'),
                  'secundaria_certificado' => $this->input->post('sec_certificado'),
                  'secundaria_promedio' => $this->input->post('sec_promedio'),
                  'preparatoria_periodo' => $this->input->post('prep_periodo'),
                  'preparatoria_escuela' => $this->input->post('prep_escuela'),
                  'preparatoria_ciudad' => $this->input->post('prep_ciudad'),
                  'preparatoria_certificado' => $this->input->post('prep_certificado'),
                  'preparatoria_promedio' => $this->input->post('prep_promedio'),
                  'licenciatura_periodo' => $this->input->post('lic_periodo'),
                  'licenciatura_escuela' => $this->input->post('lic_escuela'),
                  'licenciatura_ciudad' => $this->input->post('lic_ciudad'),
                  'licenciatura_certificado' => $this->input->post('lic_certificado'),
                  'licenciatura_promedio' => $this->input->post('lic_promedio'),
                  'actual_periodo' => $this->input->post('actual_periodo'),
                  'actual_escuela' => $this->input->post('actual_escuela'),
                  'actual_ciudad' => $this->input->post('actual_ciudad'),
                  'actual_certificado' => $this->input->post('actual_certificado'),
                  'actual_promedio' => $this->input->post('actual_promedio'),
                  'cedula_profesional' => $this->input->post('cedula'),
                  'otros_certificados' => $this->input->post('otro_certificado'),
                  'comentarios' => $this->input->post('estudios_comentarios'),
                  'carrera_inactivo' => $this->input->post('carrera_inactivo')
              );
              $this->candidato_model->editarEstudios($estudios, $id_candidato);
          }
          else{
              $estudios = array(
                  'creacion' => $date,
                  'edicion' => $date,
                  'id_usuario' => $id_usuario,
                  'id_candidato' => $id_candidato,
                  'primaria_periodo' => $this->input->post('prim_periodo'),
                  'primaria_escuela' => $this->input->post('prim_escuela'),
                  'primaria_ciudad' => $this->input->post('prim_ciudad'),
                  'primaria_certificado' => $this->input->post('prim_certificado'),
                  'primaria_promedio' => $this->input->post('prim_promedio'),
                  'secundaria_periodo' => $this->input->post('sec_periodo'),
                  'secundaria_escuela' => $this->input->post('sec_escuela'),
                  'secundaria_ciudad' => $this->input->post('sec_ciudad'),
                  'secundaria_certificado' => $this->input->post('sec_certificado'),
                  'secundaria_promedio' => $this->input->post('sec_promedio'),
                  'preparatoria_periodo' => $this->input->post('prep_periodo'),
                  'preparatoria_escuela' => $this->input->post('prep_escuela'),
                  'preparatoria_ciudad' => $this->input->post('prep_ciudad'),
                  'preparatoria_certificado' => $this->input->post('prep_certificado'),
                  'preparatoria_promedio' => $this->input->post('prep_promedio'),
                  'licenciatura_periodo' => $this->input->post('lic_periodo'),
                  'licenciatura_escuela' => $this->input->post('lic_escuela'),
                  'licenciatura_ciudad' => $this->input->post('lic_ciudad'),
                  'licenciatura_certificado' => $this->input->post('lic_certificado'),
                  'licenciatura_promedio' => $this->input->post('lic_promedio'),
                  'actual_periodo' => $this->input->post('actual_periodo'),
                  'actual_escuela' => $this->input->post('actual_escuela'),
                  'actual_ciudad' => $this->input->post('actual_ciudad'),
                  'actual_certificado' => $this->input->post('actual_certificado'),
                  'actual_promedio' => $this->input->post('actual_promedio'),
                  'cedula_profesional' => $this->input->post('cedula'),
                  'otros_certificados' => $this->input->post('otro_certificado'),
                  'comentarios' => $this->input->post('estudios_comentarios'),
                  'carrera_inactivo' => $this->input->post('carrera_inactivo')
              );
              $this->candidato_model->guardarEstudios($estudios);
          }
          //$this->generarAvancesUST($id_candidato);
          $msj = array(
              'codigo' => 1,
              'msg' => 'success'
          );
      }
      echo json_encode($msj);
    }
    function guardarAntecendentesSociales(){
      $this->form_validation->set_rules('sindical', '¿Perteneció algún puesto sindical?', 'required|trim');
      $this->form_validation->set_rules('sindical_nombre', '¿A cuál?', 'required|trim');
      $this->form_validation->set_rules('sindical_cargo', '¿Cargo?', 'required|trim');
      $this->form_validation->set_rules('partido', '¿Pertenece algún partido político?', 'required|trim');
      $this->form_validation->set_rules('partido_nombre', '¿A cuál?', 'required|trim');
      $this->form_validation->set_rules('partido_cargo', '¿Cargo?', 'required|trim');
      $this->form_validation->set_rules('club', '¿Pertenece algún club deportivo?', 'required|trim');
      $this->form_validation->set_rules('deporte', '¿Qué deporte practica?', 'required|trim');
      $this->form_validation->set_rules('religion', '¿Qué religión profesa?', 'required|trim');
      $this->form_validation->set_rules('religion_frecuencia', '¿Con qué frecuencia?', 'required|trim');
      $this->form_validation->set_rules('bebidas', '>¿Ingiere bebidas alcohólicas?', 'required|trim');
      $this->form_validation->set_rules('bebidas_frecuencia', '¿Con qué frecuencia?', 'required|trim');
      $this->form_validation->set_rules('fumar', '¿Acostumbra fumar?', 'required|trim');
      $this->form_validation->set_rules('fumar_frecuencia', '¿Con qué frecuencia?', 'required|trim');
      $this->form_validation->set_rules('cirugia', '¿Ha tenido alguna intervención quirúrgica?', 'required|trim');
      $this->form_validation->set_rules('enfermedades', '¿Antecedentes de enfermedades en su familia?', 'required|trim');
      $this->form_validation->set_rules('corto_plazo', '¿Cuáles son sus planes a corto plazo?', 'required|trim');
      $this->form_validation->set_rules('mediano_plazo', '¿Cuáles son sus planes a mediano plazo?', 'required|trim');

      $this->form_validation->set_message('required','El campo {field} es obligatorio');

      $msj = array();
      if ($this->form_validation->run() == FALSE) {
          $msj = array(
              'codigo' => 0,
              'msg' => validation_errors()
          );
      } 
      else{
        date_default_timezone_set('America/Mexico_City');
        $date = date('Y-m-d H:i:s');
        $id_candidato = $this->input->post('id_candidato');
        $id_usuario = $this->session->userdata('id');

        $data['sociales'] = $this->candidato_model->revisionAntecedentesSociales($id_candidato);
        if($data['sociales'] != ""){
          $sociales = array(
            'edicion' => $date,
            'id_usuario' => $id_usuario,
            'sindical' => $this->input->post('sindical'),
            'sindical_nombre' => $this->input->post('sindical_nombre'),
            'sindical_cargo' => $this->input->post('sindical_cargo'),
            'partido' => $this->input->post('partido'),
            'partido_nombre' => $this->input->post('partido_nombre'),
            'partido_cargo' => $this->input->post('partido_cargo'),
            'club' => $this->input->post('club'),
            'deporte' => $this->input->post('deporte'),
            'religion' => $this->input->post('religion'),
            'religion_frecuencia' => $this->input->post('religion_frecuencia'),
            'bebidas' => $this->input->post('bebidas'),
            'bebidas_frecuencia' => $this->input->post('bebidas_frecuencia'),
            'fumar' => $this->input->post('fumar'),
            'fumar_frecuencia' => $this->input->post('fumar_frecuencia'),
            'cirugia' => $this->input->post('cirugia'),
            'enfermedades' => $this->input->post('enfermedades'),
            'corto_plazo' => $this->input->post('corto_plazo'),
            'mediano_plazo' => $this->input->post('mediano_plazo')
          );
          $this->candidato_model->updateSociales($sociales, $id_candidato);
        }
        else{
          $sociales = array(
            'creacion' => $date,
            'edicion' => $date,
            'id_usuario' => $id_usuario,
            'id_candidato' => $id_candidato,
            'sindical' => $this->input->post('sindical'),
            'sindical_nombre' => $this->input->post('sindical_nombre'),
            'sindical_cargo' => $this->input->post('sindical_cargo'),
            'partido' => $this->input->post('partido'),
            'partido_nombre' => $this->input->post('partido_nombre'),
            'partido_cargo' => $this->input->post('partido_cargo'),
            'club' => $this->input->post('club'),
            'deporte' => $this->input->post('deporte'),
            'religion' => $this->input->post('religion'),
            'religion_frecuencia' => $this->input->post('religion_frecuencia'),
            'bebidas' => $this->input->post('bebidas'),
            'bebidas_frecuencia' => $this->input->post('bebidas_frecuencia'),
            'fumar' => $this->input->post('fumar'),
            'fumar_frecuencia' => $this->input->post('fumar_frecuencia'),
            'cirugia' => $this->input->post('cirugia'),
            'enfermedades' => $this->input->post('enfermedades'),
            'corto_plazo' => $this->input->post('corto_plazo'),
            'mediano_plazo' => $this->input->post('mediano_plazo')
          );
          $this->candidato_model->saveSociales($sociales);
        }
        $msj = array(
          'codigo' => 1,
          'msg' => 'success'
        );
      }
      echo json_encode($msj);
    }
    function guardarReferenciaPersonal(){
      $this->form_validation->set_rules('nombre', 'Nombre', 'required|trim');
      $this->form_validation->set_rules('tiempo', 'Tiempo de conocerlo', 'required|trim');
      $this->form_validation->set_rules('telefono', 'Teléfono', 'required|trim|max_length[12]');
      $this->form_validation->set_rules('recomienda', '¿Lo recomienda?', 'required|trim');
      $this->form_validation->set_rules('comentario', 'Comentarios', 'required|trim');
      
      $this->form_validation->set_message('required','El campo {field} es obligatorio');
      $this->form_validation->set_message('max_length','El campo {field} debe tener máximo {param} carácteres');
      $this->form_validation->set_message('numeric','El campo {field} debe ser numérico');

      $msj = array();
      if ($this->form_validation->run() == FALSE) {
          $msj = array(
              'codigo' => 0,
              'msg' => validation_errors()
          );
      } 
      else{
          date_default_timezone_set('America/Mexico_City');
          $date = date('Y-m-d H:i:s');
          $salida = "";
          $id_candidato = $this->input->post('id_candidato');
          $id_refper = $this->input->post('id_refper');
          $num = $this->input->post('num');
          $id_usuario = $this->session->userdata('id');

          if($id_refper != ""){
              $data = array(
                  'edicion' => $date,
                  'id_usuario' => $id_usuario,
                  'nombre' => $this->input->post('nombre'),
                  'telefono' => $this->input->post('telefono'),
                  'tiempo_conocerlo' => $this->input->post('tiempo'),
                  'recomienda' => $this->input->post('recomienda'),
                  'comentario' => $this->input->post('comentario')
              );
              $this->candidato_model->editarReferenciaPersonal($id_refper, $data);
              $msj = array(
                  'codigo' => 1,
                  'msg' => 'success'
              );
          }
          else{
              $data_refper = array(
                  'creacion' => $date,
                  'edicion' => $date,
                  'id_usuario' => $id_usuario,
                  'id_candidato' => $id_candidato,
                  'nombre' => $this->input->post('nombre'),
                  'telefono' => $this->input->post('telefono'),
                  'tiempo_conocerlo' => $this->input->post('tiempo'),
                  'recomienda' => $this->input->post('recomienda'),
                  'comentario' => $this->input->post('comentario')
              );
              $id_nuevo = $this->candidato_model->guardarReferenciaPersonal($data_refper);
              //$this->generarAvancesUST($id_candidato);
              $msj = array(
                  'codigo' => 2,
                  'msg' => $id_nuevo
              );
          }
      }
      echo json_encode($msj);
    }
    function guardarAntecedenteLaboral(){
      $this->form_validation->set_rules('empresa', 'Nombre de la empresa', 'required|trim');
      $this->form_validation->set_rules('area', 'Área o Departamento', 'required|trim');
      $this->form_validation->set_rules('domicilio', 'Domicilio, calle y número', 'required|trim');
      $this->form_validation->set_rules('colonia', 'Colonia', 'required|trim');
      $this->form_validation->set_rules('cp', 'Código postal', 'required|trim|max_length[5]|numeric');
      $this->form_validation->set_rules('telefono', 'Teléfono', 'required|trim|max_length[12]');
      $this->form_validation->set_rules('tipo', 'Tipo de empresa', 'required|trim');
      $this->form_validation->set_rules('puesto', 'Puesto desempeñado', 'required|trim');
      $this->form_validation->set_rules('periodo', 'Periodo trabajado, mes y año', 'required|trim');
      $this->form_validation->set_rules('jefenombre', 'Nombre del último jefe', 'required|trim');
      $this->form_validation->set_rules('jefepuesto', 'Puesto del último jefe', 'required|trim');
      $this->form_validation->set_rules('sueldo1', 'Sueldo mensual inicial', 'required|trim|numeric');
      $this->form_validation->set_rules('sueldo2', 'Sueldo mensual final', 'required|trim|numeric');
      $this->form_validation->set_rules('actividades', '¿En qué consistía su trabajo?', 'required|trim');
      $this->form_validation->set_rules('razon', 'Causa de separación', 'required|trim');
      $this->form_validation->set_rules('comentarios', 'Comentarios y observaciones', 'required|trim');
      
      $this->form_validation->set_message('required','El campo {field} es obligatorio');
      $this->form_validation->set_message('max_length','El campo {field} debe tener máximo {param} carácteres');
      $this->form_validation->set_message('numeric','El campo {field} debe ser numérico');

      $msj = array();
      if ($this->form_validation->run() == FALSE) {
          $msj = array(
              'codigo' => 0,
              'msg' => validation_errors()
          );
      } 
      else{
        date_default_timezone_set('America/Mexico_City');
        $date = date('Y-m-d H:i:s');
        $id_candidato = $this->input->post('id_candidato');
        $num = $this->input->post('num');
        $id_usuario = $this->session->userdata('id');

        $data['refs'] = $this->candidato_model->revisionAntecedenteLaboral($id_candidato, $num);
        if($data['refs']){
            $datos = array(
                'edicion' => $date,
                'id_usuario' => $id_usuario,
                'id_candidato' => $id_candidato,
                'numero_referencia' => $num,
                'empresa' => $this->input->post('empresa'), 
                'area' => $this->input->post('area'),
                'calle' => $this->input->post('domicilio'), 
                'colonia' => $this->input->post('colonia'),
                'cp' => $this->input->post('cp'),
                'telefono' => $this->input->post('telefono'), 
                'tipo_empresa' => $this->input->post('tipo'),
                'puesto' => $this->input->post('puesto'), 
                'periodo' => $this->input->post('periodo'), 
                'jefe_nombre' => $this->input->post('jefenombre'), 
                'jefe_puesto' => $this->input->post('jefepuesto'), 
                'sueldo_inicial' => $this->input->post('sueldo1'), 
                'sueldo_final' => $this->input->post('sueldo2'), 
                'actividades' => $this->input->post('actividades'), 
                'causa_separacion' => $this->input->post('razon'),
                'trabajo_calidad' => $this->input->post('calidad'), 
                'trabajo_puntualidad' => $this->input->post('puntualidad'), 
                'trabajo_honesto' => $this->input->post('honesto'), 
                'trabajo_responsabilidad' => $this->input->post('responsabilidad'),
                'trabajo_adaptacion' => $this->input->post('adaptacion'), 
                'trabajo_actitud_jefes' => $this->input->post('actitud_jefes'),
                'trabajo_actitud_companeros' => $this->input->post('actitud_comp'),
                'comentarios' => $this->input->post('comentarios')
            );
            $this->candidato_model->editarAntecedenteLaboral($datos, $id_candidato, $num);
            $msj = array(
              'codigo' => 1,
              'msg' => 'success'
            );
        }
        else{
            $verificacion_reflab = array(
                'creacion' => $date,
                'edicion' => $date,
                'id_usuario' => $id_usuario,
                'id_candidato' => $id_candidato,
                'numero_referencia' => $num,
                'empresa' => $this->input->post('empresa'), 
                'area' => $this->input->post('area'),
                'calle' => $this->input->post('domicilio'), 
                'colonia' => $this->input->post('colonia'),
                'cp' => $this->input->post('cp'),
                'telefono' => $this->input->post('telefono'), 
                'tipo_empresa' => $this->input->post('tipo'),
                'puesto' => $this->input->post('puesto'), 
                'periodo' => $this->input->post('periodo'), 
                'jefe_nombre' => $this->input->post('jefenombre'), 
                'jefe_puesto' => $this->input->post('jefepuesto'), 
                'sueldo_inicial' => $this->input->post('sueldo1'), 
                'sueldo_final' => $this->input->post('sueldo2'), 
                'actividades' => $this->input->post('actividades'), 
                'causa_separacion' => $this->input->post('razon'),
                'trabajo_calidad' => $this->input->post('calidad'), 
                'trabajo_puntualidad' => $this->input->post('puntualidad'), 
                'trabajo_honesto' => $this->input->post('honesto'), 
                'trabajo_responsabilidad' => $this->input->post('responsabilidad'),
                'trabajo_adaptacion' => $this->input->post('adaptacion'), 
                'trabajo_actitud_jefes' => $this->input->post('actitud_jefes'),
                'trabajo_actitud_companeros' => $this->input->post('actitud_comp'),
                'comentarios' => $this->input->post('comentarios')
            );
            $id_nuevo = $this->candidato_model->guardarAntecedenteLaboral($verificacion_reflab);
            $msj = array(
              'codigo' => 2,
              'msg' => $id_nuevo
            );
        }
      }
      echo json_encode($msj);
    }
    function guardarInvestigacionLegal(){
      $this->form_validation->set_rules('penal', 'Penal', 'required|trim');
      $this->form_validation->set_rules('penal_notas', 'Penal notas', 'required|trim');
      $this->form_validation->set_rules('civil', 'Civil', 'required|trim');
      $this->form_validation->set_rules('civil_notas', 'Civil notas', 'required|trim');
      $this->form_validation->set_rules('laboral', 'Laboral', 'required|trim');
      $this->form_validation->set_rules('laboral_notas', 'Laboral notas', 'required|trim');
      
      $this->form_validation->set_message('required','El campo {field} es obligatorio');

      $msj = array();
      if ($this->form_validation->run() == FALSE) {
          $msj = array(
              'codigo' => 0,
              'msg' => validation_errors()
          );
      } 
      else{
        date_default_timezone_set('America/Mexico_City');
        $date = date('Y-m-d H:i:s');
        $id_candidato = $this->input->post('id_candidato');
        $id_usuario = $this->session->userdata('id');

        $data['legal'] = $this->candidato_model->revisionInvestigacionLegal($id_candidato);
        if($data['legal']){
            $datos = array(
                'edicion' => $date,
                'id_usuario' => $id_usuario,
                'penal' => $this->input->post('penal'),
                'penal_notas' => $this->input->post('penal_notas'),
                'civil' => $this->input->post('civil'),
                'civil_notas' => $this->input->post('civil_notas'),
                'laboral' => $this->input->post('laboral'),
                'laboral_notas' => $this->input->post('laboral_notas')
            );
            foreach ($data['legal'] as $dato) {
                $id = $dato->id;
            }
            $this->candidato_model->editarInvestigacionLegal($datos, $id);
        }
        else{
          $datos = array(
              'creacion' => $date,
              'edicion' => $date,
              'id_usuario' => $id_usuario,
              'id_candidato' => $id_candidato,
              'penal' => $this->input->post('penal'),
              'penal_notas' => $this->input->post('penal_notas'),
              'civil' => $this->input->post('civil'),
              'civil_notas' => $this->input->post('civil_notas'),
              'laboral' => $this->input->post('laboral'),
              'laboral_notas' => $this->input->post('laboral_notas')
          );
          $id = $this->candidato_model->guardarInvestigacionLegal($datos);
        }
        $msj = array(
          'codigo' => 1,
          'msg' => 'success'
        );
      }
      echo json_encode($msj);
    }
    function guardarTrabajosNoMencionados(){
      $this->form_validation->set_rules('no_mencionados', 'Trabajos no mencionados', 'required|trim');
      $this->form_validation->set_rules('resultado', 'Resultado', 'required|trim');
      $this->form_validation->set_rules('notas', 'Notas', 'required|trim');
      
      $this->form_validation->set_message('required','El campo {field} es obligatorio');

      $msj = array();
      if ($this->form_validation->run() == FALSE) {
          $msj = array(
              'codigo' => 0,
              'msg' => validation_errors()
          );
      } 
      else{
        date_default_timezone_set('America/Mexico_City');
        $date = date('Y-m-d H:i:s');
        $id_candidato = $this->input->post('id_candidato');
        $id_usuario = $this->session->userdata('id');

        $data['no_mencionados'] = $this->candidato_model->revisionTrabajosNoMencionados($id_candidato);
        if($data['no_mencionados']){
            $datos = array(
                'edicion' => $date,
                'id_usuario' => $id_usuario,
                'no_mencionados' => $this->input->post('no_mencionados'),
                'resultado_no_mencionados' => $this->input->post('resultado'),
                'notas_no_mencionados' => $this->input->post('notas')
            );
            foreach ($data['no_mencionados'] as $dato) {
                $id = $dato->id;
            }
            $this->candidato_model->updateTrabajosNoMencionados($datos, $id);
        }
        else{
            $datos = array(
                'creacion' => $date,
                'edicion' => $date,
                'id_usuario' => $id_usuario,
                'id_candidato' => $id_candidato,
                'no_mencionados' => $this->input->post('no_mencionados'),
                'resultado_no_mencionados' => $this->input->post('resultado'),
                'notas_no_mencionados' => $this->input->post('notas')
            );
          $id = $this->candidato_model->saveTrabajosNoMencionados($datos);
        }
        $msj = array(
          'codigo' => 1,
          'msg' => 'success'
        );
      }
      echo json_encode($msj);
    }
    function actualizarProcesoCandidato(){
      date_default_timezone_set('America/Mexico_City');
      $date = date('Y-m-d H:i:s');
      $fecha_dia = date('d-m-Y');
      $id_usuario = $this->session->userdata('id');
      $id_candidato = $this->input->post('id_candidato');
      $id_doping = $this->input->post('id_doping');

      $pruebas = $this->candidato_model->getPruebasCandidato($id_candidato);
      $datos_pruebas = array(
        'creacion' => $date,
        'edicion' => $date,
        'id_usuario' => $id_usuario,
        'id_candidato' => $id_candidato,
        'id_cliente' => $pruebas->id_cliente,
        'socioeconomico' => $pruebas->socioeconomico,
        'tipo_antidoping' => $pruebas->tipo_antidoping,
        'antidoping' => $pruebas->antidoping,
        'status_doping' => 0,
        'tipo_psicometrico' => $pruebas->tipo_psicometrico,
        'psicometrico' => $pruebas->psicometrico,
        'medico' => $pruebas->medico,
        'buro_credito' => $pruebas->buro_credito,
        'sociolaboral' => $pruebas->sociolaboral,
        'ofac' => $pruebas->ofac,
        'resultado_ofac' => $pruebas->resultado_ofac,
        'oig' => $pruebas->oig,
        'resultado_oig' => $pruebas->resultado_oig,
        'sam' => $pruebas->sam,
        'resultado_sam' => $pruebas->resultado_sam,
        'data_juridica' => $pruebas->data_juridica,
        'res_data_juridica' => $pruebas->res_data_juridica,
        'otro_requerimiento' => $pruebas->otro_requerimiento
      );
      $this->candidato_model->eliminarCandidatoPruebas($id_candidato);
      $this->candidato_model->crearPruebas($datos_pruebas);
      $this->doping_model->cambiarEstatusDoping($id_candidato);
      //Historial
      $info = $this->candidato_model->getInfoCandidatoEspecifico($id_candidato);
      $fecha_alta = fecha_sinhora_espanol_front($info->fecha_alta);
      $visita = ($info->visitador == 1)? 'SI':'NO';
      $examen_antidoping = ($pruebas->antidoping > 0)? 'SI':'NO';
      $examen_psicometrico = ($pruebas->psicometrico == 1)? 'SI':'NO';
      $examen_medico = ($pruebas->medico == 1)? 'SI':'NO';
      $buro = ($pruebas->buro_credito == 1)? 'SI':'NO';
      switch ($info->status_bgc) {
          case 1:
              $estatus_final = 'POSITIVO';
              break;
          case 2:
              $estatus_final = 'NEGATIVO';
              break;
          case 3:
              $estatus_final = 'A CONSIDERACION';
              break;
      }
      $historial = array(
          'creacion' => $date,
          'id_candidato' => $id_candidato,
          'usuario' => $info->usuario,
          'id_tipo_proceso' => $info->id_tipo_proceso,
          'puesto' => $info->puesto,
          'fecha_alta' => $fecha_alta,
          'visita' => $visita,
          'antidoping' => $examen_antidoping,
          'psicometrico' => $examen_psicometrico,
          'medico' => $examen_medico,
          'buro_credito' => $buro,
          'tiempo_proceso' => $info->tiempo_parcial,
          'status_bgc' => $estatus_final
      );
      $this->candidato_model->guardarHistorialCandidato($historial);
        
      $this->candidato_model->eliminarCandidatoFinalizado($id_candidato);
      $this->candidato_model->eliminarCandidatoBGC($id_candidato);
      //Borrar datos de la Visita
      $this->candidato_model->eliminarCandidatoEgresos($id_candidato);
      $this->candidato_model->eliminarCandidatoHabitacion($id_candidato);
      $this->candidato_model->eliminarCandidatoVecinos($id_candidato);
      $this->candidato_model->eliminarCandidatoPersona($id_candidato);
      $this->candidato_model->eliminarCandidatoPersonaMismoTrabajo($id_candidato);

      $dop = array(
          'edicion' => $date,
          'status' => 0
      );
      $this->candidato_model->editarDoping($dop, $id_doping);

      $datos = array(
          'edicion' => $date,
          'fecha_alta' => $date,
          'muebles' => '',
          'adeudo_muebles' => 0,
          'ingresos' => '',
          'comentario' => '',
          'status' => 0,
          'status_bgc' => 0,
          'visitador' => 0,
          'tiempo_parcial' => 0
      );
      $this->candidato_model->editarCandidato($datos, $id_candidato);

      $row = $this->candidato_model->checkActualizacionCandidato($id_candidato);
      if($row != null){
        $act = array(
            'edicion' => $date,
            'usuarios' => $row->usuarios.','.$id_usuario,
            'fechas' => $row->fechas.','.$fecha_dia,
            'num' => ($row->num + 1)
        );
        $this->candidato_model->editarActualizacionCandidato($act, $id_candidato);
      }
      else{
        $act = array(
            'creacion' => $date,
            'edicion' => $date,
            'usuarios' => $id_usuario,
            'id_candidato' => $id_candidato,
            'fechas' => $fecha_dia,
            'num' => 1
        );
        $this->candidato_model->guardarActualizacionCandidato($act);
      }
      echo $salida = 1;
    }
    function guardarDocumentacionVisita(){
      $this->form_validation->set_rules('fecha_acta', 'Fecha de expedición del acta de nacimiento', 'required|trim');
      $this->form_validation->set_rules('numero_acta', 'Número y/o vigencia del acta de nacimiento', 'required|trim');
      $this->form_validation->set_rules('fecha_domicilio', 'Fecha de expedición del comprobante del domicilio', 'required|trim');
      $this->form_validation->set_rules('numero_domicilio', 'Número y/o vigencia del comprobante del domicilio', 'required|trim');
      $this->form_validation->set_rules('fecha_curp', 'Fecha de expedición del CURP', 'required|trim');
      $this->form_validation->set_rules('numero_curp', 'Número y/o vigencia del CURP', 'required|trim');
      $this->form_validation->set_rules('fecha_rfc', 'Fecha de expedición del RFC', 'required|trim');
      $this->form_validation->set_rules('numero_rfc', 'Número y/o vigencia del RFC', 'required|trim');
      
      $this->form_validation->set_message('required','El campo {field} es obligatorio');

      $msj = array();
      if ($this->form_validation->run() == FALSE) {
          $msj = array(
              'codigo' => 0,
              'msg' => validation_errors()
          );
      }
      else{
        date_default_timezone_set('America/Mexico_City');
        $date = date('Y-m-d H:i:s');
        $id_candidato = $this->input->post('id_candidato');
        $id_usuario = $this->session->userdata('id');
        
        $f_acta = ($this->input->post('fecha_acta') !== null)? fecha_espanol_bd($this->input->post('fecha_acta')) : "";
        $f_domicilio = ($this->input->post('fecha_domicilio') !== null)? fecha_espanol_bd($this->input->post('fecha_domicilio')) : "";
        $f_curp = ($this->input->post('fecha_curp') !== null)? fecha_espanol_bd($this->input->post('fecha_curp'))  :"";
        $f_imss = ($this->input->post('fecha_imss') !== null)? fecha_espanol_bd($this->input->post('fecha_imss')) : "";
        $f_retencion = ($this->input->post('fecha_retencion') !== null)? fecha_espanol_bd($this->input->post('fecha_retencion')) : "";
        $f_rfc = ($this->input->post('fecha_rfc') !== null)? fecha_espanol_bd($this->input->post('fecha_rfc')) : "";
        $f_licencia = ($this->input->post('fecha_licencia') !== null)? fecha_espanol_bd($this->input->post('fecha_licencia')) : "";
        $f_migra = ($this->input->post('fecha_migra') !== null)? fecha_espanol_bd($this->input->post('fecha_migra')) : "";
        $f_visa = ($this->input->post('fecha_visa') !== null)? fecha_espanol_bd($this->input->post('fecha_visa')) : "";
        $documentacion = array(
            'edicion' => $date,
            'fecha_acta' => $f_acta,
            'acta' => $this->input->post('numero_acta'),
            'fecha_domicilio' => $f_domicilio,
            'cuenta_domicilio' => $this->input->post('numero_domicilio'),
            'emision_ine' => $this->input->post('fecha_ine'),
            'ine' => $this->input->post('numero_ine'),
            'emision_curp' => $f_curp,
            'curp' => $this->input->post('numero_curp'),
            'emision_nss' => $f_imss,
            'nss' => $this->input->post('numero_imss'),
            'fecha_retencion_impuestos' => $f_retencion,
            'retencion_impuestos' => $this->input->post('numero_retencion'),
            'emision_rfc' => $f_rfc,
            'rfc' => $this->input->post('numero_rfc'),
            'fecha_licencia' => $f_licencia,
            'licencia' => $this->input->post('numero_licencia'),
            'vigencia_migratoria' => $f_migra,
            'numero_migratorio' => $this->input->post('numero_migra'),
            'fecha_visa' => $f_visa,
            'visa' => $this->input->post('numero_visa')
        );
        $this->candidato_model->editarCandidato($documentacion, $id_candidato);

        $msj = array(
            'codigo' => 1,
            'msg' => 'success'
        );
      } 
      echo json_encode($msj);
    }
    function guardarIntegranteFamiliar(){
      $this->form_validation->set_rules('nombre', 'Nombre', 'required|trim');
      $this->form_validation->set_rules('parentesco', 'Parentesco', 'required|trim');
      $this->form_validation->set_rules('edad', 'Edad', 'required|trim|numeric|max_length[3]');
      $this->form_validation->set_rules('civil', 'Estado civil', 'required|trim');
      $this->form_validation->set_rules('escolaridad', 'Escolaridad', 'required|trim');
      $this->form_validation->set_rules('vive', '¿Vive con usted?', 'required|trim');
      $this->form_validation->set_rules('empresa', 'Empresa', 'required|trim');
      $this->form_validation->set_rules('puesto', 'Puesto', 'required|trim');
      $this->form_validation->set_rules('antiguedad', 'Antigüedad', 'required|trim');
      $this->form_validation->set_rules('sueldo', 'Sueldo', 'required|trim|numeric');
      $this->form_validation->set_rules('aportacion', 'Aportación', 'required|trim|numeric');
      $this->form_validation->set_rules('muebles', 'Muebles e inmuebles', 'required|trim');
      $this->form_validation->set_rules('adeudo', 'Adeudo', 'required|trim');
      
      $this->form_validation->set_message('required','El campo {field} es obligatorio');
      $this->form_validation->set_message('max_length','El campo {field} debe tener máximo {param} carácteres');
      $this->form_validation->set_message('numeric','El campo {field} debe ser numérico');

      $msj = array();
      if ($this->form_validation->run() == FALSE) {
          $msj = array(
              'codigo' => 0,
              'msg' => validation_errors()
          );
      }
      else{
        date_default_timezone_set('America/Mexico_City');
        $date = date('Y-m-d H:i:s');
        $id_candidato = $this->input->post('id_candidato');
        $id_persona = $this->input->post('id_persona');
        $id_usuario = $this->session->userdata('id');

        $datos = array(
          'edicion' => $date,
          'nombre' => $this->input->post('nombre'),
          'id_tipo_parentesco' => $this->input->post('parentesco'),
          'edad' => $this->input->post('edad'),
          'id_grado_estudio' =>  $this->input->post('escolaridad'),
          'misma_vivienda' =>  $this->input->post('vive'),
          'id_estado_civil' =>  $this->input->post('civil'),
          'empresa' =>  $this->input->post('empresa'),
          'puesto' =>  $this->input->post('puesto'),
          'antiguedad' => $this->input->post('antiguedad'),
          'sueldo' =>  $this->input->post('sueldo'),
          'monto_aporta' =>  $this->input->post('aportacion'),
          'muebles' =>  $this->input->post('muebles'),
          'adeudo' =>  $this->input->post('adeudo')
        );
        $this->candidato_model->editarIntegranteFamiliar($datos, $id_persona);

        $msj = array(
          'codigo' => 1,
          'msg' => 'success'
        );
      }
      echo json_encode($msj);
    }
    function editarExtrasCandidato(){
      $this->form_validation->set_rules('notas', 'Notas', 'required|trim');
      $this->form_validation->set_rules('muebles', 'Muebles e inmuebles del candidato', 'required|trim');
      $this->form_validation->set_rules('adeudo', 'Adeudo', 'required|trim');
      $this->form_validation->set_rules('ingresos', 'Ingresos del candidato', 'required|trim|numeric');
      
      $this->form_validation->set_message('required','El campo {field} es obligatorio');
      $this->form_validation->set_message('numeric','El campo {field} debe ser numérico');


      $msj = array();
      if ($this->form_validation->run() == FALSE) {
          $msj = array(
              'codigo' => 0,
              'msg' => validation_errors()
          );
      }
      else{
        date_default_timezone_set('America/Mexico_City');
        $date = date('Y-m-d H:i:s');
        $datos = array(
            'edicion' => $date,
            'muebles' => $this->input->post('muebles'),
            'comentario' => $this->input->post('notas'),
            'adeudo_muebles' => $this->input->post('adeudo'),
            'ingresos' => $this->input->post('ingresos')

        );
        $this->candidato_model->editarCandidato($datos, $this->input->post('id_candidato'));
        $msj = array(
          'codigo' => 1,
          'msg' => 'success'
        );
      }
      echo json_encode($msj);
    }
    function guardarEgresos(){
      $this->form_validation->set_rules('renta', 'Renta', 'required|trim|numeric');
      $this->form_validation->set_rules('alimentos', 'Alimentos', 'required|trim|numeric');
      $this->form_validation->set_rules('servicios', 'Servicios', 'required|trim|numeric');
      $this->form_validation->set_rules('transportes', 'Transportes', 'required|trim|numeric');
      $this->form_validation->set_rules('otros_gastos', 'Otros', 'required|trim|numeric');
      $this->form_validation->set_rules('solvencia', 'Cuando los egresos son mayores a los ingresos, ¿cómo los solventa?', 'required|trim');
      
      $this->form_validation->set_message('required','El campo {field} es obligatorio');
      $this->form_validation->set_message('numeric','El campo {field} debe ser numérico');


      $msj = array();
      if ($this->form_validation->run() == FALSE) {
          $msj = array(
              'codigo' => 0,
              'msg' => validation_errors()
          );
      }
      else{
        date_default_timezone_set('America/Mexico_City');
        $date = date('Y-m-d H:i:s');
        $id_candidato = $this->input->post('id_candidato');
        $id_usuario = $this->session->userdata('id');

        $data['egresos'] = $this->candidato_model->revisionEgresos($id_candidato);
        if($data['egresos']){
            $datos = array(
                'edicion' => $date,
                'id_usuario' => $id_usuario,
                'renta' => $this->input->post('renta'),
                'alimentos' => $this->input->post('alimentos'),
                'servicios' => $this->input->post('servicios'),
                'transporte' => $this->input->post('transportes'),
                'otros' => $this->input->post('otros_gastos'),
                'solvencia' => $this->input->post('solvencia')
            );
            foreach ($data['egresos'] as $dato) {
                $id = $dato->id;
            }
            $this->candidato_model->editarEgresos($datos, $id);
        }
        else{
            $datos = array(
                'creacion' => $date,
                'edicion' => $date,
                'id_usuario' => $id_usuario,
                'id_candidato' => $id_candidato,
                'renta' => $this->input->post('renta'),
                'alimentos' => $this->input->post('alimentos'),
                'servicios' => $this->input->post('servicios'),
                'transporte' => $this->input->post('transportes'),
                'otros' => $this->input->post('otros_gastos'),
                'solvencia' => $this->input->post('solvencia')
            );
            $id = $this->candidato_model->guardarEgresos($datos);
        }
        $msj = array(
          'codigo' => 1,
          'msg' => 'success'
        );
      }
      echo json_encode($msj);
    }
    function guardarHabitacion(){
      $this->form_validation->set_rules('tiempo_residencia', 'Tiempo de residencia en el domicilio actual', 'required|trim');
      $this->form_validation->set_rules('nivel_zona', 'Nivel de la zona', 'required|trim');
      $this->form_validation->set_rules('tipo_vivienda', 'Tipo de vivienda', 'required|trim');
      $this->form_validation->set_rules('recamaras', 'Recámaras', 'required|trim|numeric');
      $this->form_validation->set_rules('banios', 'Baños', 'required|trim');
      $this->form_validation->set_rules('distribucion', 'Distribución', 'required|trim');
      $this->form_validation->set_rules('calidad_mobiliario', 'Calidad mobiliario', 'required|trim|numeric');
      $this->form_validation->set_rules('mobiliario', 'Mobiliario', 'required|trim');
      $this->form_validation->set_rules('tamanio_vivienda', 'Tamaño vivienda', 'required|trim');
      $this->form_validation->set_rules('condiciones_vivienda', 'Condiciones de la vivienda', 'required|trim');
      
      $this->form_validation->set_message('required','El campo {field} es obligatorio');
      $this->form_validation->set_message('numeric','El campo {field} debe ser numérico');


      $msj = array();
      if ($this->form_validation->run() == FALSE) {
          $msj = array(
              'codigo' => 0,
              'msg' => validation_errors()
          );
      }
      else{
        date_default_timezone_set('America/Mexico_City');
        $date = date('Y-m-d H:i:s');
        $id_candidato = $this->input->post('id_candidato');
        $id_usuario = $this->session->userdata('id');
        $data['datos'] = $this->candidato_model->revisionHabitacion($id_candidato);
        if($data['datos']){
          $datos = array(
              'edicion' => $date,
              'id_usuario' => $id_usuario,
              'tiempo_residencia' => $this->input->post('tiempo_residencia'),
              'id_tipo_nivel_zona' => $this->input->post('nivel_zona'),
              'id_tipo_vivienda' => $this->input->post('tipo_vivienda'),
              'recamaras' => $this->input->post('recamaras'),
              'banios' => $this->input->post('banios'),
              'distribucion' => $this->input->post('distribucion'),
              'calidad_mobiliario' => $this->input->post('calidad_mobiliario'),
              'mobiliario' => $this->input->post('mobiliario'),
              'tamanio_vivienda' => $this->input->post('tamanio_vivienda'),
              'id_tipo_condiciones' => $this->input->post('condiciones_vivienda')
          );
          foreach ($data['datos'] as $dato) {
              $id = $dato->id;
          }
          $this->candidato_model->editarHabitacion($datos, $id);
        }
        else{
          $datos = array(
              'creacion' => $date,
              'edicion' => $date,
              'id_usuario' => $id_usuario,
              'id_candidato' => $id_candidato,
              'tiempo_residencia' => $this->input->post('tiempo_residencia'),
              'id_tipo_nivel_zona' => $this->input->post('nivel_zona'),
              'id_tipo_vivienda' => $this->input->post('tipo_vivienda'),
              'recamaras' => $this->input->post('recamaras'),
              'banios' => $this->input->post('banios'),
              'distribucion' => $this->input->post('distribucion'),
              'calidad_mobiliario' => $this->input->post('calidad_mobiliario'),
              'mobiliario' => $this->input->post('mobiliario'),
              'tamanio_vivienda' => $this->input->post('tamanio_vivienda'),
              'id_tipo_condiciones' => $this->input->post('condiciones_vivienda')
          );
          $id = $this->candidato_model->guardarHabitacion($datos);
        }
        $msj = array(
          'codigo' => 1,
          'msg' => 'success'
        );
      }
      echo json_encode($msj);
    }
    function guardarReferenciasVecinales(){
      $this->form_validation->set_rules('nombre', 'Nombre', 'required|trim');
      $this->form_validation->set_rules('domicilio', 'Domicilio', 'required|trim');
      $this->form_validation->set_rules('telefono', 'Teléfono', 'required|trim');
      $this->form_validation->set_rules('concepto', '¿Qué concepto tiene del aspirante?', 'required|trim');
      $this->form_validation->set_rules('familia', '¿En qué concepto tiene a la familia como vecinos?', 'required|trim');
      $this->form_validation->set_rules('civil', '¿Conoce el estado civil del aspirante? ¿Cuál es?', 'required|trim');
      $this->form_validation->set_rules('hijos', '¿Tiene hijos?', 'required|trim');
      $this->form_validation->set_rules('sabetrabaja', '¿Sabe en dónde trabaja?', 'required|trim');
      $this->form_validation->set_rules('notas', 'Notas', 'required|trim');
      
      $this->form_validation->set_message('required','El campo {field} es obligatorio');
      $this->form_validation->set_message('numeric','El campo {field} debe ser numérico');


      $msj = array();
      if ($this->form_validation->run() == FALSE) {
        $msj = array(
            'codigo' => 0,
            'msg' => validation_errors()
        );
      }
      else{
        date_default_timezone_set('America/Mexico_City');
        $date = date('Y-m-d H:i:s');
        $id_candidato = $this->input->post('id_candidato');
        $idrefvec = $this->input->post('idrefvec');
        $id_usuario = $this->session->userdata('id');
        $num = $this->input->post('num');

        if($idrefvec != ""){
          $datos = array(
              'edicion' => $date,
              'id_usuario' => $id_usuario,
              'nombre' => $this->input->post('nombre'),
              'telefono' => $this->input->post('telefono'),
              'domicilio' => $this->input->post('domicilio'),
              'concepto_candidato' => $this->input->post('concepto'),
              'concepto_familia' => $this->input->post('familia'),
              'civil_candidato' => $this->input->post('civil'),
              'hijos_candidato' => $this->input->post('hijos'),
              'sabe_trabaja' => $this->input->post('sabetrabaja'),
              'notas' => $this->input->post('notas')
          );
          $this->candidato_model->updateReferenciaVecinal($idrefvec, $datos);
          $msj = array(
            'codigo' => 1,
            'msg' => 'success'
          );
        }
        else{
            $datos = array(
                'creacion' => $date,
                'edicion' => $date,
                'id_usuario' => $id_usuario,
                'id_candidato' => $id_candidato,
                'nombre' => $this->input->post('nombre'),
                'telefono' => $this->input->post('telefono'),
                'domicilio' => $this->input->post('domicilio'),
                'concepto_candidato' => $this->input->post('concepto'),
                'concepto_familia' => $this->input->post('familia'),
                'civil_candidato' => $this->input->post('civil'),
                'hijos_candidato' => $this->input->post('hijos'),
                'sabe_trabaja' => $this->input->post('sabetrabaja'),
                'notas' => $this->input->post('notas')
            );
            $id = $this->candidato_model->insertReferenciaVecinal($datos);
            $msj = array(
              'codigo' => 2,
              'msg' => $id
            );
        }
      }
      echo json_encode($msj);
    }
    function terminarProceso(){
      $this->form_validation->set_rules('personal1', 'Descripción de datos personales', 'required|trim');
      $this->form_validation->set_rules('personal2', 'Descripción de hábitos y referencias personales', 'required|trim');
      $this->form_validation->set_rules('laboral1', 'Número de referencias laborales señaladas', 'required|trim');
      $this->form_validation->set_rules('laboral2', 'Descripción de las referencias laborales', 'required|trim');
      $this->form_validation->set_rules('socio1', 'Descripción de la vivienda, zona y condiciones', 'required|trim');
      $this->form_validation->set_rules('socio2', 'Descripción de gastos y referencias vecinales', 'required|trim');
      $this->form_validation->set_rules('recomendable', 'De acuerdo a lo anterior, la persona investigada es considerada', 'required|trim');
      
      $this->form_validation->set_message('required','El campo {field} es obligatorio');
      $this->form_validation->set_message('numeric','El campo {field} debe ser numérico');

      $msj = array();
      if ($this->form_validation->run() == FALSE) {
        $msj = array(
            'codigo' => 0,
            'msg' => validation_errors()
        );
      }
      else{
        date_default_timezone_set('America/Mexico_City');
        $date = date('Y-m-d H:i:s');
        $id_candidato = $this->input->post('id_candidato');
        $id_usuario = $this->session->userdata('id');

        $num = $this->candidato_model->checkConclusionesCandidato($id_candidato);
        if($num > 0){
          $finalizado = array(
            'id_usuario' => $id_usuario,
            'descripcion_personal1' => $this->input->post('personal1'),
            'descripcion_personal2' => $this->input->post('personal2'),
            'descripcion_laboral1' => $this->input->post('laboral1'),
            'descripcion_laboral2' => $this->input->post('laboral2'),
            'descripcion_socio1' => $this->input->post('socio1'),
            'descripcion_socio2' => $this->input->post('socio2'),
            'recomendable' => $this->input->post('recomendable')
          );
          $this->candidato_model->editarProcesoFinalizado($finalizado, $id_candidato);
          $this->candidato_model->statusBGCCandidato($this->input->post('recomendable'), $id_candidato);
        }
        else{
          $finalizado = array(
            'creacion' => $date,
            'id_usuario' => $id_usuario,
            'id_candidato' => $id_candidato,
            'descripcion_personal1' => $this->input->post('personal1'),
            'descripcion_personal2' => $this->input->post('personal2'),
            'descripcion_laboral1' => $this->input->post('laboral1'),
            'descripcion_laboral2' => $this->input->post('laboral2'),
            'descripcion_socio1' => $this->input->post('socio1'),
            'descripcion_socio2' => $this->input->post('socio2'),
            'recomendable' => $this->input->post('recomendable')
          );
          $this->candidato_model->guardarProcesoFinalizado($finalizado);
          $this->candidato_model->statusBGCCandidato($this->input->post('recomendable'), $id_candidato);

          $data['usuarios_cliente'] = $this->candidato_model->getCorreoCliente($id_candidato);
          $data['usuarios_subcliente'] = $this->candidato_model->getCorreoSubCliente($id_candidato);
          if($data['usuarios_cliente']){
            foreach($data['usuarios_cliente'] as $cliente){
              $from = $this->config->item('smtp_user');
              $to = $cliente->correo;
              $subject = "RODI - Proceso finalizado del candidato: ".$cliente->candidato;
              $datos['candidato'] = $cliente->candidato;
              $message = $this->load->view('correos/proceso_finalizado_espanol',$datos,TRUE);
                      
              $this->load->library('phpmailer_lib');
              $mail = $this->phpmailer_lib->load();
              $mail->isSMTP();
              $mail->Host     = 'rodi.com.mx';
              $mail->SMTPAuth = true;
              $mail->Username = 'rodicontrol@rodi.com.mx';
              $mail->Password = 'RRodi#2019@';
              $mail->SMTPSecure = 'ssl';
              $mail->Port     = 465;
              $mail->setFrom('rodicontrol@rodi.com.mx', 'Rodi');
              $mail->addAddress($to);
              $mail->Subject = $subject;
              $mail->isHTML(true);
              $mailContent = $message;
              $mail->Body = $mailContent;
              $mail->send();
            }
          }
          if($data['usuarios_subcliente']){
            foreach($data['usuarios_subcliente'] as $subcliente){
              $from = $this->config->item('smtp_user');
              $to = $subcliente->correo;
              $subject = "RODI - Proceso finalizado del candidato: ".$subcliente->candidato;
              $datos['candidato'] = $subcliente->candidato;
              $message = $this->load->view('correos/proceso_finalizado_espanol',$datos,TRUE);
                      
              $this->load->library('phpmailer_lib');
              $mail = $this->phpmailer_lib->load();
              $mail->isSMTP();
              $mail->Host     = 'rodi.com.mx';
              $mail->SMTPAuth = true;
              $mail->Username = 'rodicontrol@rodi.com.mx';
              $mail->Password = 'RRodi#2019@';
              $mail->SMTPSecure = 'ssl';
              $mail->Port     = 465;
              $mail->setFrom('rodicontrol@rodi.com.mx', 'Rodi');
              $mail->addAddress($to);
              $mail->Subject = $subject;
              $mail->isHTML(true);
              $mailContent = $message;
              $mail->Body = $mailContent;
              $mail->send();
            }
          }
          $msj = array(
            'codigo' => 1,
            'msg' => 'success'
          );
        }
      }
      echo json_encode($msj);
    }
    function crearPDF(){
      $mpdf = new \Mpdf\Mpdf();
      date_default_timezone_set('America/Mexico_City');
      $data['hoy'] = date("d-m-Y");
      $hoy = date("d-m-Y");
      $id_candidato = $_POST['idPDF'];
      $data['datos'] = $this->candidato_model->getInfoCandidato($id_candidato);
      foreach($data['datos'] as $row){
          $f = $row->fecha_alta;
          $ffin = $row->fecha_fin;
          $nombreCandidato = $row->nombre." ".$row->paterno." ".$row->materno;
          $cliente = $row->cliente;
          $subcliente = $row->subcliente;
          $id_doping = $row->idDoping;
          $id_cliente = $row->id_cliente;
      }
      $fecha_fin = formatoFechaEspanol($ffin);
      $f_alta = formatoFechaEspanol($f);
      $hoy = formatoFecha($hoy);
      $data['finalizado'] = $this->candidato_model->getDatosFinalizadosCandidato($id_candidato);
      $data['doping'] = $this->candidato_model->getDopingCandidato($id_candidato);
      $data['pruebas'] = $this->candidato_model->getPruebasCandidato($id_candidato);
      $data['docs'] = $this->candidato_model->getDocumentacionCandidato($id_candidato);
      $data['academico'] = $this->candidato_model->getEstudiosCandidato($id_candidato);
      $data['sociales'] = $this->candidato_model->getAntecedentesSociales($id_candidato);
      $data['familia'] = $this->candidato_model->getFamiliares($id_candidato);
      $data['egresos'] = $this->candidato_model->getEgresosFamiliares($id_candidato);
      $data['vivienda'] = $this->candidato_model->getDatosVivienda($id_candidato);
      $data['ref_personal'] = $this->candidato_model->getReferenciasPersonales($id_candidato);
      $data['ref_vecinal'] = $this->candidato_model->getReferenciasVecinales($id_candidato);
      $data['legal'] = $this->candidato_model->getVerificacionLegal($id_candidato);
      $data['nom'] = $this->candidato_model->getTrabajosNoMencionados($id_candidato);
      $data['finalizado'] = $this->candidato_model->getDatosFinalizadosCandidato($id_candidato);
      $data['det_estudio'] = $this->candidato_model->getStatusVerificacionEstudios($id_candidato);
      $data['ref_laboral'] = $this->candidato_model->getAntecedentesLaborales($id_candidato);
      $data['analista'] = $this->candidato_model->getAnalista($id_candidato);
      $data['coordinadora'] = $this->candidato_model->getCoordinadora($id_candidato);
      $data['cliente'] = $cliente;
      $data['subcliente'] = $subcliente;
      $data['fecha_fin'] = $ffin;
      $html = $this->load->view('pdfs/candidato_general_pdf',$data,TRUE);
      $mpdf->setAutoTopMargin = 'stretch';
      $mpdf->AddPage();
      if($id_cliente == 39){
          $mpdf->SetHTMLHeader('<div style="width: 33%; float: left;"><img style="height: 100px;" src="'.base_url().'img/logo_talink.png"></div><div style="width: 33%; float: right;text-align: right;">Fecha de Registro: '.$f_alta.'<br>Fecha de Elaboración: '.$fecha_fin.'</div>');
          $mpdf->SetHTMLFooter('<div style="position: absolute; left: 20px; bottom: 10px; color: rgba(0,0,0,0.5);"><p style="font-size: 10px;">4-EST-001.Rev. 01 <br>Fecha de Rev. 05/06/2020</p></div>');
          
      }
      else{
          $mpdf->SetHTMLHeader('<div style="width: 33%; float: left;"><img style="height: 50px;" src="'.base_url().'img/logo.png"></div><div style="width: 33%; float: right;text-align: right;">Fecha de Registro: '.$f_alta.'<br>Fecha de Elaboración: '.$fecha_fin.'</div>');
          $mpdf->SetHTMLFooter('<div style="position: absolute; left: 20px; bottom: 10px; color: rgba(0,0,0,0.5);"><p style="font-size: 10px;">Calle Benito Juarez # 5693, Col. Santa María del Pueblito <br>Zapopan, Jalisco C.P. 45018 <br>Tel. (33) 2301-8599<br><br>4-EST-001.Rev. 01 <br>Fecha de Rev. 05/06/2020</p></div><div style="position: absolute; right: 0;  bottom: 0;"><img class="" src="'.base_url().'img/logo_pie.png"></div>');
      }        
      $mpdf->WriteHTML($html);

      $mpdf->Output('Estudio_'.$cliente.'-'.$nombreCandidato.'.pdf','D'); // opens in browser
    }
}
