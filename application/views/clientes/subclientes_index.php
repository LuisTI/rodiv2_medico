<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<title><?php echo strtoupper($this->session->userdata('subcliente')); ?> | RODI</title>
	<link rel="stylesheet" href="<?php echo base_url() ?>css/bootstrap.min.css">
	<link rel="stylesheet" href="<?php echo base_url() ?>css/cliente/style.css">
	<link rel="stylesheet" href="<?php echo base_url() ?>css/bootstrap-datetimepicker.min.css">
	<!-- Google Font -->
	<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
	<script src="https://kit.fontawesome.com/fdf6fee49b.js"></script>
	<!-- DataTable -->
	<link rel="stylesheet" href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
	<link rel="icon" type="image/jpg" href="<?php echo base_url() ?>img/favicon.jpg" sizes="64x64">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">
	<script src="https://kit.fontawesome.com/fdf6fee49b.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
	<!--link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css"-->
	<!--link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css"-->

</head>
<body>
	<div id="exito" class="alert alert-success in mensaje" style='display:none;'>
      	<strong>Success!</strong> The candidate has been add succesfully
  	</div>
  	<div id="mensaje" class="alert alert-success in mensaje" style='display:none;'>
      	<strong>Success!</strong><p id="texto_msj"></p>
  	</div>
  	<div class="modal fade" id="newModal" role="dialog" data-backdrop="static" data-keyboard="false">
	  	<div class="modal-dialog modal-lg" role="document">
		    <div class="modal-content">
		      	<div class="modal-header">
		      		<h4 class="modal-title">Nuevo Registro</h4>
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			          <span aria-hidden="true">&times;</span>
			        </button>
		      	</div>
		      	<div class="modal-body">
		        	<?php 
					echo form_open('Candidato/addCandidate', 'id="datos"'); ?>
						<div class="row">
							<div class="col-md-12">
								<label for="puesto">Puesto *</label>
			        			<select name="puesto" id="puesto" class="form-control obligado">
						            <option value="">Selecciona</option>
						            <?php 
						            foreach ($puestos as $p) { ?>
					            		<option value="<?php echo $p->id; ?>"><?php echo $p->nombre; ?></option>
						            <?php	
						            } ?>
					          	</select>
					          	<br><br>
							</div>
						</div>
			        	<div class="row">
			        		<div class="col-md-4">
			        			<label for="nombre">Nombre(s) *</label>
			        			<input type="text" class="form-control obligado" name="nombre" id="nombre" placeholder="Nombre(s)" onKeyUp="document.getElementById(this.id).value=document.getElementById(this.id).value.toUpperCase()">
			        			<br>
			        		</div>
			        		<div class="col-md-4">
			        			<label for="paterno">Apellido paterno *</label>
			        			<input type="text" class="form-control obligado" name="paterno" id="paterno" placeholder="Apellido Paterno" onKeyUp="document.getElementById(this.id).value=document.getElementById(this.id).value.toUpperCase()">
			        			<br>
			        		</div>
			        		<div class="col-md-4">
			        			<label for="materno">Apellido materno *</label>
			        			<input type="text" class="form-control obligado" name="materno" id="materno" placeholder="Apellido materno" onKeyUp="document.getElementById(this.id).value=document.getElementById(this.id).value.toUpperCase()">
			        			<br>
			        		</div>			    
			        	</div>
			        	<div class="row">
			        		<div class="col-md-4">
			        			<label for="correo">Correo electrónico</label>
			        			<input type="email" class="form-control" name="correo" id="correo" placeholder="Email" onKeyUp="document.getElementById(this.id).value=document.getElementById(this.id).value.toLowerCase()">
			        			<br>
			        		</div>
			        		<div class="col-md-4">
			        			<label for="celular">Tel. Celular *</label>
			        			<input type="text" class="form-control solo_numeros obligado" name="celular" id="celular" placeholder="Tel. Celular" maxlength="10">
			        			<br>
			        		</div>
			        		<div class="col-md-4">
			        			<label for="fijo">Tel. Casa </label>
			        			<input type="text" class="form-control solo_numeros" name="fijo" id="fijo" placeholder="Tel. Casa" maxlength="10">
			        			<br>
			        		</div>
			        	</div>
			        	<h4 class="text-center">Selecciona los estudios que requiere el candidato:</h4><br><br>
			        	<div class="row">
			        		<div class="col-md-4 div_estudio">
			        			<label class="contenedor_check fuente-14">Socioeconómico
	                                <input type="checkbox" name="socio" id="socio" checked disabled>
	                                <span class="checkmark"></span>
	                            </label>
	                            <br>
	                        </div>
	                        <div class="col-md-4 div_estudio">
	                        	<label class="contenedor_check fuente-14">Antidoping
	                                <input type="checkbox" name="antidoping" id="antidoping">
	                                <span class="checkmark"></span>
	                            </label>
	                            <br>
	                        </div>
	                        <div class="col-md-4 div_estudio">
	                        	<label class="contenedor_check fuente-14">Psicométrico
	                                <input type="checkbox" name="psicometrico" id="psicometrico">
	                                <span class="checkmark"></span>
	                            </label>
	                            <br>
	                        </div>
	                    </div>
	                    <div class="row">
	                    	<div class="col-md-4 div_estudio">
			        			<label class="contenedor_check fuente-14">Médico
	                                <input type="checkbox" name="medico" id="medico">
	                                <span class="checkmark"></span>
	                            </label>
	                            <br>
	                        </div>
	                        <div class="col-md-4 div_estudio">
			        			<label class="contenedor_check fuente-14">Buró de Crédito
	                                <input type="checkbox" name="buro" id="buro">
	                                <span class="checkmark"></span>
	                            </label>
	                            <br>
	                        </div>
	                        <div class="col-md-4 div_estudio">
			        			<label class="contenedor_check fuente-14">Sociolaboral
	                                <input type="checkbox" name="laboral" id="laboral">
	                                <span class="checkmark"></span>
	                            </label>
	                            <br><br>
	                        </div>
	                    </div>
			        	<div class="row">
			        		<div class="col-md-6">
								<label for="examen">Examen antidoping *</label>
			        			<select name="examen" id="examen" class="form-control" disabled>
						            <option value="" selected>Selecciona</option>
						            <?php 
						            foreach ($drogas as $d) { ?>
					            		<option value="<?php echo $d->id; ?>"><?php echo $d->nombre." (".$d->conjunto.")"; ?></option>
						            <?php	
						            } ?>
					          	</select>
					          	<br><br>
							</div>
			        		<div class="col-md-6">
			        			<label for="otro_requisito">¿Requiere algo más para el candidato?</label>
			        			<textarea class="form-control" name="otro_requisito" id="otro_requisito" rows="3" placeholder="Escriba su mensaje"></textarea>
			        			<br>
			        		</div>
			        	</div>
			        	<div id="div_antidoping" class="padding-40">
			        		
			        	</div>
			        	<div id="div_psicometrico" class="padding-40">
			        		
			        	</div>
			        	<div id="div_buro" class="padding-40">
			        		
			        	</div>
			        	<div class="row">
			        		<div class="col-md-4 offset-md-4">
			        			<label for="cv">Cargar CV o solicitud de empleo del candidato</label>
			        			<input type="file" id="cv" name="cv" class="form-control" accept=".pdf, .jpg, .jpeg, .png" multiple><br>
			        		</div>
			        	</div>
			        <?php if(!empty(validation_errors())): ?>
						<div class="alert alert-danger in mensaje">
						  	<?php echo validation_errors(); ?>
						</div>
					<?php endif; ?>
					<div id="alert-msg">
						
					</div>
			        <div id="campos_vacios" class="alert alert-danger">
		      			<p class="msj_error">Hay campos vacíos</p>
		      		</div>
		      		<div id="repetido">
		      			<p class="msj_error">El nombre y/o el email del candidato ya existen</p>
		      		</div>
		      	</div>
		      	<div class="modal-footer">
			        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
			        <button type="submit" class="btn btn-success" name="submit" id="registrar">Registrar</button>
			        <?php echo form_close(); ?>
		      	</div>
		    </div>
	 	</div>
	</div>
	<div class="modal fade" id="quitarModal" role="dialog" data-backdrop="static" data-keyboard="false">
	  	<div class="modal-dialog modal-dialog-centered">
	    	<div class="modal-content">
	      		<div class="modal-header">
	      			<h4 class="modal-title" id="titulo_accion"></h4>
	        		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			          <span aria-hidden="true">&times;</span>
			        </button>
	      		</div>
	      		<div class="modal-body">
	        		<p class="" id="texto_confirmacion"></p><br>
	        		<div class="row" id="div_commentario">
	        			<div class="col-md-12">
	        				<label for="motivo">Comment *</label>
	        				<textarea name="motivo" id="motivo" class="form-control" rows="3"></textarea>
	        				<br>
	        			</div>
	        		</div>
	        		<div class="msj_error">
	        			<p id="msg_accion"></p>
	        		</div>
	    		</div>
		    	<div class="modal-footer">
		      		<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
		      		<button type="button" class="btn btn-danger" id="btnGuardar" onclick="ejecutarAccion()">Accept</button>
		    	</div>
	  		</div>
		</div>
	</div>
	<div class="modal fade" id="statusModal" role="dialog" data-backdrop="static" data-keyboard="false">
	  	<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
		    <div class="modal-content">
		      	<div class="modal-header">
		        	<h4 class="modal-title">Estatus actual del estudio del candidato</h4>
			        <button type="button" class="close" data-dismiss="modal" aria-label="Cerrar">
			          <span aria-hidden="true">&times;</span>
			        </button>
		      	</div>
		      	<div class="modal-body">
		      		<p id="nombreCandidato" class="text-center"></p>
		      		<div id="div_status"></div>
		      	</div>
		      	<div class="modal-footer">
			        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
		      	</div>
		    </div>
	 	</div>
	</div>
	<div class="modal fade" id="llamadasModal" role="dialog" data-backdrop="static" data-keyboard="false">
	  	<div class="modal-dialog modal-lg" role="document">
		    <div class="modal-content">
		      	<div class="modal-header">
		        	<h4 class="modal-title">Llamadas al Candidato </h4>
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			          <span aria-hidden="true">&times;</span>
			        </button>
		      	</div>
		      	<div class="modal-body">
		      		<div id="div_llamadas"></div>
		      	</div>
		      	<div class="modal-footer">
			        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
		      	</div>
		    </div>
	 	</div>
	</div>
	<div class="modal fade" id="emailsModal" role="dialog" data-backdrop="static" data-keyboard="false">
	  	<div class="modal-dialog modal-lg" role="document">
		    <div class="modal-content">
		      	<div class="modal-header">
		        	<h4 class="modal-title">Correos enviados al Candidato</h4>
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			          <span aria-hidden="true">&times;</span>
			        </button>
		      	</div>
		      	<div class="modal-body">
		      		<div id="div_emails"></div>
		      	</div>
		      	<div class="modal-footer">
			        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
		      	</div>
		    </div>
	 	</div>
	</div>
	<div class="modal fade" id="verModal" role="dialog" data-backdrop="static" data-keyboard="false">
	  	<div class="modal-dialog modal-dialog-centered">
	    	<div class="modal-content">
	      		<div class="modal-header">
	      			<h4 class="modal-title">Candidate comments</h4>
	        		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			          <span aria-hidden="true">&times;</span>
			        </button>
	      		</div>
	      		<div class="modal-body">
	        		<p class="" id="comentario_candidato"></p><br>
	    		</div>
		    	<div class="modal-footer">
		      		<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
		    	</div>
	  		</div>
		</div>
	</div>
	<div class="modal fade" id="avancesModal" role="dialog" data-backdrop="static" data-keyboard="false">
	  	<div class="modal-dialog modal-lg" role="document">
		    <div class="modal-content">
		      	<div class="modal-header">
		        	<h4 class="modal-title">Avances en el estudio del candidato </h4>
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			          <span aria-hidden="true">&times;</span>
			        </button>
		      	</div>
		      	<div class="modal-body">
		      		<div id="div_avances"></div>
		      	</div>
		      	<div class="modal-footer">
			        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
		      	</div>
		    </div>
	 	</div>
	</div>
	<header>
		<nav class="navbar navbar-expand-lg navbar-light bg-rodi">
		  	<a class="navbar-brand" href="#">
		  		<img src="<?php echo base_url() ?>/img/favicon.jpg" width="32" height="32" class="d-inline-block align-top">
		  		<?php echo strtoupper($this->session->userdata('subcliente')); ?>
		  	</a>
		  	<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
		    <span class="navbar-toggler-icon"></span>
		  	</button>
		  	<div class="collapse navbar-collapse" id="navbarNavDropdown">
			    <ul class="navbar-nav ml-auto">
			    	<?php 
			    	if($this->session->userdata('idsubcliente') != 180){ ?>
				    	<li>
				    		<a class="btn btn-app btn_acciones" id="btn_nuevo" data-toggle="modal" data-target="#newModal">
					    		<i class="fas fa-plus-circle"></i> <span>Registrar candidato</span>
					  		</a>
				    	</li>
			    	<?php } ?>
			    	<li class="nav-item dropdown">
				        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-user icon"></i>
				          <?php echo $this->session->userdata('nombre')." ".$this->session->userdata('paterno'); ?>
				        </a>
				        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
				          <a class="dropdown-item" href="<?php echo base_url(); ?>Login/logout">Cerrar sesión</a>
				        </div>
				    </li>
			    </ul>
		  	</div>
		</nav>
	</header>
	<div class="loader" style="display: none;"></div>
	<section>
		<div class="container">
			<div class="row">
				<div class="col-lg-6 offset-lg-3">
					<div class="div_titulo">
						<p class="titulo">Tabla de Registro de Candidatos</p>
					</div>
				</div>
			</div>
			<div class="row tabla">
				<div class="col-lg-12">
					<input type="hidden" class="idCandidato">
					<input type="hidden" class="correo">
					<table id="tabla" class="table stripe hover row-border cell-border table-responsive"></table>
				</div>
			</div>
		</div>
	</section>
	<script src="<?php echo base_url() ?>js/jquery-3.3.1.min.js"></script>
	<script src="<?php echo base_url() ?>js/bootstrap.min.js"></script>
	<script src="<?php echo base_url() ?>js/clients.js"></script>

	<script src="<?php echo base_url() ?>js/bootstrap-datetimepicker.min.js"></script>
	<script src="<?php echo base_url() ?>js/icheck.js"></script>
	<script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>
	<script>
    $(document).ready(function(){
    	var url = '<?php echo base_url('Subcliente/getCandidatos'); ?>';
		var psico = '<?php echo base_url(); ?>_psicometria/';
    	
		$('#tabla').DataTable({
			"pageLength": 10,
	      	"pagingType": "simple",
	      	"order": [0, "desc"],
	      	//"stateSave": true,
	      	"serverSide": false,
	      	"ajax": url,
	      	"bDestroy": true,
	      	"columns":[ 
	      		{ 	title: 'id', data: 'id', visible: false },
		        { 	title: 'Candidato', data: 'nombreCompleto', "width": "25%",
		        	mRender: function(data, type, full){
		        		return full.nombre+"<br>"+full.paterno+" "+full.materno;
		        	}
		        },
		        { title: 'Fecha de alta', data: 'fecha_alta',
	        		mRender: function(data, type, full){
            			var f = data.split(' ');
            			var h = f[1];
            			var aux = h.split(':');
            			var hora = aux[0]+':'+aux[1];
            			var aux = f[0].split('-');
            			var fecha = aux[2]+"/"+aux[1]+"/"+aux[0];
            			var tiempo = fecha+' '+hora;
        				return tiempo;
          			}
	        	},
	        	{ title: 'Visita', data: 'visitador', "width": "12%",
	        		mRender: function(data, type, full){
	        			if(full.id_subcliente != 180){
	        				if(data == 1){
			        			return "<i class='fas fa-circle estatus1'></i>Registrada";
			        		}
			        		else{
		        				return "<i class='fas fa-circle estatus0'></i>Pendiente";
		        			}
	        			}
	        			else{
	        				return "N/A";
	        			}
          			}
	        	},
	          	
	        	{ title: 'Estatus', data: 'id', "width": "8%",
	        		mRender: function(data, type, full){
	        			if(full.status == 0){
	        				return '<a href="javascript:void(0)" data-toggle="tooltip" data-accion="3" title="Mensajes de avances" id="msj_avances" class="fa-tooltip a-acciones"><i class="fas fa-comment-dots"></i></a><a href="javascript:void(0)" id="ver" data-toggle="tooltip" title="Ver estatus del candidato" class="fa-tooltip a-acciones"><i class="fas fa-eye"></i></a>';
	        			}
	        			if(full.status == 2){
		      				return "<i class='fas fa-circle status_bgc1'></i>Finalizado";
		      			}
          			}
	        	},
	          	
		        { title: 'Acciones', data: 'id', bSortable: false, "width": "8%",
         			mRender: function(data, type, full) {
     					if(full.status == 0){
     						if(full.id_cliente == 39){
     							if(full.id_subcliente != 180){
     								if(full.id_grado_estudio != 0 && full.id_grado_estudio != null){
		     							return '<a href="javascript:void(0)" id="llamada" data-toggle="tooltip" title="Llamadas al candidato" class="fa-tooltip a-acciones"><i class="fas fa-phone"></i></a><a href="javascript:void(0)" id="correoEnviado" data-toggle="tooltip" title="Correos al candidato" class="fa-tooltip a-acciones"><i class="fas fa-envelope"></i></a><div style="display: inline-block;"><form id="pdf'+data+'" action="<?php echo base_url('Candidato/crearPrevioPDF'); ?>" method="POST"><a href="javascript:void(0);" data-toggle="tooltip" title="Descargar previo del estudio" id="pdfPrevio" class="fa-tooltip a-acciones"><i class="far fa-file-powerpoint"></i></a><input type="hidden" name="idPrevio" id="idPrevio'+data+'" value="'+data+'"></form></div>';
		     						}
		     						else{
		     							return '<a href="javascript:void(0)" id="llamada" data-toggle="tooltip" title="Llamadas al candidato" class="fa-tooltip a-acciones"><i class="fas fa-phone"></i></a><a href="javascript:void(0)" id="correoEnviado" data-toggle="tooltip" title="Correos al candidato" class="fa-tooltip a-acciones"><i class="fas fa-envelope"></i></a>';
		     						}
     							}
     							else{
     								return '<a href="javascript:void(0)" id="llamada" data-toggle="tooltip" title="Llamadas al candidato" class="fa-tooltip a-acciones"><i class="fas fa-phone"></i></a><a href="javascript:void(0)" id="correoEnviado" data-toggle="tooltip" title="Correos al candidato" class="fa-tooltip a-acciones"><i class="fas fa-envelope"></i></a>';
     							}
     						}
     						else{
     							if(full.id_grado_estudio != 0 && full.id_grado_estudio != null){
	     							return '<a href="javascript:void(0)" id="llamada" data-toggle="tooltip" title="Llamadas al candidato" class="fa-tooltip a-acciones"><i class="fas fa-phone"></i></a><a href="javascript:void(0)" id="correoEnviado" data-toggle="tooltip" title="Correos al candidato" class="fa-tooltip a-acciones"><i class="fas fa-envelope"></i></a>';
	     						}
	     						else{
	     							return '<a href="javascript:void(0)" id="llamada" data-toggle="tooltip" title="Llamadas al candidato" class="fa-tooltip a-acciones"><i class="fas fa-phone"></i></a><a href="javascript:void(0)" id="correoEnviado" data-toggle="tooltip" title="Correos al candidato" class="fa-tooltip a-acciones"><i class="fas fa-envelope"></i></a>';
	     						}
     						}
            			}
            			if(full.status == 2){
            				if(full.id_subcliente != 180){
            					return '<div style="display: inline-block;"><form id="pdf'+data+'" action="<?php echo base_url('Candidato/crearEstudioPDF'); ?>" method="POST"><a href="javascript:void(0);" data-toggle="tooltip" title="Descargar documento final" id="pdfFinal" class="fa-tooltip a-acciones"><i class="fas fa-file-pdf"></i></a><input type="hidden" name="idPDF" id="idPDF'+data+'" value="'+data+'"></form></div>';
            				}
            				else{
            					return '<div style="display: inline-block;"><form id="pdf'+data+'" action="<?php echo base_url('Candidato/crearEstudioSubclienteInglesPDF'); ?>" method="POST"><a href="javascript:void(0);" data-toggle="tooltip" title="Descargar documento final" id="pdfFinal" class="fa-tooltip a-acciones"><i class="fas fa-file-pdf"></i></a><input type="hidden" name="idPDF" id="idPDF'+data+'" value="'+data+'"></form></div>';
            				}
            				
            			}
          			}
        		},
		        { title: 'Antidoping', data: 'id', bSortable: false, "width": "8%",
         			mRender: function(data, type, full) {
         				if(full.tipo_antidoping == 1){
         					if(full.fecha_resultado != null && full.fecha_resultado != ""){
	         					if(full.resultado_doping == 1){
	         						return '<i class="fas fa-circle status_bgc2"></i>No aprobado <div style="display: inline-block;margin-left:10px;"><form id="pdfForm'+full.idDoping+'" action="<?php echo base_url('Doping/createPDF'); ?>" method="POST"><a href="javascript:void(0);" data-toggle="tooltip" title="Descargar resultado" id="pdfDoping" class="fa-tooltip a-acciones"><i class="fas fa-file-pdf"></i></a><input type="hidden" name="idDop" id="idDop'+full.idDoping+'" value="'+full.idDoping+'"></form></div>'; 
	         					}
	         					else{
	         						return '<i class="fas fa-circle status_bgc1"></i>Aprobado <div style="display: inline-block;margin-left:10px;"><form id="pdfForm'+full.idDoping+'" action="<?php echo base_url('Doping/createPDF'); ?>" method="POST"><a href="javascript:void(0);" data-toggle="tooltip" title="Descargar resultado" id="pdfDoping" class="fa-tooltip a-acciones"><i class="fas fa-file-pdf"></i></a><input type="hidden" name="idDop" id="idDop'+full.idDoping+'" value="'+full.idDoping+'"></form></div>';
	         					}
	         				}
	         				else{
	         					return "Pendiente";
	         				}
         				}
         				if(full.tipo_antidoping == 0 || full.tipo_antidoping == "" || full.tipo_antidoping == null){
         					return "N/A";
         				}
          			}
        		},
        		{ title: 'Médico', data: 'id', width: '10%',
	        		mRender: function(data, type, full){
                        if(full.medico == 1){
                            if(full.imagen != null && full.conclusion != null){
								return '<div style="display: inline-block;"><form id="med'+full.idMedico+'" action="<?php echo base_url('Medico/crearPDF'); ?>" method="POST"><a href="javascript:void(0);" data-toggle="tooltip" title="Descargar documento final" id="pdfMedico" class="fa-tooltip a-acciones"><i class="fas fa-file-pdf"></i></a><input type="hidden" name="idMedico" id="idMedico'+full.idMedico+'" value="'+full.idMedico+'"></form></div>';
                            }
                            else{
                                return "<i class='fas fa-circle status_bgc0'></i> En proceso";
                            }
                        }
                        else{
	        				return "N/A";
                        }
	        		}
				},
        		{ title: 'Psicometrico', data: 'id', bSortable: false, "width": "13%",
         			mRender: function(data, type, full) {
         				if(full.psicometrico == 1){
         					if(full.archivo != null && full.archivo != ""){
								return '<a href="'+psico+full.archivo+'" target="_blank" download="'+full.archivo+'" data-toggle="tooltip" title="Descargar psicometrico" id="descarga_psicometrico" class="fa-tooltip a-acciones"><i class="fas fa-file-powerpoint"></i></a>'; 
	         				}
	         				else{
								return 'Pendiente';
	         				}
         				}
         				else{
         					return "N/A";
         				}
          			}
        		},
		        { title: 'Resultado', data: 'id', bSortable: false, "width": "12%",
         			mRender: function(data, type, full) {
     					if(full.status == 0){
     						return '<i class="fas fa-circle status_bgc0"></i>En proceso';
     					}
     					else{
     						if(full.status_bgc == 0){
	         					return '<a href="javascript:void(0)" data-toggle="tooltip" title="Finalizar proceso del candidato" id="final" class="fa-tooltip a-acciones"><i class="fas fa-user-check"></i></a>';
	         				}
	         				if(full.status_bgc == 1){
	         					return "<i class='fas fa-circle status_bgc1'></i>Aprobado";
	         				}
	         				if(full.status_bgc == 2){
	         					return "<i class='fas fa-circle status_bgc2'></i>No aprobado";
	         				}
	         				if(full.status_bgc == 3){
	         					return "<i class='fas fa-circle status_bgc3'></i>A consideración";
	         				}
     					}
     				}
        		}          
      		],
      		
	      	fnDrawCallback: function (oSettings) {
	        	$('a[data-toggle="tooltip"]').tooltip({ trigger: "hover" });
	      	},
	      	rowCallback: function( row, data ) {
	      		$("a#ver", row).bind('click', () => {
      				var salida = "";
	      			var visitado = (data.visitador == 0)? "<tr><th>Documentación</th><th>En proceso</th></tr><tr><th>Datos del grupo familiar</th><th>En proceso</th></tr><tr><th>Egresos mensuales</th><th>En proceso</th></tr><tr><th>Habitación y medio ambiente</th><th>En proceso</th></tr><tr><th>Referencias vecinales</th><th>En proceso</th></tr>":"<tr><th>Documentación</th><th>Terminado</th></tr><tr><th>Datos del grupo familiar</th><th>Terminado</th></tr><tr><th>Egresos mensuales</th><th>Terminado</th></tr><tr><th>Habitación y medio ambiente</th><th>Terminado</th></tr><tr><th>Referencias vecinales</th><th>Terminado</th></tr>";

	      			var estudios = (data.idEstudios == "" || data.idEstudios == null)? "<tr><th>Historial académico </th><th>En proceso</th></tr>":"<tr><th>Historial académico </th><th>Terminado</th></tr>";
        			var sociales = (data.idSociales == "" || data.idSociales == null)? "<tr><th>Antecedentes sociales </th><th>En proceso</th></tr>":"<tr><th>Antecedentes sociales </th><th>Terminado</th></tr>";
        			var personales = (data.idPersonales == "" || data.idPersonales == null)? "<tr><th>Referencias personales </th><th>En proceso</th></tr>":"<tr><th>Referencias personales </th><th>Terminado</th></tr>";
        			var laborales = (data.idLaborales == "" || data.idLaborales == null)? "<tr><th>Antecedentes laborales </th><th>En proceso</th></tr>":"<tr><th>Antecedentes laborales </th><th>Terminado</th></tr>";
        			var legales = (data.idLegales == "" || data.idLegales == null)? "<tr><th>Investigación legal </th><th>En proceso</th></tr>":"<tr><th>Investigación legal </th><th>Terminado</th></tr>";

	      			salida += '<table class="table table-striped">';
			        salida += '<thead>';
			        salida += '<tr>';
			        salida += '<th scope="col">Concepto</th>';
			        salida += '<th scope="col">Estatus</th>';
			        salida += '</tr>';
			        salida += '</thead>';
			        salida += '<tbody>';
			        salida += visitado;
			        salida += estudios;
			        salida += sociales;
			        salida += personales;
			        salida += laborales;
			        salida += legales;
                	salida += '</tbody>';
        			salida += '</table>';

        			$("#nombreCandidato").text("Nombre del candidato: "+data.nombreCompleto);
        			$("#div_status").html(salida);
        			$("#statusModal").modal("show");
      			
	      		});
	      		$("a#ofac", row).bind('click', () => {
	      			$(".idCandidato").val(data.id);
	      			$("#idCliente").val(data.id_cliente);
	      			$("#ofac_nombrecandidato").text(data.nombre+" "+data.paterno+" "+data.materno);
	      			estatusOFAC();
	      		});
	      		$('a[id^=pdfDoping]', row).bind('click', () => {
		            var id = data.idDoping;
		            $('#pdfForm'+id).submit();
		        });
	      		$("a#cancelar", row).bind('click', () => {
	      			$(".idCandidato").val(data.id);
	      			$("#titulo_accion").text("Cancel candidate");
	      			$("#texto_confirmacion").html("Are you sure you want to cancel <b>"+data.nombre+" "+data.paterno+" "+data.materno+"</b>?");
	      			$("#btnGuardar").attr('value','cancel');
	      			$("#div_commentario").css('display','block');
	      			$("#quitarModal").modal("show");
	      		});
	      		$("a#eliminar", row).bind('click', () => {
	      			$(".idCandidato").val(data.id);
	      			$("#titulo_accion").text("Delete candidate");
	      			$("#texto_confirmacion").html("Are you sure you want to delete <b>"+data.nombre+" "+data.paterno+" "+data.materno+"</b>?");
	      			$("#btnGuardar").attr('value','delete');
	      			$("#div_commentario").css('display','block');
	      			$("#quitarModal").modal("show");
	      		});
	      		$("a#generar", row).bind('click', () => {
	      			$(".idCandidato").val(data.id);
	      			$(".correo").val(data.correo);
	      			$("#titulo_accion").text("Generate password");
	      			$("#texto_confirmacion").html("Are you sure you want to generate other password for <b>"+data.nombre+" "+data.paterno+" "+data.materno+"</b>?");
	      			$("#btnGuardar").attr('value','generate');
	      			$("#div_commentario").css('display','none');
	      			$("#quitarModal").modal("show");
	      		});
	      		$('a[id^=pdfFinal]', row).bind('click', () => {
		            var id = data.id;
		            $('#pdf'+id).submit();
		        });
		        $("a#msj_avances", row).bind('click', () => {
	      			$.ajax({
			            url: '<?php echo base_url('Candidato/viewAvances'); ?>',
			            type: 'post',
			            data: {'id_candidato':data.id,'id_cliente':data.id_cliente},
			            success : function(res)
			            { 
			            	$("#div_avances").html(res);

			            }
			        });
	      			$("#avancesModal").modal("show");
	      		});
		        $("a#llamada", row).bind('click', () => {
	      			$.ajax({
			            url: '<?php echo base_url('Candidato/viewLlamadas'); ?>',
			            type: 'post',
			            data: {'id_candidato':data.id,'id_cliente':data.id_cliente},
			            success : function(res)
			            { 
			            	$("#div_llamadas").html(res);

			            }
			        });
	      			$("#llamadasModal").modal("show");
	      		});
	      		$("a#correoEnviado", row).bind('click', () => {
	      			$.ajax({
			            url: '<?php echo base_url('Candidato/viewEmails'); ?>',
			            type: 'post',
			            data: {'id_candidato':data.id,'id_cliente':data.id_cliente},
			            success : function(res)
			            { 
			            	$("#div_emails").html(res);

			            }
			        });
	      			$("#emailsModal").modal("show");
	      		});
	      		$('a#comentario', row).bind('click', () => {
		            $.ajax({
			            url: '<?php echo base_url('index.php/Candidato/viewComentario'); ?>',
			            type: 'post',
			            data: {'id_candidato':data.id},
			            success : function(res)
			            { 
			            	if(res != 0){
				            	$("#comentario_candidato").html(res);
				            	$("#verModal").modal('show');
			            	}
			            	else{
				            	$("#comentario_candidato").html("No comments");
				            	$("#verModal").modal('show');
			            	}
			            	

			            }
			        });
		        });
		        $('a#documentos', row).bind('click', () => {
		            $.ajax({
			            url: '<?php echo base_url('Candidato/viewDocumentos'); ?>',
			            type: 'post',
			            data: {'id_candidato':data.id},
			            success : function(res)
			            { 
			            	if(res != 0){
			            		$("#lista_documentos").empty();
				            	$("#lista_documentos").html(res);
				            	$("#documentosModal").modal('show');
			            	}
			            	else{
			            		$("#lista_documentos").empty();
				            	$("#lista_documentos").html("<p class='text-center'><b>Documents under review</b></p>");
				            	$("#documentosModal").modal('show');
			            	}
			            	

			            }
			        });
		        });
      		},
			"language": {
		        "lengthMenu": "Mostrar _MENU_ registros por página",
		        "zeroRecords": "No se encontraron registros",
		        "info": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
		        "infoEmpty": "Sin registros disponibles",
		        "infoFiltered": "(Filtrado _MAX_ registros totales)",
		        "sSearch": 'Search: <i class="fas fa-search"></i>',
		        "oPaginate": {
		          "sLast": "Última página",
		          "sFirst": "Primera",
		          "sNext": "<i class='fa  fa-arrow-right'></i>",
		          "sPrevious": "<i class='fa fa-arrow-left'></i>"
		        }
		    }
		});
		$("#tabla").DataTable().search(" ");
		$("#proyecto").change(function(){
	      	var id_proyecto = $(this).val();
	      	if(id_proyecto != "" && id_proyecto != 0){
	        	$.ajax({
	          		url: '<?php echo base_url('Subcliente/getPaqueteSubclienteProyecto'); ?>',
	          		method: 'POST',
	          		data: {'id_proyecto':id_proyecto},
	          		dataType: "text",
	          		success: function(res)
	          		{
	          			if(res != ""){
	          				//$('#examen').prop('disabled', false);
	            			$('#examen').html(res);
	          			}
	          		}
	        	});
	      	}
	      	else{
	      		$('#examen').empty();
	      		$('#examen').append($("<option selected></option>").attr("value","").text("Select"));
	      	}	      	
	    });
	    $("#antidoping").change(function(){
			if(this.checked){
				$("#examen").prop('disabled',false);
				$("#examen").addClass('obligado');
			}
			else{
				$("#examen").prop('disabled',true);
				$("#examen").val("");
				$("#examen").removeClass('obligado');
			}
		});
		$("#registrar").click(function(e){
			e.preventDefault();
			var id_cliente = '<?php echo $this->session->userdata('idcliente') ?>';
			var id_subcliente = '<?php echo $this->session->userdata('idsubcliente') ?>';
			var correo = $('#correo').val();

			var datos = new FormData();
			datos.append('usuario', 3);
			datos.append('subcliente', id_subcliente);
	        datos.append('nombre', $("#nombre").val());
	        datos.append('paterno', $("#paterno").val());
	        datos.append('materno', $("#materno").val());
	        datos.append('correo', $("#correo").val());
	        datos.append('celular', $("#celular").val());
	        datos.append('fijo', $("#fijo").val());
	        datos.append('puesto', $("#puesto").val());
	        datos.append('examen', $("#examen").val());
	        datos.append('otro', $("#otro_requisito").val());
	        datos.append('id_cliente', id_cliente);
	        //datos.append('cv', $("#cv")[0].files[0]);
	        var num_files = document.getElementById('cv').files.length;
            if(num_files > 0){
	        	datos.append("hay_cvs", 1);
	        	for(var x = 0; x < num_files; x++) {
	                datos.append("cvs[]", document.getElementById('cv').files[x]);
	            }
	        }
	        else{
	        	datos.append("hay_cvs", 0);
	        }

	        //datos.append('socio', $("input:checkbox[id='socio']:checked").val());
			datos.append('medico', $("input:checkbox[id='medico']:checked").val());
			datos.append('laboral', $("input:checkbox[id='laboral']:checked").val());
			datos.append('antidoping', $("input:checkbox[id='antidoping']:checked").val());
			datos.append('psicometrico', $("input:checkbox[id='psicometrico']:checked").val());
			datos.append('buro', $("input:checkbox[id='buro']:checked").val());

			var totalVacios = $('.obligado').filter(function(){
	        	return !$(this).val();
	      	}).length;

	      	if(totalVacios > 0){
	        	$(".obligado").each(function() {
	          		var element = $(this);
	          		if (element.val() == "") {
	            		element.addClass("requerido");
	            		$("#newModal #campos_vacios .msj_error").css('color','white');
	            		$("#newModal #campos_vacios").css('display','block');
			            setTimeout(function(){
			              $('#newModal #campos_vacios').fadeOut();
			            },4000);
	          		}
	          		else{
	            		element.removeClass("requerido");
	          		}
	        	});
	        	if($("input:checkbox[id='medico']:checked").val() == undefined &&
	      			$("input:checkbox[id='laboral']:checked").val() == undefined &&
	      			$("input:checkbox[id='antidoping']:checked").val() == undefined && 
	      			$("input:checkbox[id='psicometrico']:checked").val() == undefined && 
	      			$("input:checkbox[id='buro']:checked").val() == undefined){
	          		$(".div_estudio").addClass("requerido");
	          		$("#newModal #campos_vacios").css('display','block');
		            setTimeout(function(){
		              $('#newModal #campos_vacios').fadeOut();
		            },4000);
	          	}
	          	else{
	          		$(".div_estudio").removeClass("requerido");
	          		$('#newModal #campos_vacios').css('display','none');
	          	}
	      	}
	      	else{
	      		if(!isEmail(correo) && correo != ""){
	      			$('#newModal #campos_vacios').css("display", "none");
	      			$("#newModal #correo_invalido .msj_error").css('color','white');
	      			$('#newModal #correo_invalido').css("display", "block");
	      			$("#correo").addClass("requerido");
	      			setTimeout(function(){
	        			$("#newModal #correo_invalido").fadeOut();
	      			},4000);
	    		}
	    		else{
		     		$.ajax({
		              	url: '<?php echo base_url('Candidato/registrarCandidatoEspanol'); ?>',
		              	type: 'POST',
		              	data: datos,
		              	contentType: false,  
		     			cache: false,  
		     			processData:false,
		              	beforeSend: function() {
		                	$('.loader').css("display","block");
		              	},
		              	success : function(res){ 
		              		if(res == 1){
		              			setTimeout(function(){
			                  		$('.loader').fadeOut();
			                  	},300);
			                  	$("#newModal").modal('hide');
			          			$('#newModal #correo_invalido').css("display", "none");
			          			$("#texto_msj").text(" El candidato ha sido registrado correctamente");
			          			$("#mensaje").css('display','block');
			          			recargarTable();
			          			setTimeout(function(){
			            			$("#mensaje").fadeOut();
			          			},6000);
			          			/*$('#repetido').css("display", "block");
			          			setTimeout(function(){
			            			$("#repetido").fadeOut();
			          			},4000);*/
		              		}
		              		if(res == 0){
		              			setTimeout(function(){
			                  		$('.loader').fadeOut();
			                  	},300);
			                  	//$("#newModal").modal('hide');
		              			$('#newModal #campos_vacios').css("display", "none");
			          			$('#newModal #correo_invalido').css("display", "none");
			          			$("#newModal #repetido .msj_error").css('color','white');
			          			$("#newModal #repetido").css("display", "block");
			          			//recargarTable();
			          			setTimeout(function(){
			            			$("#newModal #repetido").fadeOut();
			          			},6000);
			          			/*$('#repetido').css("display", "block");
			          			setTimeout(function(){
			            			$("#repetido").fadeOut();
			          			},4000);*/
		              		}
		              		if(res != 0 && res != 1){
		              			setTimeout(function(){
			                  		$('.loader').fadeOut();
			                  	},200);
		              			$('#alert-msg').html('<div class="alert alert-danger">' + res + '</div>');              			
		          			}
		          			
		              	}
		        	});
	        	}
	      	}

		});
    });
	function estatusOFAC(){
		var id_candidato = $(".idCandidato").val();
		var f = new Date();
		var dia = f.getDate();
		var mes = (f.getMonth() +1);
		var dia = (dia < 10) ? '0'+dia : dia;
		var mes = (mes < 10) ? '0'+mes : mes;
		var h = f.getHours();
		var m = f.getMinutes();
		$.ajax({
      		url: '<?php echo base_url('Candidato/checkOfac'); ?>',
      		method: 'POST',
      		data: {'id_candidato':id_candidato},
      		dataType: "text",
      		success: function(res)
      		{
      			$("#fecha_estatus_ofac").empty();
      			$("#estatus_ofac").empty();
      			$("#res_ofac").empty();
      			$("#estatus_oig").empty();
      			$("#res_oig").empty();
  				var datos = res.split('@@');
  				if(datos[0] == 0){
  					$("#fecha_titulo_ofac").html("<b>No date</b>");
  					$("#estatus_ofac").html("<b>OFAC Status: </b>Not defined yet");
	    			$("#res_ofac").html("<b>Result:</b> Not defined yet");
	    			$("#estatus_oig").html("<b>OIG Status: </b>Not defined yet");
	    			$("#res_oig").html("<b>Result:</b> Not defined yet");
  				}
  				else{
  					$("#fecha_titulo_ofac").html("<b>Last update</b>");
  					$("#fecha_estatus_ofac").text(datos[0]);
  					$("#estatus_ofac").html("<b>OFAC Status:</b> "+datos[1]);
	    			var res_ofac = (datos[2] == 1)? "Positive":"Negative";
	    			$("#res_ofac").html("<b>Result:</b> "+res_ofac);
	    			$("#estatus_oig").html("<b>OIG Status:</b> "+datos[3]);
	    			var res_oig = (datos[4] == 1)? "Positive":"Negative";
	    			$("#res_oig").html("<b>Result:</b> "+res_oig);
  				}

      		},error:function(res)
      		{
        		//$('#errorModal').modal('show');
      		}
    	});
		$("#ofacModal").modal("show");
	}
	function ejecutarAccion(){
		var accion = $("#btnGuardar").val();
		var id_candidato = $(".idCandidato").val();
		var correo = $(".correo").val();
		var motivo = $("#motivo").val();
		if(accion == 'cancel'){
			if(motivo == ""){
				$("#msg_accion").text("The comment is required");
				$("#msg_accion").css('display','block');
				setTimeout(function(){
              		$('#msg_accion').fadeOut();
            	},5000);
			}
			else{
				$.ajax({
	              	url: '<?php echo base_url('index.php/Candidato/cancel'); ?>',
	              	type: 'post',
	              	data: {'id_candidato':id_candidato,'motivo':motivo},
	              	beforeSend: function() {
	                	$('.loader').css("display","block");
	              	},
	              	success : function(res){ 
	              		setTimeout(function(){
	                  		$('.loader').fadeOut();
	                	},300);
	            		$("#quitarModal").modal('hide');
	            		recargarTable();
	            		$("#texto_msj").text('The candidate has been cancelled succesfully');
	            		$("#mensaje").css('display','block');
	            		setTimeout(function(){
	                  		$('#mensaje').fadeOut();
	                	},3000);
	              	},error: function(res){
	                	$('#errorModal').modal('show');
	              	}
	        	});
			}
		}
		if(accion == 'delete'){
			if(motivo == ""){
				$("#msg_accion").text("The comment is required");
				$("#msg_accion").css('display','block');
				setTimeout(function(){
              		$('#msg_accion').fadeOut();
            	},5000);
			}
			else{
				$.ajax({
	              	url: '<?php echo base_url('index.php/Candidato/delete'); ?>',
	              	type: 'post',
	              	data: {'id_candidato':id_candidato,'motivo':motivo},
	              	beforeSend: function() {
	                	$('.loader').css("display","block");
	              	},
	              	success : function(res){ 
	              		setTimeout(function(){
	                  		$('.loader').fadeOut();
	                	},300);
	            		$("#quitarModal").modal('hide');
	            		recargarTable();
	            		$("#texto_msj").text('The candidate has been deleted succesfully');
	            		$("#mensaje").css('display','block');
	            		setTimeout(function(){
	                  		$('#mensaje').fadeOut();
	                	},3000);
	              	},error: function(res){
	                	$('#errorModal').modal('show');
	              	}
	        	});
			}
		}
		if(accion == 'generate'){
			$.ajax({
              	url: '<?php echo base_url('index.php/Candidato/generate'); ?>',
              	type: 'post',
              	data: {'id_candidato':id_candidato,'correo':correo},
              	beforeSend: function() {
                	$('.loader').css("display","block");
              	},
              	success : function(res){ 
              		setTimeout(function(){
                  		$('.loader').fadeOut();
                	},300);
            		$("#quitarModal").modal('hide');
            		$("#user").text(correo);
            		$("#pass").text(res);
            		$("#respuesta_mail").text("* An email has been sent with this credentials to the candidate. This email could take a few minutes to be delivered.");
            		$("#passModal").modal('show');
            		recargarTable();
            		$("#texto_msj").text('The password has been created succesfully');
            		$("#mensaje").css('display','block');
            		setTimeout(function(){
                  		$('#mensaje').fadeOut();
                	},3000);
              	},error: function(res){
                	$('#errorModal').modal('show');
              	}
        	});
		}
	}
    //Verificacion de correo
	function isEmail(email) {
	    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
	    return regex.test(email);
	}
	function recargarTable(){
    	$("#tabla").DataTable().ajax.reload();
  	}
  	$('#quitarModal').on('hidden.bs.modal', function (e) {
	  	$("#msg_accion").css('display','none');
		$(this)
		    .find("input,textarea")
		       .val('')
		       .end();
	});
	$('#newModal').on('hidden.bs.modal', function (e) {
		$("#alert-msg").empty();
	  	$("#alert-msg").css('display','none');
	  	$("#campos_vacios").css('display','none');
	  	$("#correo_invalido").css('display','none');
	  	$("#repetido").css('display','none');
	  	$(".obligado").removeClass("requerido");
		$(this)
		    .find("input,select")
		       .val('')
		       .end();
	});
	var hoy = new Date();
  	var dd = hoy.getDate();
  	var mm = hoy.getMonth()+1;
  	var yyyy = hoy.getFullYear();
  	var hora = hoy.getHours()+":"+hoy.getMinutes();

  	if(dd<10) {
      	dd='0'+dd;
  	} 

  	if(mm<10) {
      	mm='0'+mm;
  	}
	$("#fecha_nacimiento").datetimepicker({
  		minView: 2,
    	format: "mm/dd/yyyy",
    	startView: 4,
    	autoclose: true,
    	todayHighlight: true,
    	pickerPosition: "bottom-left",
    	forceParse: false
  	});
  	$('#fecha_nacimiento').datetimepicker('setEndDate', (yyyy - 18)+'-'+'01-01');
	</script>
</body>
</html>