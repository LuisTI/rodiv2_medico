<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reporte extends CI_Controller{

	function __construct(){
		parent::__construct();
	}

	function index(){
        $datos['candidatos'] = $this->doping_model->getCandidatosSinDoping();
        $datos['paquetes'] = $this->doping_model->getPaquetesAntidoping();
        $datos['clientes'] = $this->doping_model->getClientes();
        $datos['identificaciones'] = $this->doping_model->getTiposIdentificaciones();
        $data['permisos'] = $this->usuario_model->getPermisos($this->session->userdata('id'));
        $data['permisos_subclientes'] = $this->usuario_model->getPermisosSubclientes($this->session->userdata('id'));
        $data['accesos'] = $this->usuario_model->getModulos($this->session->userdata('idrol'));
        $datos['usuarios'] = $this->usuario_model->getUsuarios();
        foreach($data['accesos'] as $acceso) {
            $items[] = $acceso->id_operaciones;
        }
        $data['acceso'] = $items;
		$this->load
		->view('adminpanel/header',$data)
		->view('adminpanel/scripts')
		->view('reportes/reportes_index',$datos)
		->view('adminpanel/footer');
	}
    function getSubclientes(){
        $id_cliente = $_POST['id_cliente'];
        $data['subclientes'] = $this->reporte_model->getSubclientes($id_cliente);
        $salida = "<option value=''>Selecciona Subcliente</option>";
        if($data['subclientes']){
            $salida .= "<option value='0'>TODOS</option>";
            foreach ($data['subclientes'] as $row){
                $salida .= "<option value='".$row->id."'>".$row->nombre."</option>";
            } 
            echo $salida;
        }
        else{
            $salida .= "<option value=''>N/A</option>";
            echo $salida;
        }
    }
    function getProyectos(){
        $id_cliente = $_POST['id_cliente'];
        $data['proyectos'] = $this->doping_model->getProyectos($id_cliente);
        $salida = "<option value=''>Selecciona Proyecto</option>";
        if($data['proyectos']){
            $salida .= "<option value='0'>TODOS</option>";
            foreach ($data['proyectos'] as $row){
                $salida .= "<option value='".$row->id."'>".$row->nombre."</option>";
            } 
            echo $salida;
        }
        else{
            $salida .= "<option value=''>N/A</option>";
            echo $salida;
        }
    }
    function reporteDopingFinalizados(){
        $f_inicio = fecha_espanol_bd($_POST['fi']);
        $f_fin = fecha_espanol_bd($_POST['ff']);
        $cliente = $_POST['cliente'];
        $subcliente = $_POST['subcliente'];
        $proyecto = $_POST['proyecto'];
        $resultado = $_POST['res'];
        $lab = $_POST['lab'];

        $data['datos'] = $this->reporte_model->reporteDopingFinalizados($f_inicio, $f_fin, $cliente, $subcliente, $proyecto, $resultado, $lab);
        //var_dump($data['datos']);
        if($data['datos']){
            $salida = '<div style="text-align:center;margin-bottom:50px;"><a class="btn btn-success" href="'.base_url().'Reporte/reporteDopingFinalizados_Excel/'.$f_inicio.'_'.$f_fin.'_'.$cliente.'_'.$subcliente.'_'.$proyecto.'_'.$resultado.'_'.$lab.'" target="_blank"><i class="fas fa-file-excel"></i> Exportar a Excel</a></div>';
            $salida .= '<table style="border: 0px; border-collapse: collapse;width: 100%;padding:5px;">';
            $salida .= '<tr>';
            $salida .= '<th style"border: 1px solid #a4a6a5;text-align: left;padding: 6px;">Fecha doping</th>';
            $salida .= '<th style"border: 1px solid #a4a6a5;text-align: left;padding: 6px;">Nombre</th>';
            $salida .= '<th style"border: 1px solid #a4a6a5;text-align: left;padding: 6px;">Cliente</th>';
            $salida .= '<th style"border: 1px solid #a4a6a5;text-align: left;padding: 6px;">Subcliente</th>';
            $salida .= '<th style"border: 1px solid #a4a6a5;text-align: left;padding: 6px;">Proyecto</th>';
            $salida .= '<th style"border: 1px solid #a4a6a5;text-align: left;padding: 6px;">Examen</th>';
            $salida .= '<th style"border: 1px solid #a4a6a5;text-align: left;padding: 6px;">Código</th>';
            $salida .= '<th style"border: 1px solid #a4a6a5;text-align: left;padding: 6px;">Fecha resultado</th>';
            $salida .= '<th style"border: 1px solid #a4a6a5;text-align: left;padding: 6px;">Resultado</th>';
            $salida .= '<th style"border: 1px solid #a4a6a5;text-align: left;padding: 6px;">Laboratorio</th>';
            $salida .= '</tr>';
            foreach($data['datos'] as $row){
                $subcliente = ($row->subcliente != "" && $row->subcliente != null)? $row->subcliente:"-";
                $proyecto = ($row->proyecto != "" && $row->proyecto != null)? $row->proyecto:"-";
                $f_doping = $this->reporteFecha($row->fecha_doping);
                $f_resultado = ($row->fecha_resultado != "" && $row->fecha_resultado != null)? $this->reporteFecha($row->fecha_resultado):"Sin resultado";
                $res = ($row->resultado == 1)? "Positivo":"Negativo";
                $salida .= "<tr><tbody>";
                $salida .= '<td style"border: 1px solid #a4a6a5;text-align: left;padding: 6px;">'.$f_doping.'</td>';
                $salida .= '<td style"border: 1px solid #a4a6a5;text-align: left;padding: 6px;">'.$row->candidato.'</td>';
                $salida .= '<td style"border: 1px solid #a4a6a5;text-align: left;padding: 6px;">'.$row->cliente.'</td>';
                $salida .= '<td style"border: 1px solid #a4a6a5;text-align: left;padding: 6px;">'.$subcliente.'</td>';
                $salida .= '<td style"border: 1px solid #a4a6a5;text-align: left;padding: 6px;">'.$proyecto.'</td>';
                $salida .= '<td style"border: 1px solid #a4a6a5;text-align: left;padding: 6px;">'.$row->parametros.'</td>';
                $salida .= '<td style"border: 1px solid #a4a6a5;text-align: left;padding: 6px;">'.$row->codigo_prueba.'</td>';
                $salida .= '<td style"border: 1px solid #a4a6a5;text-align: left;padding: 6px;">'.$f_resultado.'</td>';
                $salida .= '<td style"border: 1px solid #a4a6a5;text-align: left;padding: 6px;">'.$res.'</td>';
                $salida .= '<td style"border: 1px solid #a4a6a5;text-align: left;padding: 6px;">'.$row->laboratorio.'</td>';
                $salida .= "</tbody></tr>";
            }
            $salida .= "</table>";
        }
        else{
            $salida = '<p style="text-align:center;font-size:18px;font-weight:bold;">Sin registros de acuerdo a los filtros aplicados</p>';
        }
        echo $salida;
    }
    function reporteDopingFinalizados_Excel(){
        $datos = $this->uri->segment(3);
        $dato = explode('_', $datos);
        $f_inicio = $dato[0];
        $f_fin = $dato[1];
        $cliente = $dato[2];
        $subcliente = $dato[3];
        $proyecto = $dato[4];
        $resultado = $dato[5];
        $lab = $dato[6];
        //var_dump($datos);
        $data['datos'] = $this->reporte_model->reporteDopingFinalizados($f_inicio, $f_fin, $cliente, $subcliente, $proyecto, $resultado, $lab);
        if($data['datos']){
            //Cargamos la librería de excel.
            $this->load->library('excel');
            //$mobiledata = $this->export->mobileList();
            $excel = new PHPExcel();
            $excel->setActiveSheetIndex(0);
            //$excel->getActiveSheet()->setTitle('Reporte_RegistrosDoping');
            //Contador de filas
            $contador = 1;
            //Le aplicamos ancho las columnas.
            $excel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
            $excel->getActiveSheet()->getColumnDimension('B')->setWidth(100);
            $excel->getActiveSheet()->getColumnDimension('C')->setWidth(50);
            $excel->getActiveSheet()->getColumnDimension('D')->setWidth(50);
            $excel->getActiveSheet()->getColumnDimension('E')->setWidth(50);
            $excel->getActiveSheet()->getColumnDimension('F')->setWidth(20);
            $excel->getActiveSheet()->getColumnDimension('G')->setWidth(30);
            $excel->getActiveSheet()->getColumnDimension('H')->setWidth(20);
            $excel->getActiveSheet()->getColumnDimension('I')->setWidth(20);
            $excel->getActiveSheet()->getColumnDimension('J')->setWidth(30);
            //Le aplicamos negrita a los títulos de la cabecera.
            $excel->getActiveSheet()->getStyle("A{$contador}")->getFont()->setBold(true);
            $excel->getActiveSheet()->getStyle("B{$contador}")->getFont()->setBold(true);
            $excel->getActiveSheet()->getStyle("C{$contador}")->getFont()->setBold(true);
            $excel->getActiveSheet()->getStyle("D{$contador}")->getFont()->setBold(true);
            $excel->getActiveSheet()->getStyle("E{$contador}")->getFont()->setBold(true);
            $excel->getActiveSheet()->getStyle("F{$contador}")->getFont()->setBold(true);
            $excel->getActiveSheet()->getStyle("G{$contador}")->getFont()->setBold(true);
            $excel->getActiveSheet()->getStyle("H{$contador}")->getFont()->setBold(true);
            $excel->getActiveSheet()->getStyle("I{$contador}")->getFont()->setBold(true);
            $excel->getActiveSheet()->getStyle("J{$contador}")->getFont()->setBold(true);
            //Definimos los títulos de la cabecera.
            $excel->getActiveSheet()->setCellValue("A{$contador}", 'FECHA DOPING');
            $excel->getActiveSheet()->setCellValue("B{$contador}", 'NOMBRE');
            $excel->getActiveSheet()->setCellValue("C{$contador}", 'CLIENTE');
            $excel->getActiveSheet()->setCellValue("D{$contador}", 'SUBCLIENTE');
            $excel->getActiveSheet()->setCellValue("E{$contador}", 'PROYECTO');
            $excel->getActiveSheet()->setCellValue("F{$contador}", 'EXAMEN');
            $excel->getActiveSheet()->setCellValue("G{$contador}", 'CÓDIGO');
            $excel->getActiveSheet()->setCellValue("H{$contador}", 'FECHA RESULTADO');
            $excel->getActiveSheet()->setCellValue("I{$contador}", 'RESULTADO');
            $excel->getActiveSheet()->setCellValue("J{$contador}", 'LABORATORIO');
            //Definimos la data del cuerpo.        
            foreach($data['datos'] as $row){
                $subcliente = ($row->subcliente != "" && $row->subcliente != null)? $row->subcliente:"-";
                $proyecto = ($row->proyecto != "" && $row->proyecto != null)? $row->proyecto:"-";
                $f_doping = $this->reporteFecha($row->fecha_doping);
                $f_resultado = ($row->fecha_resultado != "" && $row->fecha_resultado != null)? $this->reporteFecha($row->fecha_resultado):"Sin resultado";
                $res = ($row->resultado == 1)? "Positivo":"Negativo";
               //Incrementamos una fila más, para ir a la siguiente.
               $contador++;
               //Informacion de las filas de la consulta.
               $excel->getActiveSheet()->setCellValue("A{$contador}", $f_doping);
               $excel->getActiveSheet()->setCellValue("B{$contador}", $row->candidato);
               $excel->getActiveSheet()->setCellValue("C{$contador}", $row->cliente);
               $excel->getActiveSheet()->setCellValue("D{$contador}", $subcliente);
               $excel->getActiveSheet()->setCellValue("E{$contador}", $proyecto);
               $excel->getActiveSheet()->setCellValue("F{$contador}", $row->parametros);
               $excel->getActiveSheet()->setCellValue("G{$contador}", $row->codigo_prueba);
               $excel->getActiveSheet()->setCellValue("H{$contador}", $f_resultado);
               $excel->getActiveSheet()->setCellValue("I{$contador}", $res);
               $excel->getActiveSheet()->setCellValue("J{$contador}", $row->laboratorio);
            }
            //Le ponemos un nombre al archivo que se va a generar.
            $archivo = "Reporte1_RegistrosDopingFinalizados.xls";
            $objWriter = new PHPExcel_Writer_Excel2007($excel);
            $objWriter->save($archivo);
            // download file
            header("Content-Type: application/vnd.ms-excel");
            redirect(site_url().$archivo);    
            /*$archivo = "Reporte_RegistrosDoping.xls";
            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename="'.$archivo.'"');
            header('Cache-Control: max-age=0');
            $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
            //Hacemos una salida al navegador con el archivo Excel.
            $objWriter->save('php://output');*/
        }
        /*else{
            $contador = 2;
            $this->excel->getActiveSheet()->setCellValue("A{$contador}", "SIN REGISTROS");
        }*/
    }
    function reporteEstudiosFinalizados(){
        $f_inicio = fecha_espanol_bd($_POST['fi']);
        $f_fin = fecha_espanol_bd($_POST['ff']);
        $cliente = $_POST['cliente'];
        $usuario = $_POST['usuario'];

        $salida = '<div style="text-align:center;margin-bottom:50px;"><a class="btn btn-success" href="'.base_url().'Reporte/reporteEstudiosFinalizados_Excel/'.$f_inicio.'_'.$f_fin.'_'.$cliente.'_'.$usuario.'" target="_blank"><i class="fas fa-file-excel"></i> Exportar a Excel</a></div>';
        $salida .= '<table style="border: 0px; border-collapse: collapse;width: 100%;padding:5px;">';
        $salida .= '<tr>';
        $salida .= '<th style"border: 1px solid #a4a6a5;text-align: left;padding: 6px;">Fecha alta</th>';
        $salida .= '<th style"border: 1px solid #a4a6a5;text-align: left;padding: 6px;">Fecha finalizado</th>';
        $salida .= '<th style"border: 1px solid #a4a6a5;text-align: left;padding: 6px;">Analista</th>';
        $salida .= '<th style"border: 1px solid #a4a6a5;text-align: left;padding: 6px;">Candidato</th>';
        $salida .= '<th style"border: 1px solid #a4a6a5;text-align: left;padding: 6px;">Cliente</th>';
        $salida .= '<th style"border: 1px solid #a4a6a5;text-align: left;padding: 6px;">SLA</th>';
        $salida .= '</tr>';
        $salida .= "<tbody>";

        if($usuario == 0){
            $data['usuarios'] = $this->reporte_model->getUsuarios();
            foreach($data['usuarios'] as $user){
                if($cliente == 0){
                    $data['data'] = $this->reporte_model->getClientes();
                    foreach($data['data'] as $cl){
                        if($cl->id == 1 || $cl->id == 2){
                            $data['datos1'] = $this->reporte_model->reporteFinalizados_HCL_UST($f_inicio, $f_fin, $cl->id, $user->id);
                            if($data['datos1']){
                                foreach($data['datos1'] as $row){
                                    $f_alta = $this->reporteFecha($row->fecha_alta);
                                    $f_final = $this->reporteFecha($row->fecha_final);
                                    $salida .= '<tr>';
                                    $salida .= '<td style"border: 1px solid #a4a6a5;text-align: left;padding: 6px;">'.$f_alta.'</td>';
                                    $salida .= '<td style"border: 1px solid #a4a6a5;text-align: left;padding: 6px;">'.$f_final.'</td>';
                                    $salida .= '<td style"border: 1px solid #a4a6a5;text-align: left;padding: 6px;">'.$row->usuario.'</td>';
                                    $salida .= '<td style"border: 1px solid #a4a6a5;text-align: left;padding: 6px;">'.$row->candidato.'</td>';
                                    $salida .= '<td style"border: 1px solid #a4a6a5;text-align: left;padding: 6px;">'.$row->cliente.'</td>';
                                    $salida .= '<td style"border: 1px solid #a4a6a5;text-align: left;padding: 6px;">'.$row->tiempo.' días</td>';
                                    $salida .= '</tr>';
                                }
                                $band = 0;
                            }
                            else{
                                $band = 1;
                            }
                        }
                        elseif($cl->id == 3 || $cl->id == 77){
                            $data['datos2'] = $this->reporte_model->reporteFinalizados_TATA_WIPRO($f_inicio, $f_fin, $cl->id, $user->id);
                            if($data['datos2']){
                                foreach($data['datos2'] as $row){
                                    $f_alta = $this->reporteFecha($row->fecha_alta);
                                    $f_final = $this->reporteFecha($row->fecha_final);
                                    $salida .= '<tr>';
                                    $salida .= '<td style"border: 1px solid #a4a6a5;text-align: left;padding: 6px;">'.$f_alta.'</td>';
                                    $salida .= '<td style"border: 1px solid #a4a6a5;text-align: left;padding: 6px;">'.$f_final.'</td>';
                                    $salida .= '<td style"border: 1px solid #a4a6a5;text-align: left;padding: 6px;">'.$row->usuario.'</td>';
                                    $salida .= '<td style"border: 1px solid #a4a6a5;text-align: left;padding: 6px;">'.$row->candidato.'</td>';
                                    $salida .= '<td style"border: 1px solid #a4a6a5;text-align: left;padding: 6px;">'.$row->cliente.'</td>';
                                    $salida .= '<td style"border: 1px solid #a4a6a5;text-align: left;padding: 6px;">'.$row->tiempo_parcial.' días</td>';
                                    $salida .= '</tr>';
                                }
                                $band = 0;
                            }
                            else{
                                $band = 1;
                            }
                        }
                        else{
                            $data['datos3'] = $this->reporte_model->reporteFinalizados_Espanol($f_inicio, $f_fin, $cl->id, $user->id);
                            if($data['datos3']){
                                foreach($data['datos3'] as $row){
                                    $f_alta = $this->reporteFecha($row->fecha_alta);
                                    $f_final = $this->reporteFecha($row->fecha_final);
                                    $salida .= '<tr>';
                                    $salida .= '<td style"border: 1px solid #a4a6a5;text-align: left;padding: 6px;">'.$f_alta.'</td>';
                                    $salida .= '<td style"border: 1px solid #a4a6a5;text-align: left;padding: 6px;">'.$f_final.'</td>';
                                    $salida .= '<td style"border: 1px solid #a4a6a5;text-align: left;padding: 6px;">'.$row->usuario.'</td>';
                                    $salida .= '<td style"border: 1px solid #a4a6a5;text-align: left;padding: 6px;">'.$row->candidato.'</td>';
                                    $salida .= '<td style"border: 1px solid #a4a6a5;text-align: left;padding: 6px;">'.$row->cliente.'</td>';
                                    $salida .= '<td style"border: 1px solid #a4a6a5;text-align: left;padding: 6px;">'.$row->tiempo.' días</td>';
                                    $salida .= '</tr>';
                                }
                                $band = 0;
                            }
                            else{
                                $band = 1;
                            }
                        }
                        
                    }
                    
                }
                else{
                    if($cliente == 1 || $cliente == 2){
                        $data['datos1'] = $this->reporte_model->reporteFinalizados_HCL_UST($f_inicio, $f_fin, $cliente, $user->id);
                        if($data['datos1']){
                            foreach($data['datos1'] as $row){
                                $f_alta = $this->reporteFecha($row->fecha_alta);
                                $f_final = $this->reporteFecha($row->fecha_final);
                                $salida .= '<tr>';
                                $salida .= '<td style"border: 1px solid #a4a6a5;text-align: left;padding: 6px;">'.$f_alta.'</td>';
                                $salida .= '<td style"border: 1px solid #a4a6a5;text-align: left;padding: 6px;">'.$f_final.'</td>';
                                $salida .= '<td style"border: 1px solid #a4a6a5;text-align: left;padding: 6px;">'.$row->usuario.'</td>';
                                $salida .= '<td style"border: 1px solid #a4a6a5;text-align: left;padding: 6px;">'.$row->candidato.'</td>';
                                $salida .= '<td style"border: 1px solid #a4a6a5;text-align: left;padding: 6px;">'.$row->cliente.'</td>';
                                $salida .= '<td style"border: 1px solid #a4a6a5;text-align: left;padding: 6px;">'.$row->tiempo.' días</td>';
                                $salida .= '</tr>';
                            }
                            $band = 0;
                        }
                        else{
                            $band = 1;
                        }
                    }
                    elseif($cliente == 3 || $cliente == 77){
                        $data['datos2'] = $this->reporte_model->reporteFinalizados_TATA_WIPRO($f_inicio, $f_fin, $cliente, $user->id);
                        if($data['datos2']){
                            foreach($data['datos2'] as $row){
                                $f_alta = $this->reporteFecha($row->fecha_alta);
                                $f_final = $this->reporteFecha($row->fecha_final);
                                $salida .= '<tr>';
                                $salida .= '<td style"border: 1px solid #a4a6a5;text-align: left;padding: 6px;">'.$f_alta.'</td>';
                                $salida .= '<td style"border: 1px solid #a4a6a5;text-align: left;padding: 6px;">'.$f_final.'</td>';
                                $salida .= '<td style"border: 1px solid #a4a6a5;text-align: left;padding: 6px;">'.$row->usuario.'</td>';
                                $salida .= '<td style"border: 1px solid #a4a6a5;text-align: left;padding: 6px;">'.$row->candidato.'</td>';
                                $salida .= '<td style"border: 1px solid #a4a6a5;text-align: left;padding: 6px;">'.$row->cliente.'</td>';
                                $salida .= '<td style"border: 1px solid #a4a6a5;text-align: left;padding: 6px;">'.$row->tiempo_parcial.' días</td>';
                                $salida .= '</tr>';
                                
                            }
                            $band = 0;
                        }
                        else{
                            $band = 1;
                        }
                    }
                    else{
                        $data['datos3'] = $this->reporte_model->reporteFinalizados_Espanol($f_inicio, $f_fin, $cliente, $user->id);
                        if($data['datos3']){
                            foreach($data['datos3'] as $row){
                                $f_alta = $this->reporteFecha($row->fecha_alta);
                                $f_final = $this->reporteFecha($row->fecha_final);
                                $salida .= '<tr>';
                                $salida .= '<td style"border: 1px solid #a4a6a5;text-align: left;padding: 6px;">'.$f_alta.'</td>';
                                $salida .= '<td style"border: 1px solid #a4a6a5;text-align: left;padding: 6px;">'.$f_final.'</td>';
                                $salida .= '<td style"border: 1px solid #a4a6a5;text-align: left;padding: 6px;">'.$row->usuario.'</td>';
                                $salida .= '<td style"border: 1px solid #a4a6a5;text-align: left;padding: 6px;">'.$row->candidato.'</td>';
                                $salida .= '<td style"border: 1px solid #a4a6a5;text-align: left;padding: 6px;">'.$row->cliente.'</td>';
                                $salida .= '<td style"border: 1px solid #a4a6a5;text-align: left;padding: 6px;">'.$row->tiempo.' días</td>';
                                $salida .= '</tr>';
                                
                            }
                            $band = 0;
                        }
                        else{
                            $band = 1;
                        }
                    }
                }
            }
            $salida .= "</tbody>";
            $salida .= "</table>";
        }
        else{
            if($cliente == 0){
                $data['data'] = $this->reporte_model->getClientes();
                foreach($data['data'] as $cl){
                    if($cl->id == 1 || $cl->id == 2){
                        $data['datos1'] = $this->reporte_model->reporteFinalizados_HCL_UST($f_inicio, $f_fin, $cl->id, $usuario);
                        if($data['datos1']){
                            foreach($data['datos1'] as $row){
                                $f_alta = $this->reporteFecha($row->fecha_alta);
                                $f_final = $this->reporteFecha($row->fecha_final);
                                $salida .= '<tr>';
                                $salida .= '<td style"border: 1px solid #a4a6a5;text-align: left;padding: 6px;">'.$f_alta.'</td>';
                                $salida .= '<td style"border: 1px solid #a4a6a5;text-align: left;padding: 6px;">'.$f_final.'</td>';
                                $salida .= '<td style"border: 1px solid #a4a6a5;text-align: left;padding: 6px;">'.$row->usuario.'</td>';
                                $salida .= '<td style"border: 1px solid #a4a6a5;text-align: left;padding: 6px;">'.$row->candidato.'</td>';
                                $salida .= '<td style"border: 1px solid #a4a6a5;text-align: left;padding: 6px;">'.$row->cliente.'</td>';
                                $salida .= '<td style"border: 1px solid #a4a6a5;text-align: left;padding: 6px;">'.$row->tiempo.' días</td>';
                                $salida .= '</tr>';
                            }
                            $band = 0;
                        }
                        else{
                            $band = 1;
                        }
                    }
                    elseif($cl->id == 3 || $cl->id == 77){
                        $data['datos2'] = $this->reporte_model->reporteFinalizados_TATA_WIPRO($f_inicio, $f_fin, $cl->id, $usuario);
                        if($data['datos2']){
                            foreach($data['datos2'] as $row){
                                $f_alta = $this->reporteFecha($row->fecha_alta);
                                $f_final = $this->reporteFecha($row->fecha_final);
                                $salida .= '<tr>';
                                $salida .= '<td style"border: 1px solid #a4a6a5;text-align: left;padding: 6px;">'.$f_alta.'</td>';
                                $salida .= '<td style"border: 1px solid #a4a6a5;text-align: left;padding: 6px;">'.$f_final.'</td>';
                                $salida .= '<td style"border: 1px solid #a4a6a5;text-align: left;padding: 6px;">'.$row->usuario.'</td>';
                                $salida .= '<td style"border: 1px solid #a4a6a5;text-align: left;padding: 6px;">'.$row->candidato.'</td>';
                                $salida .= '<td style"border: 1px solid #a4a6a5;text-align: left;padding: 6px;">'.$row->cliente.'</td>';
                                $salida .= '<td style"border: 1px solid #a4a6a5;text-align: left;padding: 6px;">'.$row->tiempo_parcial.' días</td>';
                                $salida .= '</tr>';
                                
                            }
                            $band = 0;
                        }
                        else{
                            $band = 1;
                        }
                    }
                    else{
                        $data['datos3'] = $this->reporte_model->reporteFinalizados_Espanol($f_inicio, $f_fin, $cl->id, $usuario);
                        if($data['datos3']){
                            foreach($data['datos3'] as $row){
                                $f_alta = $this->reporteFecha($row->fecha_alta);
                                $f_final = $this->reporteFecha($row->fecha_final);
                                $salida .= '<tr>';
                                $salida .= '<td style"border: 1px solid #a4a6a5;text-align: left;padding: 6px;">'.$f_alta.'</td>';
                                $salida .= '<td style"border: 1px solid #a4a6a5;text-align: left;padding: 6px;">'.$f_final.'</td>';
                                $salida .= '<td style"border: 1px solid #a4a6a5;text-align: left;padding: 6px;">'.$row->usuario.'</td>';
                                $salida .= '<td style"border: 1px solid #a4a6a5;text-align: left;padding: 6px;">'.$row->candidato.'</td>';
                                $salida .= '<td style"border: 1px solid #a4a6a5;text-align: left;padding: 6px;">'.$row->cliente.'</td>';
                                $salida .= '<td style"border: 1px solid #a4a6a5;text-align: left;padding: 6px;">'.$row->tiempo.' días</td>';
                                $salida .= '</tr>';
                                
                            }
                            $band = 0;
                        }
                        else{
                            $band = 1;
                        }
                    }
                }
                $salida .= "</tbody></tr>";
                $salida .= "</table>";
            }
            else{
                if($cliente == 1 || $cliente == 2){
                    $data['datos1'] = $this->reporte_model->reporteFinalizados_HCL_UST($f_inicio, $f_fin, $cliente, $usuario);
                    if($data['datos1']){
                        foreach($data['datos1'] as $row){
                            $f_alta = $this->reporteFecha($row->fecha_alta);
                            $f_final = $this->reporteFecha($row->fecha_final);
                            $salida .= '<tr>';
                            $salida .= '<td style"border: 1px solid #a4a6a5;text-align: left;padding: 6px;">'.$f_alta.'</td>';
                            $salida .= '<td style"border: 1px solid #a4a6a5;text-align: left;padding: 6px;">'.$f_final.'</td>';
                            $salida .= '<td style"border: 1px solid #a4a6a5;text-align: left;padding: 6px;">'.$row->usuario.'</td>';
                            $salida .= '<td style"border: 1px solid #a4a6a5;text-align: left;padding: 6px;">'.$row->candidato.'</td>';
                            $salida .= '<td style"border: 1px solid #a4a6a5;text-align: left;padding: 6px;">'.$row->cliente.'</td>';
                            $salida .= '<td style"border: 1px solid #a4a6a5;text-align: left;padding: 6px;">'.$row->tiempo.' días</td>';
                            $salida .= '</tr>';
                        }
                        $band = 0;
                    }
                    else{
                        $band = 1;
                    }
                }
                elseif($cliente == 3 || $cliente == 77){
                    $data['datos2'] = $this->reporte_model->reporteFinalizados_TATA_WIPRO($f_inicio, $f_fin, $cliente, $usuario);
                    if($data['datos2']){
                        foreach($data['datos2'] as $row){
                            $f_alta = $this->reporteFecha($row->fecha_alta);
                            $f_final = $this->reporteFecha($row->fecha_final);
                            $salida .= '<tr>';
                            $salida .= '<td style"border: 1px solid #a4a6a5;text-align: left;padding: 6px;">'.$f_alta.'</td>';
                            $salida .= '<td style"border: 1px solid #a4a6a5;text-align: left;padding: 6px;">'.$f_final.'</td>';
                            $salida .= '<td style"border: 1px solid #a4a6a5;text-align: left;padding: 6px;">'.$row->usuario.'</td>';
                            $salida .= '<td style"border: 1px solid #a4a6a5;text-align: left;padding: 6px;">'.$row->candidato.'</td>';
                            $salida .= '<td style"border: 1px solid #a4a6a5;text-align: left;padding: 6px;">'.$row->cliente.'</td>';
                            $salida .= '<td style"border: 1px solid #a4a6a5;text-align: left;padding: 6px;">'.$row->tiempo_parcial.' días</td>';
                            $salida .= '</tr>';
                            
                        }
                        $band = 0;
                    }
                    else{
                        $band = 1;
                    }
                }
                else{
                    $data['datos3'] = $this->reporte_model->reporteFinalizados_Espanol($f_inicio, $f_fin, $cliente, $usuario);
                    if($data['datos3']){
                        foreach($data['datos3'] as $row){
                            $f_alta = $this->reporteFecha($row->fecha_alta);
                            $f_final = $this->reporteFecha($row->fecha_final);
                            $salida .= '<tr>';
                            $salida .= '<td style"border: 1px solid #a4a6a5;text-align: left;padding: 6px;">'.$f_alta.'</td>';
                            $salida .= '<td style"border: 1px solid #a4a6a5;text-align: left;padding: 6px;">'.$f_final.'</td>';
                            $salida .= '<td style"border: 1px solid #a4a6a5;text-align: left;padding: 6px;">'.$row->usuario.'</td>';
                            $salida .= '<td style"border: 1px solid #a4a6a5;text-align: left;padding: 6px;">'.$row->candidato.'</td>';
                            $salida .= '<td style"border: 1px solid #a4a6a5;text-align: left;padding: 6px;">'.$row->cliente.'</td>';
                            $salida .= '<td style"border: 1px solid #a4a6a5;text-align: left;padding: 6px;">'.$row->tiempo.' días</td>';
                            $salida .= '</tr>';
                            
                        }
                        $band = 0;
                    }
                    else{
                        $band = 1;
                    }
                }
            }
        }

        echo $salida;
    }
    function reporteEstudiosFinalizados_Excel(){
        $datos = $this->uri->segment(3);
        $dato = explode('_', $datos);
        $f_inicio = $dato[0];
        $f_fin = $dato[1];
        $cliente = $dato[2];
        $usuario = $dato[3];
         //Cargamos la librería de excel.
        $this->load->library('excel');
        //$mobiledata = $this->export->mobileList();
        $excel = new PHPExcel();
        $excel->setActiveSheetIndex(0);
        //Contador de filas
        $contador = 1;

        //Le aplicamos ancho las columnas.
        $excel->getActiveSheet()->getColumnDimension('A')->setWidth(30);
        $excel->getActiveSheet()->getColumnDimension('B')->setWidth(30);
        $excel->getActiveSheet()->getColumnDimension('C')->setWidth(30);
        $excel->getActiveSheet()->getColumnDimension('D')->setWidth(50);
        $excel->getActiveSheet()->getColumnDimension('E')->setWidth(50);
        $excel->getActiveSheet()->getColumnDimension('F')->setWidth(30);
       
        //Le aplicamos negrita a los títulos de la cabecera.
        $excel->getActiveSheet()->getStyle("A{$contador}")->getFont()->setBold(true);
        $excel->getActiveSheet()->getStyle("B{$contador}")->getFont()->setBold(true);
        $excel->getActiveSheet()->getStyle("C{$contador}")->getFont()->setBold(true);
        $excel->getActiveSheet()->getStyle("D{$contador}")->getFont()->setBold(true);
        $excel->getActiveSheet()->getStyle("E{$contador}")->getFont()->setBold(true);
        $excel->getActiveSheet()->getStyle("F{$contador}")->getFont()->setBold(true);
       
        //Definimos los títulos de la cabecera.
        $excel->getActiveSheet()->setCellValue("A{$contador}", 'FECHA ALTA');
        $excel->getActiveSheet()->setCellValue("B{$contador}", 'FECHA FINALIZADO');
        $excel->getActiveSheet()->setCellValue("C{$contador}", 'ANALISTA');
        $excel->getActiveSheet()->setCellValue("D{$contador}", 'CANDIDATO');
        $excel->getActiveSheet()->setCellValue("E{$contador}", 'CLIENTE');
        $excel->getActiveSheet()->setCellValue("F{$contador}", 'SLA');
        
        if($usuario == 0){
            $data['usuarios'] = $this->reporte_model->getUsuarios();
            foreach($data['usuarios'] as $user){
                if($cliente == 0){
                    $data['data'] = $this->reporte_model->getClientes();
                    foreach($data['data'] as $cl){
                        if($cl->id == 1 || $cl->id == 2){
                            $data['datos1'] = $this->reporte_model->reporteFinalizados_HCL_UST($f_inicio, $f_fin, $cl->id, $user->id);
                            if($data['datos1']){
                                foreach($data['datos1'] as $row){
                                    $f_alta = $this->reporteFecha($row->fecha_alta);
                                    $f_final = $this->reporteFecha($row->fecha_final);
                                    //Incrementamos una fila más, para ir a la siguiente.
                                    $contador++;
                                    $excel->getActiveSheet()->setCellValue("A{$contador}", $f_alta);
                                    $excel->getActiveSheet()->setCellValue("B{$contador}", $f_final);
                                    $excel->getActiveSheet()->setCellValue("C{$contador}", $row->usuario);
                                    $excel->getActiveSheet()->setCellValue("D{$contador}", $row->candidato);
                                    $excel->getActiveSheet()->setCellValue("E{$contador}", $row->cliente);
                                    $excel->getActiveSheet()->setCellValue("F{$contador}", $row->tiempo);
                                }
                                $band = 0;
                            }
                            else{
                                $band = 1;
                            }
                        }
                        elseif($cl->id == 3 || $cl->id == 77){
                            $data['datos2'] = $this->reporte_model->reporteFinalizados_TATA_WIPRO($f_inicio, $f_fin, $cl->id, $user->id);
                            if($data['datos2']){
                                foreach($data['datos2'] as $row){
                                    $f_alta = $this->reporteFecha($row->fecha_alta);
                                    $f_final = $this->reporteFecha($row->fecha_final);
                                    //Incrementamos una fila más, para ir a la siguiente.
                                    $contador++;
                                    $excel->getActiveSheet()->setCellValue("A{$contador}", $f_alta);
                                    $excel->getActiveSheet()->setCellValue("B{$contador}", $f_final);
                                    $excel->getActiveSheet()->setCellValue("C{$contador}", $row->usuario);
                                    $excel->getActiveSheet()->setCellValue("D{$contador}", $row->candidato);
                                    $excel->getActiveSheet()->setCellValue("E{$contador}", $row->cliente);
                                    $excel->getActiveSheet()->setCellValue("F{$contador}", $row->tiempo_parcial);
                                }
                                $band = 0;
                            }
                            else{
                                $band = 1;
                            }
                        }
                        else{
                            $data['datos3'] = $this->reporte_model->reporteFinalizados_Espanol($f_inicio, $f_fin, $cl->id, $user->id);
                            if($data['datos3']){
                                foreach($data['datos3'] as $row){
                                    $f_alta = $this->reporteFecha($row->fecha_alta);
                                    $f_final = $this->reporteFecha($row->fecha_final);
                                    //Incrementamos una fila más, para ir a la siguiente.
                                    $contador++;
                                    $excel->getActiveSheet()->setCellValue("A{$contador}", $f_alta);
                                    $excel->getActiveSheet()->setCellValue("B{$contador}", $f_final);
                                    $excel->getActiveSheet()->setCellValue("C{$contador}", $row->usuario);
                                    $excel->getActiveSheet()->setCellValue("D{$contador}", $row->candidato);
                                    $excel->getActiveSheet()->setCellValue("E{$contador}", $row->cliente);
                                    $excel->getActiveSheet()->setCellValue("F{$contador}", $row->tiempo);
                                }
                                $band = 0;
                            }
                            else{
                                $band = 1;
                            }
                        }
                        
                    }
                    
                }
                else{
                    if($cliente == 1 || $cliente == 2){
                        $data['datos1'] = $this->reporte_model->reporteFinalizados_HCL_UST($f_inicio, $f_fin, $cliente, $user->id);
                        if($data['datos1']){
                            foreach($data['datos1'] as $row){
                                $f_alta = $this->reporteFecha($row->fecha_alta);
                                $f_final = $this->reporteFecha($row->fecha_final);
                                //Incrementamos una fila más, para ir a la siguiente.
                                $contador++;
                                $excel->getActiveSheet()->setCellValue("A{$contador}", $f_alta);
                                $excel->getActiveSheet()->setCellValue("B{$contador}", $f_final);
                                $excel->getActiveSheet()->setCellValue("C{$contador}", $row->usuario);
                                $excel->getActiveSheet()->setCellValue("D{$contador}", $row->candidato);
                                $excel->getActiveSheet()->setCellValue("E{$contador}", $row->cliente);
                                $excel->getActiveSheet()->setCellValue("F{$contador}", $row->tiempo);
                            }
                            $band = 0;
                        }
                        else{
                            $band = 1;
                        }
                    }
                    elseif($cliente == 3 || $cliente == 77){
                        $data['datos2'] = $this->reporte_model->reporteFinalizados_TATA_WIPRO($f_inicio, $f_fin, $cliente, $user->id);
                        if($data['datos2']){
                            foreach($data['datos2'] as $row){
                                $f_alta = $this->reporteFecha($row->fecha_alta);
                                $f_final = $this->reporteFecha($row->fecha_final);
                                //Incrementamos una fila más, para ir a la siguiente.
                                $contador++;
                                $excel->getActiveSheet()->setCellValue("A{$contador}", $f_alta);
                                $excel->getActiveSheet()->setCellValue("B{$contador}", $f_final);
                                $excel->getActiveSheet()->setCellValue("C{$contador}", $row->usuario);
                                $excel->getActiveSheet()->setCellValue("D{$contador}", $row->candidato);
                                $excel->getActiveSheet()->setCellValue("E{$contador}", $row->cliente);
                                $excel->getActiveSheet()->setCellValue("F{$contador}", $row->tiempo_parcial);
                            }
                            $band = 0;
                        }
                        else{
                            $band = 1;
                        }
                    }
                    else{
                        $data['datos3'] = $this->reporte_model->reporteFinalizados_Espanol($f_inicio, $f_fin, $cliente, $user->id);
                        if($data['datos3']){
                            foreach($data['datos3'] as $row){
                                $f_alta = $this->reporteFecha($row->fecha_alta);
                                $f_final = $this->reporteFecha($row->fecha_final);
                                //Incrementamos una fila más, para ir a la siguiente.
                                $contador++;
                                $excel->getActiveSheet()->setCellValue("A{$contador}", $f_alta);
                                $excel->getActiveSheet()->setCellValue("B{$contador}", $f_final);
                                $excel->getActiveSheet()->setCellValue("C{$contador}", $row->usuario);
                                $excel->getActiveSheet()->setCellValue("D{$contador}", $row->candidato);
                                $excel->getActiveSheet()->setCellValue("E{$contador}", $row->cliente);
                                $excel->getActiveSheet()->setCellValue("F{$contador}", $row->tiempo);
                            }
                            $band = 0;
                        }
                        else{
                            $band = 1;
                        }
                    }
                }
            }
            $salida .= "</tbody>";
            $salida .= "</table>";
        }
        else{
            if($cliente == 0){
                $data['data'] = $this->reporte_model->getClientes();
                foreach($data['data'] as $cl){
                    if($cl->id == 1 || $cl->id == 2){
                        $data['datos1'] = $this->reporte_model->reporteFinalizados_HCL_UST($f_inicio, $f_fin, $cl->id, $usuario);
                        if($data['datos1']){
                            foreach($data['datos1'] as $row){
                                $f_alta = $this->reporteFecha($row->fecha_alta);
                                $f_final = $this->reporteFecha($row->fecha_final);
                                //Incrementamos una fila más, para ir a la siguiente.
                                $contador++;
                                $excel->getActiveSheet()->setCellValue("A{$contador}", $f_alta);
                                $excel->getActiveSheet()->setCellValue("B{$contador}", $f_final);
                                $excel->getActiveSheet()->setCellValue("C{$contador}", $row->usuario);
                                $excel->getActiveSheet()->setCellValue("D{$contador}", $row->candidato);
                                $excel->getActiveSheet()->setCellValue("E{$contador}", $row->cliente);
                                $excel->getActiveSheet()->setCellValue("F{$contador}", $row->tiempo);
                            }
                            $band = 0;
                        }
                        else{
                            $band = 1;
                        }
                    }
                    elseif($cl->id == 3 || $cl->id == 77){
                        $data['datos2'] = $this->reporte_model->reporteFinalizados_TATA_WIPRO($f_inicio, $f_fin, $cl->id, $usuario);
                        if($data['datos2']){
                            foreach($data['datos2'] as $row){
                                $f_alta = $this->reporteFecha($row->fecha_alta);
                                $f_final = $this->reporteFecha($row->fecha_final);
                                //Incrementamos una fila más, para ir a la siguiente.
                                $contador++;
                                $excel->getActiveSheet()->setCellValue("A{$contador}", $f_alta);
                                $excel->getActiveSheet()->setCellValue("B{$contador}", $f_final);
                                $excel->getActiveSheet()->setCellValue("C{$contador}", $row->usuario);
                                $excel->getActiveSheet()->setCellValue("D{$contador}", $row->candidato);
                                $excel->getActiveSheet()->setCellValue("E{$contador}", $row->cliente);
                                $excel->getActiveSheet()->setCellValue("F{$contador}", $row->tiempo_parcial);
                            }
                            $band = 0;
                        }
                        else{
                            $band = 1;
                        }
                    }
                    else{
                        $data['datos3'] = $this->reporte_model->reporteFinalizados_Espanol($f_inicio, $f_fin, $cl->id, $usuario);
                        if($data['datos3']){
                            foreach($data['datos3'] as $row){
                                $f_alta = $this->reporteFecha($row->fecha_alta);
                                $f_final = $this->reporteFecha($row->fecha_final);
                                //Incrementamos una fila más, para ir a la siguiente.
                                $contador++;
                                $excel->getActiveSheet()->setCellValue("A{$contador}", $f_alta);
                                $excel->getActiveSheet()->setCellValue("B{$contador}", $f_final);
                                $excel->getActiveSheet()->setCellValue("C{$contador}", $row->usuario);
                                $excel->getActiveSheet()->setCellValue("D{$contador}", $row->candidato);
                                $excel->getActiveSheet()->setCellValue("E{$contador}", $row->cliente);
                                $excel->getActiveSheet()->setCellValue("F{$contador}", $row->tiempo);
                            }
                            $band = 0;
                        }
                        else{
                            $band = 1;
                        }
                    }
                }
                $salida .= "</tbody></tr>";
                $salida .= "</table>";
            }
            else{
                if($cliente == 1 || $cliente == 2){
                    $data['datos1'] = $this->reporte_model->reporteFinalizados_HCL_UST($f_inicio, $f_fin, $cliente, $usuario);
                    if($data['datos1']){
                        foreach($data['datos1'] as $row){
                            $f_alta = $this->reporteFecha($row->fecha_alta);
                            $f_final = $this->reporteFecha($row->fecha_final);
                            //Incrementamos una fila más, para ir a la siguiente.
                            $contador++;
                            $excel->getActiveSheet()->setCellValue("A{$contador}", $f_alta);
                            $excel->getActiveSheet()->setCellValue("B{$contador}", $f_final);
                            $excel->getActiveSheet()->setCellValue("C{$contador}", $row->usuario);
                            $excel->getActiveSheet()->setCellValue("D{$contador}", $row->candidato);
                            $excel->getActiveSheet()->setCellValue("E{$contador}", $row->cliente);
                            $excel->getActiveSheet()->setCellValue("F{$contador}", $row->tiempo);
                        }
                        $band = 0;
                    }
                    else{
                        $band = 1;
                    }
                }
                elseif($cliente == 3 || $cliente == 77){
                    $data['datos2'] = $this->reporte_model->reporteFinalizados_TATA_WIPRO($f_inicio, $f_fin, $cliente, $usuario);
                    if($data['datos2']){
                        foreach($data['datos2'] as $row){
                            $f_alta = $this->reporteFecha($row->fecha_alta);
                            $f_final = $this->reporteFecha($row->fecha_final);
                            //Incrementamos una fila más, para ir a la siguiente.
                            $contador++;
                            $excel->getActiveSheet()->setCellValue("A{$contador}", $f_alta);
                            $excel->getActiveSheet()->setCellValue("B{$contador}", $f_final);
                            $excel->getActiveSheet()->setCellValue("C{$contador}", $row->usuario);
                            $excel->getActiveSheet()->setCellValue("D{$contador}", $row->candidato);
                            $excel->getActiveSheet()->setCellValue("E{$contador}", $row->cliente);
                            $excel->getActiveSheet()->setCellValue("F{$contador}", $row->tiempo_parcial);
                        }
                        $band = 0;
                    }
                    else{
                        $band = 1;
                    }
                }
                else{
                    $data['datos3'] = $this->reporte_model->reporteFinalizados_Espanol($f_inicio, $f_fin, $cliente, $usuario);
                    if($data['datos3']){
                        foreach($data['datos3'] as $row){
                            $f_alta = $this->reporteFecha($row->fecha_alta);
                            $f_final = $this->reporteFecha($row->fecha_final);
                            //Incrementamos una fila más, para ir a la siguiente.
                            $contador++;
                            $excel->getActiveSheet()->setCellValue("A{$contador}", $f_alta);
                            $excel->getActiveSheet()->setCellValue("B{$contador}", $f_final);
                            $excel->getActiveSheet()->setCellValue("C{$contador}", $row->usuario);
                            $excel->getActiveSheet()->setCellValue("D{$contador}", $row->candidato);
                            $excel->getActiveSheet()->setCellValue("E{$contador}", $row->cliente);
                            $excel->getActiveSheet()->setCellValue("F{$contador}", $row->tiempo);
                        }
                        $band = 0;
                    }
                    else{
                        $band = 1;
                    }
                }
            }
        }
        //Le ponemos un nombre al archivo que se va a generar.
        $archivo = "Reporte2_ESEFinalizadosAnalistas.xls";
        $objWriter = new PHPExcel_Writer_Excel2007($excel);
        $objWriter->save($archivo);
        // download file
        header("Content-Type: application/vnd.ms-excel");
        redirect(site_url().$archivo);       
    }
    function reporteDopingGeneral(){
        $f_inicio = fecha_espanol_bd($_POST['fi']);
        $f_fin = fecha_espanol_bd($_POST['ff']);
        $cliente = $_POST['cliente'];
        $subcliente = $_POST['subcliente'];
        $proyecto = $_POST['proyecto'];

        $data['datos'] = $this->reporte_model->reporteDopingGeneral($f_inicio, $f_fin, $cliente, $subcliente, $proyecto);
        //var_dump($data['datos']);
        if($data['datos']){
            $salida = '<div style="text-align:center;margin-bottom:50px;"><a class="btn btn-success" href="'.base_url().'Reporte/reporteDopingGeneral_Excel/'.$f_inicio.'_'.$f_fin.'_'.$cliente.'_'.$subcliente.'_'.$proyecto.'" target="_blank"><i class="fas fa-file-excel"></i> Exportar a Excel</a></div>';
            $salida .= '<table style="border: 0px; border-collapse: collapse;width: 100%;padding:5px;">';
            $salida .= '<tr>';
            $salida .= '<th style"border: 1px solid #a4a6a5;text-align: left;padding: 6px;">Fecha registro</th>';
            $salida .= '<th style"border: 1px solid #a4a6a5;text-align: left;padding: 6px;">Nombre</th>';
            $salida .= '<th style"border: 1px solid #a4a6a5;text-align: left;padding: 6px;">Cliente</th>';
            $salida .= '<th style"border: 1px solid #a4a6a5;text-align: left;padding: 6px;">Subcliente</th>';
            $salida .= '<th style"border: 1px solid #a4a6a5;text-align: left;padding: 6px;">Proyecto</th>';
            $salida .= '<th style"border: 1px solid #a4a6a5;text-align: left;padding: 6px;">Examen</th>';
            $salida .= '<th style"border: 1px solid #a4a6a5;text-align: left;padding: 6px;">Código</th>';
            $salida .= '</tr>';
            foreach($data['datos'] as $row){
                $subcliente = ($row->subcliente != "" && $row->subcliente != null)? $row->subcliente:"-";
                $proyecto = ($row->proyecto != "" && $row->proyecto != null)? $row->proyecto:"-";
                $f_doping = $this->reporteFecha($row->creacion);
                $salida .= "<tr><tbody>";
                $salida .= '<td style"border: 1px solid #a4a6a5;text-align: left;padding: 6px;">'.$f_doping.'</td>';
                $salida .= '<td style"border: 1px solid #a4a6a5;text-align: left;padding: 6px;">'.$row->candidato.'</td>';
                $salida .= '<td style"border: 1px solid #a4a6a5;text-align: left;padding: 6px;">'.$row->cliente.'</td>';
                $salida .= '<td style"border: 1px solid #a4a6a5;text-align: left;padding: 6px;">'.$subcliente.'</td>';
                $salida .= '<td style"border: 1px solid #a4a6a5;text-align: left;padding: 6px;">'.$proyecto.'</td>';
                $salida .= '<td style"border: 1px solid #a4a6a5;text-align: left;padding: 6px;">'.$row->parametros.'</td>';
                $salida .= '<td style"border: 1px solid #a4a6a5;text-align: left;padding: 6px;">'.$row->codigo_prueba.'</td>';
                $salida .= "</tbody></tr>";
            }
            $salida .= "</table>";
        }
        else{
            $salida = '<p style="text-align:center;font-size:18px;font-weight:bold;">Sin registros de acuerdo a los filtros aplicados</p>';
        }
        echo $salida;
    }
    function reporteDopingGeneral_Excel(){
        $datos = $this->uri->segment(3);
        $dato = explode('_', $datos);
        $f_inicio = $dato[0];
        $f_fin = $dato[1];
        $cliente = $dato[2];
        $subcliente = $dato[3];
        $proyecto = $dato[4];
        //var_dump($datos);
        $data['datos'] = $this->reporte_model->reporteDopingGeneral($f_inicio, $f_fin, $cliente, $subcliente, $proyecto);
        if($data['datos']){
            //Cargamos la librería de excel.
            $this->load->library('excel');
            //$mobiledata = $this->export->mobileList();
            $excel = new PHPExcel();
            $excel->setActiveSheetIndex(0);
            //$excel->getActiveSheet()->setTitle('Reporte_RegistrosDoping');
            //Contador de filas
            $contador = 1;
            //Le aplicamos ancho las columnas.
            $excel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
            $excel->getActiveSheet()->getColumnDimension('B')->setWidth(100);
            $excel->getActiveSheet()->getColumnDimension('C')->setWidth(50);
            $excel->getActiveSheet()->getColumnDimension('D')->setWidth(50);
            $excel->getActiveSheet()->getColumnDimension('E')->setWidth(50);
            $excel->getActiveSheet()->getColumnDimension('F')->setWidth(20);
            $excel->getActiveSheet()->getColumnDimension('G')->setWidth(30);
            
            //Le aplicamos negrita a los títulos de la cabecera.
            $excel->getActiveSheet()->getStyle("A{$contador}")->getFont()->setBold(true);
            $excel->getActiveSheet()->getStyle("B{$contador}")->getFont()->setBold(true);
            $excel->getActiveSheet()->getStyle("C{$contador}")->getFont()->setBold(true);
            $excel->getActiveSheet()->getStyle("D{$contador}")->getFont()->setBold(true);
            $excel->getActiveSheet()->getStyle("E{$contador}")->getFont()->setBold(true);
            $excel->getActiveSheet()->getStyle("F{$contador}")->getFont()->setBold(true);
            $excel->getActiveSheet()->getStyle("G{$contador}")->getFont()->setBold(true);
            
            //Definimos los títulos de la cabecera.
            $excel->getActiveSheet()->setCellValue("A{$contador}", 'FECHA REGISTRO');
            $excel->getActiveSheet()->setCellValue("B{$contador}", 'NOMBRE');
            $excel->getActiveSheet()->setCellValue("C{$contador}", 'CLIENTE');
            $excel->getActiveSheet()->setCellValue("D{$contador}", 'SUBCLIENTE');
            $excel->getActiveSheet()->setCellValue("E{$contador}", 'PROYECTO');
            $excel->getActiveSheet()->setCellValue("F{$contador}", 'EXAMEN');
            $excel->getActiveSheet()->setCellValue("G{$contador}", 'CÓDIGO');
            
            //Definimos la data del cuerpo.        
            foreach($data['datos'] as $row){
                $subcliente = ($row->subcliente != "" && $row->subcliente != null)? $row->subcliente:"-";
                $proyecto = ($row->proyecto != "" && $row->proyecto != null)? $row->proyecto:"-";
                $f_doping = $this->reporteFecha($row->creacion);
               //Incrementamos una fila más, para ir a la siguiente.
               $contador++;
               //Informacion de las filas de la consulta.
               $excel->getActiveSheet()->setCellValue("A{$contador}", $f_doping);
               $excel->getActiveSheet()->setCellValue("B{$contador}", $row->candidato);
               $excel->getActiveSheet()->setCellValue("C{$contador}", $row->cliente);
               $excel->getActiveSheet()->setCellValue("D{$contador}", $subcliente);
               $excel->getActiveSheet()->setCellValue("E{$contador}", $proyecto);
               $excel->getActiveSheet()->setCellValue("F{$contador}", $row->parametros);
               $excel->getActiveSheet()->setCellValue("G{$contador}", $row->codigo_prueba);
            }
            //Le ponemos un nombre al archivo que se va a generar.
            $archivo = "Reporte3_RegistrosDopingGeneral.xls";
            $objWriter = new PHPExcel_Writer_Excel2007($excel);
            $objWriter->save($archivo);
            // download file
            header("Content-Type: application/vnd.ms-excel");
            redirect(site_url().$archivo);    
            /*$archivo = "Reporte_RegistrosDoping.xls";
            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename="'.$archivo.'"');
            header('Cache-Control: max-age=0');
            $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
            //Hacemos una salida al navegador con el archivo Excel.
            $objWriter->save('php://output');*/
        }
        /*else{
            $contador = 2;
            $this->excel->getActiveSheet()->setCellValue("A{$contador}", "SIN REGISTROS");
        }*/
    }
    function reporteFecha($date){
        $f = explode(' ', $date);
        $aux = explode('-', $f[0]);
        $fecha = $aux[2].'/'.$aux[1].'/'.$aux[0];
        $fecha .= " ".$f[1];
        return $fecha;
    }
	function getFechaNacimiento(){
        $id_candidato = $_POST['id_candidato'];
        $f = $this->doping_model->getFechaNacimiento($id_candidato);
        if($f->fecha_nacimiento != ""){
            $aux = explode('-', $f->fecha_nacimiento);
            $fnacimiento = $aux[2].'/'.$aux[1].'/'.$aux[0];
            echo $fnacimiento;
        }
        else{
            echo $fnacimiento = "";
        }
    }
    function createPDF(){
        $mpdf = new \Mpdf\Mpdf();
        ob_clean();
        date_default_timezone_set('America/Mexico_City');
        $data['hoy'] = date("d-m-Y");
        $hoy = date("d-m-Y");
        $id_doping = $_POST['idPDF'];
        $doping = $this->doping_model->getDatosDoping($id_doping);
        
        $data['doping'] = $doping;
        $html = $this->load->view('pdfs/doping_pdf',$data,TRUE);
        $mpdf->setAutoTopMargin = 'stretch';
        $mpdf->SetHTMLHeader('<div style="width: 100%; float: left;"><img style="height: 150px;" src="'.base_url().'img/Encabezado.png"></div>');
        $mpdf->SetHTMLFooter('<div style="position: absolute; left: 20px; bottom: 10px; color: rgba(0,0,0,0.5);"><p style="font-size: 12px;"><div style="border-bottom:1px solid gray;"><b>Teléfono:</b> (33) 2301-8599 | <b>Correo:</b> hola@rodi.com.mx | <b>Sitio web:</b> rodi.com.mx</div><br>Calle Benito Juarez # 5693, Col. Santa María del Pueblito <br>Zapopan, Jalisco, México. C.P. 45018 <br></p></div><div style="position: absolute; right: 10px;  bottom: 13px;"><img width="" src="'.base_url().'img/logo2.png"></div>');
        $mpdf->WriteHTML($html);

        $mpdf->Output('doping_'.$doping->codigo_prueba.'_'.$doping->nombre.'_'.$doping->paterno.'.pdf','D'); // opens in browser
    }
    function createPDF2(){
        $mpdf = new \Mpdf\Mpdf();
        date_default_timezone_set('America/Mexico_City');
        $data['hoy'] = date("d-m-Y");
        $hoy = date("d-m-Y");
        $id_doping = $_POST['idPDF2'];
        $doping = $this->doping_model->getDatosDoping($id_doping);
        
        $data['doping'] = $doping;
        $html = $this->load->view('doping/doping_adul_pdf',$data,TRUE);
        $mpdf->setAutoTopMargin = 'stretch';
        $mpdf->SetHTMLHeader('<div style="width: 100%; float: left;"><img style="height: 150px;" src="'.base_url().'img/Encabezado.png"></div>');
        $mpdf->SetHTMLFooter('<div style="position: absolute; left: 20px; bottom: 10px; color: rgba(0,0,0,0.5);"><p style="font-size: 12px;"><div style="border-bottom:1px solid gray;"><b>Teléfono:</b> (33) 2301-8599 | <b>Correo:</b> hola@rodi.com.mx | <b>Sitio web:</b> rodi.com.mx</div><br>Calle Benito Juarez # 5693, Col. Santa María del Pueblito <br>Zapopan, Jalisco, México. C.P. 45018 <br></p></div><div style="position: absolute; right: 10px;  bottom: 13px;"><img width="" src="'.base_url().'img/logo2.png"></div>');
        $mpdf->WriteHTML($html);

        $mpdf->Output('doping_adul_'.$doping->codigo_prueba.'_'.$doping->nombre.'_'.$doping->paterno.'.pdf','D'); // opens in browser
    }
    function createCadenaPDF(){
        $mpdf = new \Mpdf\Mpdf();
        date_default_timezone_set('America/Mexico_City');
        $data['hoy'] = date("d-m-Y");
        $hoy = date("d-m-Y");
        $id_doping = $_POST['idCadena'];
        $doping = $this->doping_model->getDatosDoping($id_doping);
        
        $data['doping'] = $doping;
        $html = $this->load->view('doping/doping_cadena_pdf',$data,TRUE);
        $mpdf->setAutoTopMargin = 'stretch';
        $mpdf->SetHTMLHeader('<div style="width: 100%; float: left;"><img style="height: 50px;" src="'.base_url().'img/logo.png"></div>');
        $mpdf->SetHTMLFooter('<div style="position: absolute; left: 20px; bottom: 10px;"><p style="font-size: 12px;"><div style="width: 95%;margin-left: 20px;">AVISO DE PRIVACIDAD RO & DI GLOBAL S DE RL DE CV HACE DE SU CONOCIMIENTO QUE LA INFORMACIÓN PROPORCIONADA POR USTED EN EL PRESENTE DOCUMENTO ES PROPIEDAD DE LA EMPRESA QUIEN CONTRATÓ ESTE SERVICIO Y NOS DESLINDAMOS DEL USO QUE SE LE DÉ A LA MISMA.</p></div></div>');
        $mpdf->WriteHTML($html);

        $mpdf->Output('cadena_'.$doping->codigo_prueba.'_'.$doping->nombre.'_'.$doping->paterno.'.pdf','D'); // opens in browser
    }
}