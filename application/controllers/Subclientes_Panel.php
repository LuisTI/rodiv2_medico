<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Subclientes_Panel extends CI_Controller{

	function __construct(){
		parent::__construct();
	}
    
  function registrar(){
    $this->form_validation->set_rules('nombre', 'Nombre', 'required|trim');
    $this->form_validation->set_rules('paterno', 'Primer apellido', 'required|trim');
    $this->form_validation->set_rules('materno', 'Segundo apellido', 'trim');
    $this->form_validation->set_rules('celular', 'Tel. Celular', 'required|trim|max_length[16]');
    $this->form_validation->set_rules('correo', 'Correo', 'trim|valid_email');
    $this->form_validation->set_rules('puesto', 'Puesto', 'required');
    $this->form_validation->set_rules('socio', 'Socioeconomico', 'required');
    $this->form_validation->set_rules('antidoping', 'Antidoping', 'required');
    $this->form_validation->set_rules('psicometrico', 'Psicométrico', 'required');
    $this->form_validation->set_rules('medico', 'Médico', 'required');

    $this->form_validation->set_message('required', 'El campo {field} es obligatorio');
    $this->form_validation->set_message('max_length', 'El campo {field} debe tener máximo {param} carácteres');
    $this->form_validation->set_message('valid_email', 'El campo {field} debe ser un correo válido');

    $msj = array();
    if ($this->form_validation->run() == FALSE) {
      $msj = array(
        'codigo' => 0,
        'msg' => validation_errors()
      );
    } else {
      date_default_timezone_set('America/Mexico_City');
      $date = date('Y-m-d H:i:s');
      $nombre = $this->input->post('nombre');
      $paterno = $this->input->post('paterno');
      $materno = $this->input->post('materno');
      $proceso = $this->input->post('proceso');
      $correo = $this->input->post('correo');
      $celular = $this->input->post('celular');
      $id_cliente = $this->session->userdata('idcliente');
      $id_subcliente = $this->session->userdata('idsubcliente');
      $id_puesto = $this->input->post('puesto');
      $socio = $this->input->post('socio');
      $medico = $this->input->post('medico');
      $antidoping = $this->input->post('antidoping');
      $psicometrico = $this->input->post('psicometrico');
      $examen = $this->input->post('examen');
      $otro_requerimiento = $this->input->post('otro');
      $existeCandidato = $this->cliente_general_model->checkCandidatoRepetidoSubcliente(strtoupper($nombre), strtoupper($paterno), strtoupper($materno), $id_cliente, $id_subcliente);
      if ($existeCandidato > 0) {
        $msj = array(
          'codigo' => 2,
          'msg' => 'El candidato ya fue registrado previamente'
        );
      } 
      else {
        $id_usuario = $this->session->userdata('id');
        $configuracion = $this->funciones_model->getConfiguraciones();
        $data = array(
          'creacion' => $date,
          'edicion' => $date,
          'id_usuario_subcliente' => $id_usuario,
          'id_usuario' => $configuracion->usuario_lider_espanol,
          'id_cliente' => $id_cliente,
          'id_subcliente' => $id_subcliente,
          'id_tipo_proceso' => $proceso,
          'id_puesto' => $id_puesto,
          'fecha_alta' => $date,
          'nombre' => strtoupper($nombre),
          'paterno' => strtoupper($paterno),
          'materno' => strtoupper($materno),
          'correo' => $correo,
          'celular' => $celular
        );
        $id_candidato = $this->candidato_model->registrarRetornaCandidato($data);

        //Subida y Registro de CV
        if ($this->input->post('hay_cvs') == 1) {
          $countfiles = count($_FILES['cvs']['name']);

          for ($i = 0; $i < $countfiles; $i++) {
            if (!empty($_FILES['cvs']['name'][$i])) {
              // Define new $_FILES array - $_FILES['file']
              $_FILES['file']['name'] = $_FILES['cvs']['name'][$i];
              $_FILES['file']['type'] = $_FILES['cvs']['type'][$i];
              $_FILES['file']['tmp_name'] = $_FILES['cvs']['tmp_name'][$i];
              $_FILES['file']['error'] = $_FILES['cvs']['error'][$i];
              $_FILES['file']['size'] = $_FILES['cvs']['size'][$i];
              $temp = str_replace(' ', '', $_FILES['cvs']['name'][$i]);
              $extension = pathinfo($_FILES['cvs']['name'][$i], PATHINFO_EXTENSION);

              $nombre_cv = $id_candidato . "_CV_" . $i . '.' . $extension;
              // Set preference
              $config['upload_path'] = './_docs/';
              $config['allowed_types'] = 'pdf|jpeg|jpg|png';
              //$config['max_size'] = '15000'; // max_size in kb
              $config['file_name'] = $nombre_cv;
              //Load upload library
              $this->load->library('upload', $config);
              $this->upload->initialize($config);
              // File upload
              if ($this->upload->do_upload('file')) {
                $data = $this->upload->data();
                //$salida = 1; 
              }
              $documento = array(
                'creacion' => $date,
                'edicion' => $date,
                'id_candidato' => $id_candidato,
                'id_tipo_documento' => 16,
                'archivo' => $nombre_cv
              );
              $this->candidato_model->registrarDocumento($documento);
            }
          }
        }

        if ($socio == 1) {
          if ($antidoping == 1) {
            if ($examen == 0) {
              $msj = array(
                'codigo' => 3,
                'msg' => 'El campo Examen Antidoping es obligatorio'
              );
            } 
            else {
              $drogas = $examen;
              $tipo_antidoping = 1;
              $pruebas = array(
                'creacion' => $date,
                'edicion' => $date,
                'id_usuario_subcliente' => $id_usuario,
                'id_candidato' => $id_candidato,
                'id_cliente' => $id_cliente,
                'socioeconomico' => $socio,
                'tipo_antidoping' => $tipo_antidoping,
                'antidoping' => $drogas,
                'tipo_psicometrico' => $psicometrico,
                'psicometrico' => $psicometrico,
                'medico' => $medico,
                'buro_credito' => 0,
                'sociolaboral' => 0,
                'otro_requerimiento' => $otro_requerimiento
              );
              $this->candidato_model->crearPruebas($pruebas);

              if ($id_subcliente != 180) {
                $visita = array(
                  'creacion' => $date,
                  'edicion' => $date,
                  'id_usuario_subcliente' => $id_usuario,
                  'id_cliente' => $id_cliente,
                  'id_subcliente' => $id_subcliente,
                  'id_candidato' => $id_candidato,
                  'id_tipo_formulario' => 4

                );
                $this->candidato_model->crearVisita($visita);
              }
              //Envio de aviso de registro por correo
              $from = $this->config->item('smtp_user');
              $info_cliente = $this->cliente_general_model->getDatosCliente($id_cliente);
              $to = "bjimenez@rodi.com.mx";
              $subject = " Nuevo candidato en la plataforma del cliente " . $info_cliente->nombre;
              $message = "Se ha agregado a " . strtoupper($this->input->post('nombre')) . " " . strtoupper($this->input->post('paterno')) . " " . strtoupper($this->input->post('materno')) . " del cliente " . $info_cliente->nombre . " en la plataforma";
              $this->load->library('phpmailer_lib');
              $mail = $this->phpmailer_lib->load();
              $mail->isSMTP();
              $mail->Host     = 'rodi.com.mx';
              $mail->SMTPAuth = true;
              $mail->Username = 'rodicontrol@rodi.com.mx';
              $mail->Password = 'RRodi#2019@';
              $mail->SMTPSecure = 'ssl';
              $mail->Port     = 465;
              $mail->setFrom('rodicontrol@rodi.com.mx', 'Rodi');
              $mail->addAddress($to);
              $mail->Subject = $subject;
              $mail->isHTML(true);
              $mailContent = $message;
              $mail->Body = $mailContent;

              if (!$mail->send()) {
                $enviado = 1;
              } else {
                $enviado = 0;
              }
              $msj = array(
                'codigo' => 1,
                'msg' => 'Success'
              );
            }
          } 
          else {
            $drogas = 0;
            $tipo_antidoping = 0;
            $pruebas = array(
              'creacion' => $date,
              'edicion' => $date,
              'id_usuario_subcliente' => $id_usuario,
              'id_candidato' => $id_candidato,
              'id_cliente' => $id_cliente,
              'socioeconomico' => $socio,
              'tipo_antidoping' => $tipo_antidoping,
              'antidoping' => $drogas,
              'tipo_psicometrico' => $psicometrico,
              'psicometrico' => $psicometrico,
              'medico' => $medico,
              'buro_credito' => 0,
              'sociolaboral' => 0,
              'otro_requerimiento' => $otro_requerimiento
            );
            $this->candidato_model->crearPruebas($pruebas);

            if ($id_subcliente != 180) {
              $visita = array(
                'creacion' => $date,
                'edicion' => $date,
                'id_usuario_subcliente' => $id_usuario,
                'id_cliente' => $id_cliente,
                'id_subcliente' => $id_subcliente,
                'id_candidato' => $id_candidato,
                'id_tipo_formulario' => 4

              );
              $this->candidato_model->crearVisita($visita);
            }
            //Envio de mensaje de nuevo registro de candidato por correo
            $from = $this->config->item('smtp_user');
            $info_cliente = $this->cliente_general_model->getDatosCliente($id_cliente);
            $to = "bjimenez@rodi.com.mx";
            $subject = " Nuevo candidato en la plataforma del cliente " . $info_cliente->nombre;
            $message = "Se ha agregado a " . strtoupper($this->input->post('nombre')) . " " . strtoupper($this->input->post('paterno')) . " " . strtoupper($this->input->post('materno')) . " del cliente " . $info_cliente->nombre . " en la plataforma";
            $this->load->library('phpmailer_lib');
            $mail = $this->phpmailer_lib->load();
            $mail->isSMTP();
            $mail->Host     = 'rodi.com.mx';
            $mail->SMTPAuth = true;
            $mail->Username = 'rodicontrol@rodi.com.mx';
            $mail->Password = 'RRodi#2019@';
            $mail->SMTPSecure = 'ssl';
            $mail->Port     = 465;
            $mail->setFrom('rodicontrol@rodi.com.mx', 'Rodi');
            $mail->addAddress($to);
            $mail->Subject = $subject;
            $mail->isHTML(true);
            $mailContent = $message;
            $mail->Body = $mailContent;

            if (!$mail->send()) {
              $enviado = 1;
            } else {
              $enviado = 0;
            }
            $msj = array(
              'codigo' => 1,
              'msg' => 'Success'
            );
          }
        } else {
          if ($antidoping == 0) {
            $msj = array(
              'codigo' => 4,
              'msg' => 'Debe aplicarse el estudio socioeconomico y/o el examen antidoping'
            );
          } 
          else {
            if ($examen == 0) {
              $msj = array(
                'codigo' => 3,
                'msg' => 'El campo Examen Antidoping es obligatorio'
              );
            } 
            else {
              $pruebas = array(
                'creacion' => $date,
                'edicion' => $date,
                'id_usuario_subcliente' => $id_usuario,
                'id_candidato' => $id_candidato,
                'id_cliente' => $id_cliente,
                'socioeconomico' => $socio,
                'tipo_antidoping' => 1,
                'antidoping' => $examen,
                'tipo_psicometrico' => 0,
                'psicometrico' => 0,
                'medico' => 0,
                'buro_credito' => 0,
                'sociolaboral' => 0,
                'otro_requerimiento' => $otro_requerimiento
              );
              $this->candidato_model->crearPruebas($pruebas);
              $msj = array(
                'codigo' => 1,
                'msg' => 'Success'
              );
            }
          }
        }
      }
    }
    echo json_encode($msj);
  }

}
    