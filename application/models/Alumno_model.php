<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Alumno_model extends CI_Model{

    function getMunicipios($id_estado){
        $this->db
        ->select('id, nombre')
        ->from('municipio')
        ->where('id_estado', $id_estado)
        ->order_by('nombre','ASC');

        $query = $this->db->get();
        if($query->num_rows() > 0){
          return $query->result();
        }else{
          return FALSE;
        }
    }
    function registrarAlumno($data){
        $this->db->insert('alumno',$data);
    }
    function getIntelaTotal($id_cliente){
        $this->db
        ->select("*")
        ->from("candidato")
        ->where("status", 1)
        ->where("id_cliente", $id_cliente)
        ->where("eliminado", 0);

        $query = $this->db->get();
        return $query->num_rows();
    }
    function getIntela($id_cliente){
        $this->db
        ->select("c.*, CONCAT(c.nombre,' ',c.paterno,' ',c.materno) as nombreCompleto, est.id as idEstudios,est.primaria_periodo, est.primaria_escuela, est.primaria_ciudad, est.primaria_certificado, est.primaria_validada, est.secundaria_periodo, est.secundaria_escuela, est.secundaria_ciudad, est.secundaria_certificado, est.secundaria_validada, est.preparatoria_periodo, est.preparatoria_escuela, est.preparatoria_ciudad, est.preparatoria_certificado, est.preparatoria_validada, est.licenciatura_periodo, est.licenciatura_escuela, est.licenciatura_ciudad, est.licenciatura_certificado, est.licenciatura_validada, est.otros_certificados, est.comentarios, est.carrera_inactivo, pr.socioeconomico, pr.tipo_antidoping, pr.antidoping, pr.tipo_psicometrico, pr.psicometrico, pr.medico, pr.buro_credito, pr.sociolaboral, pr.otro_requerimiento, cl.nombre as cliente, v.fecha_visita, v.hora_inicio, v.hora_fin")
        ->from('candidato as c')
        ->join('candidato_estudios as est','est.id_candidato = c.id',"left")
        ->join('candidato_pruebas as pr','pr.id_candidato = c.id',"left")
        ->join('visita as v','v.id_candidato = c.id',"left")
        ->join("cliente as cl","cl.id = c.id_cliente")
        ->join("subcliente as sub","sub.id = c.id_subcliente","left")
        ->where('c.id_cliente', $id_cliente)
        //->where('c.eliminado', 0)
        ->order_by('c.fecha_alta','DESC');

        $query = $this->db->get();
        if($query->num_rows() > 0){
          return $query->result();
        }else{
          return FALSE;
        }
    }
    
    function getGeneros(){
        $this->db
        ->select('*')
        ->from('genero')
        ->order_by('nombre','ASC');

        $query = $this->db->get();
        if($query->num_rows() > 0){
          return $query->result();
        }else{
          return FALSE;
        }
    }
    function getEstadosCiviles(){
        $this->db
        ->select('*')
        ->from('estado_civil')
        ->where('id !=', 7)
        ->order_by('nombre','ASC');

        $query = $this->db->get();
        if($query->num_rows() > 0){
          return $query->result();
        }else{
          return FALSE;
        }
    }
    function getTiposTransportes(){
        $this->db
        ->select('*')
        ->from('tipo_transporte')
        ->order_by('nombre','ASC');

        $query = $this->db->get();
        if($query->num_rows() > 0){
          return $query->result();
        }else{
          return FALSE;
        }
    }
}