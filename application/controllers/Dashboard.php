<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller{

	function __construct(){
		parent::__construct();
	}

	public function index(){
		if($this->session->userdata('logueado') && $this->session->userdata('tipo') == 1){
			$data['permisos'] = $this->usuario_model->getPermisos($this->session->userdata('id'));
			$data['permisos_subclientes'] = $this->usuario_model->getPermisosSubclientes($this->session->userdata('id'));
			$data['accesos'] = $this->usuario_model->getModulos($this->session->userdata('idrol'));
			foreach($data['accesos'] as $acceso) {
	      		$items[] = $acceso->id_operaciones;
	      	}
	      	$data['acceso'] = $items;
			if($this->session->userdata('idrol') == 1 || $this->session->userdata('idrol') == 6){
				$num = $this->estadistica_model->countCandidatos();
				$data['dato_totalcandidatos'] = $num->total;
				$num = $this->estadistica_model->countCandidatosCancelados();
				$data['dato_2'] = $num->total;
				$data['texto_2'] = "Candidatos cancelados";
				$num = $this->estadistica_model->countCandidatosEliminados();
				$data['dato_3'] = $num->total;
				$data['texto_3'] = "Candidatos eliminados";
				
			}
			if($this->session->userdata('idrol') == 2){
				$num = $this->estadistica_model->countCandidatosAnalista($this->session->userdata('id'));
				$data['dato_totalcandidatos'] = $num->total;
				$num = $this->estadistica_model->countCandidatosSinFormulario($this->session->userdata('id'));
				$data['dato_2'] = $num->total;
				$data['texto_2'] = "Candidatos sin envío de formulario";
				$num = $this->estadistica_model->countCandidatosSinDocumentos($this->session->userdata('id'));
				$data['dato_3'] = $num->total;
				$data['texto_3'] = "Candidatos sin envío de documentos";
			}
			$this->load
			->view('adminpanel/header',$data)
			->view('adminpanel/index')
			->view('adminpanel/scripts')
			->view('adminpanel/footer');
		}
		else{
			redirect('Login/index');
		}
	}

	public function ustglobal_panel(){
		if($this->session->userdata('logueado') && $this->session->userdata('tipo') == 2){
			$this->load->view('clientes/ust_cliente');
		}
		else{
			redirect('Login/index');
		}
	}

	function candidate_form(){
		if($this->session->userdata('logueado') && $this->session->userdata('tipo') == 3){
			$data['tiene_aviso'] = $this->candidato_model->checkAvisoPrivacidad($this->session->userdata('id'));
			$data['estados'] = $this->candidato_model->getEstados();
			$this->load->view('candidato/index',$data);
		}
		else{
			redirect('Login/index');
		}
	}
	function subcliente_candidate_form(){
		if($this->session->userdata('logueado') && $this->session->userdata('tipo') == 3){
			$data['tiene_aviso'] = $this->candidato_model->checkAvisoPrivacidad($this->session->userdata('id'));
			$data['estados'] = $this->candidato_model->getEstados();
			$data['studies'] = $this->candidato_model->getTiposStudies();
			$this->load->view('candidato/subcliente_ingles_form',$data);
		}
		else{
			redirect('Login/index');
		}
	}
	public function project_candidate(){
		if($this->session->userdata('logueado') && $this->session->userdata('tipo') == 3){
			$data['tiene_aviso'] = $this->candidato_model->checkAvisoPrivacidad($this->session->userdata('id'));
			$data['estados'] = $this->candidato_model->getEstados();
			$data['studies'] = $this->candidato_model->getTiposStudies();
			switch ($this->session->userdata('proyecto')) {
				case 20:
				case 23:
				case 25:
				case 26:
				case 28:
				case 35:
					$this->load->view('proyectos/hcl_standard',$data);
					break;
				case 21:
				case 24:
				case 27:
					$this->load->view('proyectos/hcl_usaa',$data);
					break;
			}
		}
		else{
			redirect('Login/index');
		}
	}
	public function candidate_documents(){
		if($this->session->userdata('logueado') && $this->session->userdata('tipo') == 3){
			$data['id_candidato'] = $this->session->userdata('id');
			$data['nombre'] = $this->session->userdata('nombre');
			$data['paterno'] = $this->session->userdata('paterno');
			$datos['documentos'] = $this->candidato_model->checkDocsCandidato($this->session->userdata('id'));

			if($this->session->userdata('proyecto') == 28 || $this->session->userdata('proyecto') == 26 || $this->session->userdata('proyecto') == 25 || $this->session->userdata('proyecto') == 20){
				if($datos['documentos']){
					$docs = array();
					foreach($datos['documentos'] as $doc){
						if($doc->id_tipo_documento == 3 || $doc->id_tipo_documento == 8 || $doc->id_tipo_documento == 12 || $doc->id_tipo_documento == 7 || $doc->id_tipo_documento == 10 || $doc->id_tipo_documento == 9)
							$docs[] = $doc->id_tipo_documento;
					}
					$data['docs_candidato'] = $docs;
				}
				else{
					$docs = array();
					$data['docs_candidato'] = $docs;
				}

				$this->load->view('candidato/upload_documents',$data);
			}
			if($this->session->userdata('proyecto') == 27 || $this->session->userdata('proyecto') == 24 || $this->session->userdata('proyecto') == 21){
				if($datos['documentos']){
					$docs = array();
					foreach($datos['documentos'] as $doc){
						if($doc->id_tipo_documento == 3 || $doc->id_tipo_documento == 8 || $doc->id_tipo_documento == 12 || $doc->id_tipo_documento == 7 || $doc->id_tipo_documento == 10 || $doc->id_tipo_documento == 9 || $doc->id_tipo_documento == 2)
							$docs[] = $doc->id_tipo_documento;
					}
					$data['docs_candidato'] = $docs;
				}
				else{
					$docs = array();
					$data['docs_candidato'] = $docs;
				}

				$this->load->view('candidato/upload_documents_completo',$data);
			}
			if($this->session->userdata('proyecto') == 23){
				if($datos['documentos']){
					$docs = array();
					foreach($datos['documentos'] as $doc){
						if($doc->id_tipo_documento == 3 || $doc->id_tipo_documento == 8 || $doc->id_tipo_documento == 12 || $doc->id_tipo_documento == 7 || $doc->id_tipo_documento == 10 || $doc->id_tipo_documento == 9 || $doc->id_tipo_documento == 15)
							$docs[] = $doc->id_tipo_documento;
					}
					$data['docs_candidato'] = $docs;
				}
				else{
					$docs = array();
					$data['docs_candidato'] = $docs;
				}

				$this->load->view('candidato/upload_documents_militar',$data);
			}
			if($this->session->userdata('proyecto') == 35){
				if($datos['documentos']){
					$docs = array();
					foreach($datos['documentos'] as $doc){
						if($doc->id_tipo_documento == 3 || $doc->id_tipo_documento == 8 || $doc->id_tipo_documento == 12 || $doc->id_tipo_documento == 7 || $doc->id_tipo_documento == 10 || $doc->id_tipo_documento == 9 || $doc->id_tipo_documento == 14 || $doc->id_tipo_documento == 20)
							$docs[] = $doc->id_tipo_documento;
					}
					$data['docs_candidato'] = $docs;
				}
				else{
					$docs = array();
					$data['docs_candidato'] = $docs;
				}

				$this->load->view('candidato/upload_documents_migratorio',$data);
			}
			if($this->session->userdata('proyecto') == 0 || $this->session->userdata('proyecto') == ""){
				if($datos['documentos']){
					$docs = array();
					foreach($datos['documentos'] as $doc){
						if($doc->id_tipo_documento == 3 || $doc->id_tipo_documento == 8 || $doc->id_tipo_documento == 12 || $doc->id_tipo_documento == 7 || $doc->id_tipo_documento == 10)
							$docs[] = $doc->id_tipo_documento;
					}
					$data['docs_candidato'] = $docs;
				}
				else{
					$docs = array();
					$data['docs_candidato'] = $docs;
				}
				
				$this->load->view('candidato/upload',$data);
			}
			if($this->session->userdata('proyecto') == 150 || $this->session->userdata('proyecto') == 151 || $this->session->userdata('proyecto') == 152 || $this->session->userdata('proyecto') == 153 || $this->session->userdata('proyecto') == 154){
				if($datos['documentos']){
					$docs = array();
					foreach($datos['documentos'] as $doc){
						if($doc->id_tipo_documento == 3 || $doc->id_tipo_documento == 8 || $doc->id_tipo_documento == 12 || $doc->id_tipo_documento == 7 || $doc->id_tipo_documento == 14 || $doc->id_tipo_documento == 9)
							$docs[] = $doc->id_tipo_documento;
					}
					$data['docs_candidato'] = $docs;
				}
				else{
					$docs = array();
					$data['docs_candidato'] = $docs;
				}
				
				$this->load->view('candidato/carga_docs_hcl_international',$data);
			}
			if($this->session->userdata('proyecto') == 155 || $this->session->userdata('proyecto') == 156 || $this->session->userdata('proyecto') == 157){
				if($datos['documentos']){
					$docs = array();
					foreach($datos['documentos'] as $doc){
						if($doc->id_tipo_documento == 3 || $doc->id_tipo_documento == 8 || $doc->id_tipo_documento == 12 || $doc->id_tipo_documento == 7 || $doc->id_tipo_documento == 14 || $doc->id_tipo_documento == 9)
							$docs[] = $doc->id_tipo_documento;
					}
					$data['docs_candidato'] = $docs;
				}
				else{
					$docs = array();
					$data['docs_candidato'] = $docs;
				}
				
				$this->load->view('candidato/carga_documentacion_internacional_simple',$data);
			}
			if($this->session->userdata('proyecto') == 157){
				if($datos['documentos']){
					$docs = array();
					foreach($datos['documentos'] as $doc){
						if($doc->id_tipo_documento == 3 || $doc->id_tipo_documento == 8 || $doc->id_tipo_documento == 7 || $doc->id_tipo_documento == 14 || $doc->id_tipo_documento == 9)
							$docs[] = $doc->id_tipo_documento;
					}
					$data['docs_candidato'] = $docs;
				}
				else{
					$docs = array();
					$data['docs_candidato'] = $docs;
				}
				
				$this->load->view('candidato/carga_documentacion_internacional_especial',$data);
			}
		}
		else{
			redirect('Login/index');
		}
	}
	public function hcl_panel(){
		$data['proyectos'] = $this->candidato_model->getProyectosCliente($this->session->userdata('idcliente'));
		$data['subclientes'] = $this->cliente_model->getSubclientes($this->session->userdata('idcliente'));
		if($this->session->userdata('logueado') && $this->session->userdata('tipo') == 2){
			$this->load->view('clientes/hcl_cliente', $data);
		}
		else{
			redirect('Login/index');
		}
	}
	public function tata_panel(){
		/*$data['baterias'] = $this->configuracion_model->getBateriasPsicometricas();
		$data['proyectos'] = $this->candidato_model->getProyectosCliente($this->session->userdata('idcliente'));
		$data['subclientes'] = $this->cliente_model->getSubclientes($this->session->userdata('idcliente'));*/
		if($this->session->userdata('logueado') && $this->session->userdata('tipo') == 2){
			$this->load->view('clientes/tata_cliente');
		}
		else{
			redirect('Login/index');
		}
	}
	
	
		
	function clientes_panel(){
		$data['subclientes'] = $this->cliente_model->getSubclientes($this->session->userdata('idcliente'));
		//$data['baterias'] = $this->configuracion_model->getBateriasPsicometricas();
		//$data['puestos'] = $this->cliente_model->getPuestos();
		//$data['drogas'] = $this->cliente_model->getPaquetesAntidoping();
	 	$this->load->view('clientes/clientes_index',$data);
	}
	/*----------------------------------------*/
	/* Visitador
	/*----------------------------------------*/ 
		function visitador_panel(){
			if($this->session->userdata('logueado') && $this->session->userdata('tipo') == 1 && $this->session->userdata('idrol') == 3){
				$data['parentescos'] = $this->funciones_model->getParentescos();
				$data['civiles'] = $this->funciones_model->getCiviles();
				$data['escolaridades'] = $this->funciones_model->getEscolaridades();
				$data['zonas'] = $this->funciones_model->getNivelesZona();
				$data['viviendas'] = $this->funciones_model->getTiposVivienda();
				$data['condiciones'] = $this->funciones_model->getTiposCondiciones();
				$data['visitas'] = $this->candidato_model->getCandidatosVisitador();
				$this->load->view('visitador/visitador_index',$data);
			}
			else{
				redirect('Login/index');
			}
		}
	/*----------------------------------------*/
	/* Panel Subclientes Espanol General
	/*----------------------------------------*/ 
		function subclientes_general_panel(){
			$data['puestos'] = $this->funciones_model->getPuestos();
			$data['drogas'] = $this->funciones_model->getPaquetesAntidoping();
			$data['candidatos'] = $this->subcliente_model->getCandidatos($this->session->userdata('idsubcliente'));
			$this->load->view('subclientes/subclientes_general_index',$data);
		}

	/*----------------------------------------*/
	/* Panel Subclientes Ingles REMOTE TEAM
	/*----------------------------------------*/ 
		function subclientes_ingles_panel(){
			$data['puestos'] = $this->funciones_model->getPuestos();
			$data['drogas'] = $this->funciones_model->getPaquetesAntidoping();
			$data['candidatos'] = $this->subcliente_model->getCandidatos($this->session->userdata('idsubcliente'));
			$this->load->view('subclientes/subclientes_ingles_index',$data);
		}
}