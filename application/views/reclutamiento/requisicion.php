<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  	<div id="vacios" class="alert alert-danger fade in mensaje" style='display:none;'>
      	<strong>¡Atención!</strong><span id="txt_vacios"></span>
  	</div>
  	<div id="mensaje" class="alert alert-success in mensaje" style='display:none;'>
      	<strong>¡Éxito!</strong><p id="texto_msj"></p>
  	</div>
	<section class="content-header">
      	<h1 class="titulo_seccion">Requisición de personal</h1>
    	<a class="btn btn-app" id="btn_regresar" onclick="regresar()" style="display: none;">
    		<i class="far fa-arrow-alt-circle-left"></i> <span>Regresar</span>
  		</a>
    </section>
	<div class="modal fade" id="verModal" role="dialog" data-backdrop="static" data-keyboard="false">
	  	<div class="modal-dialog modal-dialog-centered">
	    	<div class="modal-content">
	      		<div class="modal-header">
	      			<h4 class="modal-title" id="titulo_accion"></h4>
	        		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			          <span aria-hidden="true">&times;</span>
			        </button>
	      		</div>
	      		<div class="modal-body">
	      			<h4 id="nombre_candidato"></h4><br>
	        		<p class="" id="motivo"></p><br>
	    		</div>
		    	<div class="modal-footer">
		      		<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		    	</div>
	  		</div>
		</div>
	</div>
	<div class="modal fade" id="quitarModal" role="dialog" data-backdrop="static" data-keyboard="false">
	  	<div class="modal-dialog modal-dialog-centered">
	    	<div class="modal-content">
	      		<div class="modal-header">
	      			<h4 class="modal-title" id="titulo_accion"></h4>
	        		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			          <span aria-hidden="true">&times;</span>
			        </button>
	      		</div>
	      		<div class="modal-body">
	        		<p class="" id="texto_confirmacion"></p><br>
	    		</div>
		    	<div class="modal-footer">
		      		<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		      		<button type="button" class="btn btn-danger" id="btnGuardar" onclick="ejecutarAccion()">Accept</button>
		    	</div>
	  		</div>
		</div>
	</div>
    <div class="loader" style="display: none;"></div>
    <a href="#" class="scroll-to-top"><i class="fas fa-arrow-up"></i><span class="sr-only">Ir arriba</span></a>
	<section class="content" id="formulario">
		<input type="hidden" class="idCandidato">
		<input type="hidden" class="correo">
		<div class="row">
    		<div class="col-xs-12">
      			<div class="box">
        			<div class="box-body">
        				<div class="box-header tituloSubseccion">
                  			<p class="box-title" id="titulo_personal"><strong>  Datos de facturación</strong><hr></p>
                		</div>
                		<form id="datos">
	                		<div class="row">
	                			<div class="col-md-4">
				        			<label for="nombre">Nombre corto de la empresa *</label>
				        			<input type="text" class="form-control obligado" name="nombre" id="nombre" placeholder="Nombre corto de la empresa" onkeypress="searchCliente()">
				        			<input type="hidden" id="idCliente">
				        			<br>
				        		</div>
				        		<div class="col-md-8">
				        			<label for="razon">Razón social *</label>
				        			<input type="text" class="form-control obligado" name="razon" id="razon" placeholder="Razón social" >
				        			<br>
				        		</div>
					        </div>
				        	<div class="row">
					        	<div class="col-md-4">
				        			<label for="calle">Calle (Dirección de facturación) *</label>
				        			<input type="text" class="form-control obligado" name="calle" id="calle" placeholder="Calle">
				        			<br>
				        		</div>
				        		<div class="col-md-2">
				        			<label for="exterior">No. Exterior *</label>
				        			<input type="text" class="form-control solo_numeros obligado" name="exterior" id="exterior" placeholder="No. Exterior">
				        			<br>
				        		</div>
				        		<div class="col-md-2">
				        			<label for="interior">No. Interior </label>
				        			<input type="text" class="form-control" name="interior" id="interior" placeholder="No. Interior">
				        			<br>
				        		</div>
				        		<div class="col-md-4">
				        			<label for="colonia">Colonia *</label>
				        			<input type="text" class="form-control obligado" name="colonia" id="colonia" placeholder="Colonia">
				        			<br>
				        		</div>
					        </div>
					        <div class="row">
					        	<div class="col-md-4">
				        			<label for="estado">Estado *</label>
				        			<select name="estado" id="estado" class="form-control obligado">
							            <option value="">Selecciona</option>
							            <?php foreach ($estados as $e) {?>
							                <option value="<?php echo $e->id; ?>"><?php echo $e->nombre; ?></option>
							            <?php } ?>
						          	</select>
						          	<br>
				        		</div>
				        		<div class="col-md-4">
				        			<label for="municipio">Municipio *</label>
				        			<select name="municipio" id="municipio" class="form-control obligado" disabled>
							            <option value="">Selecciona</option>
						          	</select>
						          	<br>
				        		</div>
				        		<div class="col-md-2">
				        			<label for="cp">Código postal *</label>
				        			<input type="text" class="form-control solo_numeros obligado" name="cp" id="cp" placeholder="Código postal" maxlength="5">
				        			<br>
				        		</div>
					        </div>
					        <div class="row">
					        	<div class="col-md-3">
				        			<label for="telefono">Teléfono *</label>
				        			<input type="text" class="form-control solo_numeros obligado" name="telefono" id="telefono" placeholder="Teléfono" maxlength="10">
				        			<br>
				        		</div>
					        	<div class="col-md-3">
				        			<label for="correo">Correo *</label>
				        			<input type="text" class="form-control obligado" name="correo" id="correo" placeholder="Correo">
				        			<br>
				        		</div>
				        		<div class="col-md-6">
				        			<label for="contacto">Contacto *</label>
				        			<input type="text" class="form-control obligado" name="contacto" id="contacto" placeholder="Contacto">
				        			<br>
				        		</div>
					        </div>
					        <div class="row">
					        	<div class="col-md-3">
				        			<label for="rfc">RFC *</label>
				        			<input type="text" class="form-control obligado" name="rfc" id="rfc" placeholder="RFC" maxlength="10">
				        			<br>
				        		</div>
					        	<div class="col-md-3">
				        			<label for="pago">Forma de pago *</label>
				        			<select name="pago" id="pago" class="form-control obligado">
							            <option value="">Selecciona</option>
							            <?php foreach ($pagos as $p) {?>
							                <option value="<?php echo $p->id; ?>"><?php echo $p->nombre; ?></option>
							            <?php } ?>
						          	</select>
						          	<br>
				        		</div>
				        		<div class="col-md-6">
				        			<label for="cfdi">Uso de CFDI *</label>
				        			<input type="text" class="form-control obligado" name="cfdi" id="cfdi" value="G03 Gastos en general">
				        			<br>
				        		</div>
					        </div>
					        <div class="box-header tituloSubseccion">
	                  			<p class="box-title" id="titulo_familia"><strong>  Información de la vacante</strong><hr></p>
	                		</div>
					        <div class="row">
					        	<div class="col-md-6">
				        			<label for="puesto">Nombre de la posición *</label>
				        			<input type="text" class="form-control obligado" name="puesto" id="puesto" placeholder="Nombre de la posición" maxlength="10">
				        			<br>
				        		</div>
				        		<div class="col-md-3">
				        			<label for="vacantes">No. de Vacantes *</label>
				        			<input type="text" class="form-control solo_numeros obligado" name="vacantes" id="vacantes" placeholder="No. de Vacantes" maxlength="3">
				        			<br>
				        		</div>
					        </div>
					        <p class="tituloSubseccion">Formación académica requerida</p>
					        <div class="row">
					        	<div class="col-md-3">
		                           <label for="escolaridad">Escolaridad *</label>
				        			<select name="escolaridad" id="escolaridad" class="form-control obligado">
							            <option value="">Selecciona</option>
							            <?php foreach ($escolaridades as $esc) {?>
							                <option value="<?php echo $esc->id; ?>"><?php echo $esc->nombre; ?></option>
							            <?php } ?>
						          	</select>
						          	<br>
		                        </div>
		                        <div class="col-md-3">
		                           <label for="nivel">Nivel de escolaridad *</label>
				        			<select name="nivel" id="nivel" class="form-control obligado">
							            <option value="">Selecciona</option>
							            <?php foreach ($niveles as $n) {?>
							                <option value="<?php echo $n->id; ?>"><?php echo $n->nombre; ?></option>
							            <?php } ?>
							            	<option value="otro">Otro</option>
						          	</select>
						          	<br>
		                        </div>
		                        <div class="col-md-3">
		                           <label for="otro_nivel">Otro nivel de escolaridad </label>
				        			<input type="text" class="form-control" name="otro_nivel" id="otro_nivel" placeholder="Otro nivel de escolaridad" disabled>
				        			<br>
		                        </div>
					        </div>
					        <div class="row">
		                        <div class="col-md-12">
		                           <label for="carrera">Carrera requerida para el puesto </label>
				        			<input type="text" class="form-control" name="carrera" id="carrera" placeholder="Carrera requerida para el puesto">
				        			<br>
		                        </div>
					        </div>
					        <div class="row">
		                        <div class="col-md-4">
		                           <label for="idioma">Idioma </label>
				        			<input type="text" class="form-control idioma" name="idioma1" id="idioma1" placeholder="Idioma">
				        			<br>
		                        </div>
		                        <div class="col-md-2">
		                           <label for="porcentaje_idioma">Porcentaje </label>
				        			<input type="text" class="form-control solo_numeros idioma" name="porcentaje_idioma1" id="porcentaje_idioma1" placeholder="Porcentaje" maxlength="3">
				        			<br>
		                        </div>
		                        <br>
					        </div>
					        <div id="div_idiomas"></div>
					        <div class="row">
					        	<div class="col-md-3">
					        		<a class="btn btn-default" onclick="agregarIdioma()">+ Agregar otro idioma</a>
				        			<br><br><br>
					        	</div>
					        </div>
					        <div class="row">
		                        <div class="col-md-12">
		                           <label for="otros_estudios">Otros estudios </label>
				        			<input type="text" class="form-control" name="otros_estudios" id="otros_estudios" placeholder="Otros estudios">
				        			<br>
		                        </div>
					        </div>
					        <div class="row">
		                        <div class="col-md-4">
		                           	<label for="hab1">Habilidad informática requerida </label>
				        			<input type="text" class="form-control habilidad" name="hab1" id="hab1">
				        			<br>
		                        </div>
		                        <div class="col-md-2">
		                           	<label for="porcentaje_hab1">Porcentaje </label>
				        			<input type="text" class="form-control solo_numeros habilidad" name="porcentaje_hab1" id="porcentaje_hab1" placeholder="Porcentaje" maxlength="3">
				        			<br>
		                        </div>
					        </div>
					        <div id="div_habilidades"></div>
					        <div class="row">
					        	<div class="col-md-3">
					        		<a class="btn btn-default" onclick="agregarHabilidad()">+ Agregar otra habilidad informática</a>
				        			<br><br><br>
					        	</div>
					        </div>
					        <p class="tituloSubseccion">Perfil de la vacante</p>
					        <div class="row">
					        	<div class="col-md-2">
					        		<label for="genero">Sexo *</label>
				        			<select name="genero" id="genero" class="form-control obligado">
							            <option value="">Selecciona</option>
							            <?php foreach ($generos as $gen) {?>
							                <option value="<?php echo $gen->id; ?>"><?php echo $gen->nombre; ?></option>
							            <?php } ?>
						          	</select>
						          	<br>
					        	</div>
					        	<div class="col-md-2">
					        		<label for="civil">Estado civil *</label>
				        			<select name="civil" id="civil" class="form-control obligado">
							            <option value="">Selecciona</option>
							            <?php foreach ($civiles as $civ) {?>
							                <option value="<?php echo $civ->id; ?>"><?php echo $civ->nombre; ?></option>
							            <?php } ?>
						          	</select>
						          	<br>
					        	</div>
					        	<div class="col-md-2">
		                           	<label for="minima">Edad Mínima *</label>
				        			<input type="text" class="form-control solo_numeros obligado" name="minima" id="minima" placeholder="Edad Mínima" maxlength="2">
				        			<br>
		                        </div>
		                        <div class="col-md-2">
		                           	<label for="maxima">Edad Máxima *</label>
				        			<input type="text" class="form-control solo_numeros obligado" name="maxima" id="maxima" placeholder="Edad Máxima" maxlength="2">
				        			<br>
		                        </div>
		                        <div class="col-md-2">
					        		<label for="licencia">Licencia *</label>
				        			<select name="licencia" id="licencia" class="form-control obligado">
							            <option value="">Selecciona</option>
							            <?php foreach ($licencias as $lic) {?>
							                <option value="<?php echo $lic->id; ?>"><?php echo $lic->nombre; ?></option>
							            <?php } ?>
						          	</select>
						          	<br>
					        	</div>
					        </div>
					        <div class="row">
					        	<div class="col-md-2">
					        		<label for="discapacidad">Discapacidad *</label>
				        			<select name="discapacidad" id="discapacidad" class="form-control obligado">
							            <option value="">Selecciona</option>
							            <?php foreach ($discapacidades as $dis) {?>
							                <option value="<?php echo $dis->id; ?>"><?php echo $dis->nombre; ?></option>
							            <?php } ?>
						          	</select>
						          	<br>
					        	</div>
					        	<div class="col-md-2">
					        		<label for="causa">Causa que origina la vacante *</label>
				        			<select name="causa" id="causa" class="form-control obligado">
							            <option value="">Selecciona</option>
							            <?php foreach ($causas as $causa) {?>
							                <option value="<?php echo $causa->id; ?>"><?php echo $causa->nombre; ?></option>
							            <?php } ?>
						          	</select>
						          	<br>
					        	</div>
					        	<div class="col-md-8">
		                           	<label for="residencia">Lugar de residencia *</label>
				        			<input type="text" class="form-control obligado" name="residencia" id="residencia" placeholder="Lugar de residencia" >
				        			<br>
		                        </div>
					        </div>
							<div class="box-header tituloSubseccion">
	                  			<p class="box-title" id="titulo_personal"><strong>  Información sobre el cargo</strong><hr></p>
	                		</div>
	                		<div class="row">
		                		<div class="col-md-2">
					        		<label for="jornada">Jornada laboral *</label>
				        			<select name="jornada" id="jornada" class="form-control obligado">
							            <option value="">Selecciona</option>
							            <?php foreach ($jornadas as $j) {?>
							                <option value="<?php echo $j->id; ?>"><?php echo $j->nombre; ?></option>
							            <?php } ?>
						          	</select>
						          	<br><br>
					        	</div>
					        	<div class="col-md-2">
					        		<label for="hora_inicial">De (Hora) *</label>
				        			<select name="hora_inicial" id="hora_inicial" class="form-control obligado">
				        				<option value="">Selecciona</option>
							            <option value="00:00">00:00 h.</option>
										<option value="00:15">00:15 h.</option>
										<option value="00:30">00:30 h.</option>
										<option value="00:45">00:45 h.</option>
										<option value="01:00">01:00 h.</option>
										<option value="01:15">01:15 h.</option>
										<option value="01:30">01:30 h.</option>
										<option value="01:45">01:45 h.</option>
										<option value="02:00">02:00 h.</option>
										<option value="02:15">02:15 h.</option>
										<option value="02:30">02:30 h.</option>
										<option value="02:45">02:45 h.</option>
										<option value="03:00">03:00 h.</option>
										<option value="03:15">03:15 h.</option>
										<option value="03:30">03:30 h.</option>
										<option value="03:45">03:45 h.</option>
										<option value="04:00">04:00 h.</option>
										<option value="04:15">04:15 h.</option>
										<option value="04:30">04:30 h.</option>
										<option value="04:45">04:45 h.</option>
										<option value="05:00">05:00 h.</option>
										<option value="05:15">05:15 h.</option>
										<option value="05:30">05:30 h.</option>
										<option value="05:45">05:45 h.</option>
										<option value="06:00">06:00 h.</option>
										<option value="06:15">06:15 h.</option>
										<option value="06:30">06:30 h.</option>
										<option value="06:45">06:45 h.</option>
										<option value="07:00">07:00 h.</option>
										<option value="07:15">07:15 h.</option>
										<option value="07:30">07:30 h.</option>
										<option value="07:45">07:45 h.</option>
										<option value="08:00">08:00 h.</option>
										<option value="08:15">08:15 h.</option>
										<option value="08:30">08:30 h.</option>
										<option value="08:45">08:45 h.</option>
										<option value="09:00">09:00 h.</option>
										<option value="09:15">09:15 h.</option>
										<option value="09:30">09:30 h.</option>
										<option value="09:45">09:45 h.</option>
										<option value="10:00">10:00 h.</option>
										<option value="10:15">10:15 h.</option>
										<option value="10:30">10:30 h.</option>
										<option value="10:45">10:45 h.</option>
										<option value="11:00">11:00 h.</option>
										<option value="11:15">11:15 h.</option>
										<option value="11:30">11:30 h.</option>
										<option value="11:45">11:45 h.</option>
										<option value="12:00">12:00 h.</option>
										<option value="12:15">12:15 h.</option>
										<option value="12:30">12:30 h.</option>
										<option value="12:45">12:45 h.</option>
										<option value="13:00">13:00 h.</option>
										<option value="13:15">13:15 h.</option>
										<option value="13:30">13:30 h.</option>
										<option value="13:45">13:45 h.</option>
										<option value="14:00">14:00 h.</option>
										<option value="14:15">14:15 h.</option>
										<option value="14:30">14:30 h.</option>
										<option value="14:45">14:45 h.</option>
										<option value="15:00">15:00 h.</option>
										<option value="15:15">15:15 h.</option>
										<option value="15:30">15:30 h.</option>
										<option value="15:45">15:45 h.</option>
										<option value="16:00">16:00 h.</option>
										<option value="16:15">16:15 h.</option>
										<option value="16:30">16:30 h.</option>
										<option value="16:45">16:45 h.</option>
										<option value="17:00">17:00 h.</option>
										<option value="17:15">17:15 h.</option>
										<option value="17:30">17:30 h.</option>
										<option value="17:45">17:45 h.</option>
										<option value="18:00">18:00 h.</option>
										<option value="18:15">18:15 h.</option>
										<option value="18:30">18:30 h.</option>
										<option value="18:45">18:45 h.</option>
										<option value="19:00">19:00 h.</option>
										<option value="19:15">19:15 h.</option>
										<option value="19:30">19:30 h.</option>
										<option value="19:45">19:45 h.</option>
										<option value="20:00">20:00 h.</option>
										<option value="20:15">20:15 h.</option>
										<option value="20:30">20:30 h.</option>
										<option value="20:45">20:45 h.</option>
										<option value="21:00">21:00 h.</option>
										<option value="21:15">21:15 h.</option>
										<option value="21:30">21:30 h.</option>
										<option value="21:45">21:45 h.</option>
										<option value="22:00">22:00 h.</option>
										<option value="22:15">22:15 h.</option>
										<option value="22:30">22:30 h.</option>
										<option value="22:45">22:45 h.</option>
										<option value="23:00">23:00 h.</option>
										<option value="23:15">23:15 h.</option>
										<option value="23:30">23:30 h.</option>
										<option value="23:45">23:45 h.</option>
						          	</select>
						          	<br>
					        	</div>
					        	<div class="col-md-2">
					        		<label for="hora_final">A (Hora) *</label>
				        			<select name="hora_final" id="hora_final" class="form-control obligado">
				        				<option value="">Selecciona</option>
							            <option value="00:00">00:00 h.</option>
										<option value="00:15">00:15 h.</option>
										<option value="00:30">00:30 h.</option>
										<option value="00:45">00:45 h.</option>
										<option value="01:00">01:00 h.</option>
										<option value="01:15">01:15 h.</option>
										<option value="01:30">01:30 h.</option>
										<option value="01:45">01:45 h.</option>
										<option value="02:00">02:00 h.</option>
										<option value="02:15">02:15 h.</option>
										<option value="02:30">02:30 h.</option>
										<option value="02:45">02:45 h.</option>
										<option value="03:00">03:00 h.</option>
										<option value="03:15">03:15 h.</option>
										<option value="03:30">03:30 h.</option>
										<option value="03:45">03:45 h.</option>
										<option value="04:00">04:00 h.</option>
										<option value="04:15">04:15 h.</option>
										<option value="04:30">04:30 h.</option>
										<option value="04:45">04:45 h.</option>
										<option value="05:00">05:00 h.</option>
										<option value="05:15">05:15 h.</option>
										<option value="05:30">05:30 h.</option>
										<option value="05:45">05:45 h.</option>
										<option value="06:00">06:00 h.</option>
										<option value="06:15">06:15 h.</option>
										<option value="06:30">06:30 h.</option>
										<option value="06:45">06:45 h.</option>
										<option value="07:00">07:00 h.</option>
										<option value="07:15">07:15 h.</option>
										<option value="07:30">07:30 h.</option>
										<option value="07:45">07:45 h.</option>
										<option value="08:00">08:00 h.</option>
										<option value="08:15">08:15 h.</option>
										<option value="08:30">08:30 h.</option>
										<option value="08:45">08:45 h.</option>
										<option value="09:00">09:00 h.</option>
										<option value="09:15">09:15 h.</option>
										<option value="09:30">09:30 h.</option>
										<option value="09:45">09:45 h.</option>
										<option value="10:00">10:00 h.</option>
										<option value="10:15">10:15 h.</option>
										<option value="10:30">10:30 h.</option>
										<option value="10:45">10:45 h.</option>
										<option value="11:00">11:00 h.</option>
										<option value="11:15">11:15 h.</option>
										<option value="11:30">11:30 h.</option>
										<option value="11:45">11:45 h.</option>
										<option value="12:00">12:00 h.</option>
										<option value="12:15">12:15 h.</option>
										<option value="12:30">12:30 h.</option>
										<option value="12:45">12:45 h.</option>
										<option value="13:00">13:00 h.</option>
										<option value="13:15">13:15 h.</option>
										<option value="13:30">13:30 h.</option>
										<option value="13:45">13:45 h.</option>
										<option value="14:00">14:00 h.</option>
										<option value="14:15">14:15 h.</option>
										<option value="14:30">14:30 h.</option>
										<option value="14:45">14:45 h.</option>
										<option value="15:00">15:00 h.</option>
										<option value="15:15">15:15 h.</option>
										<option value="15:30">15:30 h.</option>
										<option value="15:45">15:45 h.</option>
										<option value="16:00">16:00 h.</option>
										<option value="16:15">16:15 h.</option>
										<option value="16:30">16:30 h.</option>
										<option value="16:45">16:45 h.</option>
										<option value="17:00">17:00 h.</option>
										<option value="17:15">17:15 h.</option>
										<option value="17:30">17:30 h.</option>
										<option value="17:45">17:45 h.</option>
										<option value="18:00">18:00 h.</option>
										<option value="18:15">18:15 h.</option>
										<option value="18:30">18:30 h.</option>
										<option value="18:45">18:45 h.</option>
										<option value="19:00">19:00 h.</option>
										<option value="19:15">19:15 h.</option>
										<option value="19:30">19:30 h.</option>
										<option value="19:45">19:45 h.</option>
										<option value="20:00">20:00 h.</option>
										<option value="20:15">20:15 h.</option>
										<option value="20:30">20:30 h.</option>
										<option value="20:45">20:45 h.</option>
										<option value="21:00">21:00 h.</option>
										<option value="21:15">21:15 h.</option>
										<option value="21:30">21:30 h.</option>
										<option value="21:45">21:45 h.</option>
										<option value="22:00">22:00 h.</option>
										<option value="22:15">22:15 h.</option>
										<option value="22:30">22:30 h.</option>
										<option value="22:45">22:45 h.</option>
										<option value="23:00">23:00 h.</option>
										<option value="23:15">23:15 h.</option>
										<option value="23:30">23:30 h.</option>
										<option value="23:45">23:45 h.</option>
						          	</select>
						          	<br>
					        	</div>
					        	<div class="col-md-2">
					        		<label for="dia_inicial">De (Día)*</label>
				        			<select name="dia_inicial" id="dia_inicial" class="form-control obligado">
							            <option value="">Selecciona</option>
							           	<option value="Lunes">Lunes</option>
							          	<option value="Martes">Martes</option>
							           	<option value="Miércoles">Miércoles</option>
							           	<option value="Jueves">Jueves</option>
							           	<option value="Viernes">Viernes</option>
							           	<option value="Sábado">Sábado</option>
							          	<option value="Domingo">Domingo</option>
						          	</select>
						          	<br>
					        	</div>
					        	<div class="col-md-2">
					        		<label for="dia_final">A (Día)*</label>
				        			<select name="dia_final" id="dia_final" class="form-control obligado">
							            <option value="">Selecciona</option>
							           	<option value="Lunes">Lunes</option>
							          	<option value="Martes">Martes</option>
							           	<option value="Miércoles">Miércoles</option>
							           	<option value="Jueves">Jueves</option>
							           	<option value="Viernes">Viernes</option>
							           	<option value="Sábado">Sábado</option>
							          	<option value="Domingo">Domingo</option>
						          	</select>
						          	<br>
					        	</div>
					        	<div class="col-md-2">
		                           	<label for="descanso">Día(s) de descanso *</label>
				        			<input type="text" class="form-control obligado" name="descanso" id="descanso" placeholder="Día(s) de descanso">
				        			<br>
		                        </div>
	                        </div>
	                        <div class="row">
	                        	<div class="col-md-2">
	                        		<label for="viajar">Disponibilidad para viajar *</label>
				        			<select name="viajar" id="viajar" class="form-control obligado">
							            <option value="">Selecciona</option>
							            <option value="0">No</option>
							            <option value="1">Sí</option>
						          	</select>
						          	<br><br><br>
	                        	</div>
	                        	<div class="col-md-2">
	                        		<label for="rolar">Disponibilidad para rolar turnos *</label>
				        			<select name="rolar" id="rolar" class="form-control obligado">
							            <option value="">Selecciona</option>
							            <option value="0">No</option>
							            <option value="1">Sí</option>
						          	</select>
						          	<br>
	                        	</div>
	                        	<div class="col-md-8">
	                        		<label for="zona">Zona de trabajo *</label>
	                        		<textarea name="zona" id="zona" class="form-control obligado" rows="2" placeholder="Zona de trabajo"></textarea>
	                        	</div>
	                        </div>
	                        <div class="row">
	                        	<div class="col-md-2">
	                        		<label for="sueldo">Sueldo *</label>
				        			<select name="sueldo" id="sueldo" class="form-control obligado">
							            <option value="">Selecciona</option>
							            <?php foreach ($sueldos as $su) {?>
							                <option value="<?php echo $su->id; ?>"><?php echo $su->nombre; ?></option>
							            <?php } ?>
						          	</select>
						          	<br>
	                        	</div>
	                        	<div class="col-md-2">
	                        		<label for="variable">Variable por </label>
				        			<select name="variable" id="variable" class="form-control" disabled>
				        				<option value="">Selecciona</option>
						          	</select>
						          	<br>
	                        	</div>
	                        	<div class="col-md-2">
	                        		<label for="variable_cantidad">Cantidad variable</label>
				        			<input type="text" class="form-control" name="variable_cantidad" id="variable_cantidad" placeholder="Cantidad variable" disabled>
				        			<br>
	                        	</div>
	                        	<div class="col-md-2">
	                        		<label for="sueldo_min">Sueldo mínimo *</label>
				        			<input type="text" class="form-control solo_numeros obligado" name="sueldo_min" id="sueldo_min" placeholder="Sueldo mínimo" >
				        			<br>
	                        	</div>
	                        	<div class="col-md-2">
	                        		<label for="sueldo_max">Sueldo máximo *</label>
				        			<input type="text" class="form-control solo_numeros obligado" name="sueldo_max" id="sueldo_max" placeholder="Sueldo máximo" >
				        			<br>
	                        	</div>
	                        </div>
	                        <div class="row">
	                        	<div class="col-md-2">
	                        		<label for="prestaciones">Tipo de prestaciones *</label>
				        			<select name="prestaciones" id="prestaciones" class="form-control obligado">
							            <option value="">Selecciona</option>
							            <?php foreach ($prestaciones as $pres) {?>
							                <option value="<?php echo $pres->id; ?>"><?php echo $pres->nombre; ?></option>
							            <?php } ?>
						          	</select>
						          	<br>
						        </div>
						        <div class="col-md-8">
	                        		<label for="otra_prestacion">Otras prestaciones</label>
				        			<input type="text" class="form-control" name="otra_prestacion" id="otra_prestacion" placeholder="Otras prestaciones" disabled>
				        			<br>
	                        	</div>
	                        </div>
	                        <div class="row">
	                        	<div class="col-md-10">
	                        		<label for="experiencia">Experiencia en *</label>
	                        		<textarea name="experiencia" id="experiencia" class="form-control obligado" rows="3" placeholder="Experiencia en"></textarea><br>
	                        	</div>
	                        </div>
	                        <div class="row">
	                        	<div class="col-md-10">
	                        		<label for="actividades">Actividades a realizar *</label>
	                        		<textarea name="actividades" id="actividades" class="form-control obligado" rows="3" placeholder="Actividades a realizar"></textarea><br>
	                        	</div>
	                        </div>
	                        <div class="box-header tituloSubseccion">
	                  			<p class="box-title" id="titulo_personal"><strong> Perfil del cargo</strong><hr></p>
	                		</div>
	                		<p class="tituloSubseccion">Competencia requerida para el puesto</p><br>
	                		<div class="row" id="div_competencias"><?php echo $competencias; ?></div><br>
	                		<div class="row">
	                        	<div class="col-md-10">
	                        		<label for="observaciones">Observaciones adicionales</label>
	                        		<textarea name="observaciones" id="observaciones" class="form-control" rows="3" placeholder="Observaciones adicionales"></textarea><br>
	                        	</div>
	                        </div>
	                        <div class="row">
	                        	<div class="col-md-10">
	                        		<label for="requisito">Requisitos especiales</label>
	                        		<textarea name="requisito" id="requisito" class="form-control" rows="3" placeholder="Requisitos especiales"></textarea><br>
	                        	</div>
	                        </div>
					    </form>
                		<div class="row">
                			<div class="col-md-4 col-md-offset-5 margen-top">
                				<a href="javascript:void(0)" class="btn btn-primary" onclick="crearRequisicion()">Generar requisición</a>
                			</div>
                		</div>
        			</div>
        		</div>
        	</div>
        </div>
	</section>
	
</div>
<!-- /.content-wrapper -->	
<script>
	var i = 1;
	var j = 1;
	$(document).ready(function(){
		var msj = localStorage.getItem("success");
		if(msj == 1){
  			$("#texto_msj").text("Se ha creado la requisición correctamente");
          	$('#mensaje').css("display", "block");
          	setTimeout(function(){
            	$('#mensaje').fadeOut();
          	},6000);
        	localStorage.removeItem("success");
  		}
		$("#estado").change(function(){
	      	var id_estado = $(this).val();
	      	if(id_estado != ""){
	        	$.ajax({
	          		url: '<?php echo base_url('Reclutamiento/getMunicipios'); ?>',
	          		method: 'POST',
	          		data: {'id_estado':id_estado},
	          		dataType: "text",
	          		success: function(res)
	          		{
	            		$('#municipio').prop('disabled', false);
	            		$('#municipio').html(res);
	          		},error:function(res)
	          		{
	            		//$('#errorModal').modal('show');
	          		}
	        	});
	      	}
	      	else{
        		$('#municipio').prop('disabled', true);
	        	$('#municipio').append($("<option selected></option>").attr("value","").text("Selecciona"));
	      	}
	    });
	    $("#nivel").change(function(){
	    	var escolaridad = $(this).val();
	    	if(escolaridad != "otro"){
	    		$("#otro_nivel").prop('disabled',true);
	    		$("#otro_nivel").removeClass("obligado");
	    		$("#otro_nivel").val("");

	    	}
	    	if(escolaridad == "otro"){
	    		$("#otro_nivel").prop('disabled',false);
	    		$("#otro_nivel").addClass("obligado");
	    		$("#otro_nivel").val("");
	    	}
	    });
	    $("#sueldo").change(function(){
	      	var sueldo = $(this).val();
	      	if(sueldo != "" && sueldo == 3){
	        	$.ajax({
	          		url: '<?php echo base_url('Reclutamiento/getSueldoVariables'); ?>',
	          		method: 'POST',
	          		data: {'sueldo':sueldo},
	          		dataType: "text",
	          		success: function(res)
	          		{
	          			$('#variable').addClass('obligado');
	            		$('#variable').prop('disabled', false);
	            		$('#variable').html(res);
	          		},error:function(res)
	          		{
	            		//$('#errorModal').modal('show');
	          		}
	        	});
	      	}
	      	else{
	      		$('#variable').removeClass('obligado');
        		$('#variable').prop('disabled', true);
	        	$('#variable').append($("<option selected></option>").attr("value","").text("Selecciona"));
	      	}
	    });
	    $("#variable").change(function(){
	      	var variable = $(this).val();
	      	if(variable != ""){
	      		$('#variable_cantidad').val('');
	        	$('#variable_cantidad').addClass('obligado');
	            $('#variable_cantidad').prop('disabled', false);
	      	}
	      	else{
	        	$('#variable_cantidad').removeClass('obligado');
	            $('#variable_cantidad').prop('disabled', true);
	            $('#variable_cantidad').val('');
	      	}
	    });
	    $("#prestaciones").change(function(){
	      	var prestacion = $(this).val();
	      	if(prestacion != "" && prestacion == 3){
	      		$('#otra_prestacion').val('');
	        	$('#otra_prestacion').addClass('obligado');
	            $('#otra_prestacion').prop('disabled', false);
	      	}
	      	else{
	        	$('#otra_prestacion').removeClass('obligado');
	            $('#otra_prestacion').prop('disabled', true);
	            $('#otra_prestacion').val('');
	      	}
	    });
	    $("#nombre").change(function(){
	      	var nombre = $(this).val();
	      	var id = $("#idCliente").val();
	      	if(nombre == ""){
	      		$('#idCliente').val('');
	      	}
	      	else{
	      		if(id != ""){
	      			$.ajax({
		          		url: '<?php echo base_url('Reclutamiento/getDatosCliente'); ?>',
		          		method: 'POST',
		          		data: {'id':id},
		          		dataType: "text",
		          		success: function(res)
		          		{
		          			if(res != ""){
		          				var data = res.split(',');
		          				$("#calle").val(data[0]);
		          				$("#exterior").val(data[1]);
		          				$("#interior").val(data[2]);
		          				$("#colonia").val(data[3]);
		          				$("#estado").val(data[4]);
		          				$("#cp").val(data[6]);
		          				$("#telefono").val(data[7]);
		          				$("#correo").val(data[8]);
		          				$("#contacto").val(data[9]);
		          				$("#rfc").val(data[10]);
		          				$("#razon").val(data[11]);
		          				seleccionarMunicipio(data[4],data[5]);

		          			}
		          		},error:function(res)
		          		{
		            		//$('#errorModal').modal('show');
		          		}
		        	});
	      		}
	      	}
	    });
	});
	function seleccionarMunicipio(id_estado,id_municipio){
		$.ajax({
      		url: '<?php echo base_url('Reclutamiento/getMunicipios'); ?>',
      		method: 'POST',
      		data: {'id_estado':id_estado},
      		dataType: "text",
      		success: function(res)
      		{
        		$('#municipio').prop('disabled', false);
        		$('#municipio').html(res);
        		$('#municipio').val(id_municipio).attr('selected', 'selected');
      		},error:function(res)
      		{
        		//$('#errorModal').modal('show');
      		}
    	});
	}
	function agregarHabilidad(){
		i++;
		$("#div_habilidades").append('<div class="row"><div class="col-md-4"><label for="hab'+i+'">Habilidad informática requerida </label><input type="text" class="form-control habilidad" name="hab'+i+'" id="hab'+i+'"><br></div><div class="col-md-2"><label for="porcentaje_hab'+i+'">Porcentaje</label><input type="text" class="form-control solo_numeros habilidad" name="porcentaje_hab'+i+'" id="porcentaje_hab'+i+'" placeholder="Porcentaje" maxlength="3"><br></div></div>');
	}
	function agregarIdioma(){
		j++;
		$("#div_idiomas").append('<div class="row"><div class="col-md-4"><label>Idioma </label><input type="text" class="form-control idioma" name="idioma'+j+'" id="idioma'+j+'" placeholder="Idioma"><br></div><div class="col-md-2"><label>Porcentaje </label><input type="text" class="form-control solo_numeros idioma" name="porcentaje_idioma'+j+'" id="porcentaje_idioma'+j+'" placeholder="Porcentaje" maxlength="3"><br></div></div><br>');
	}
	function getMunicipio(id_estado, id_municipio){
		$.ajax({
        	url: '<?php echo base_url('index.php/Candidato/getMunicipios'); ?>',
        	method: 'POST',
        	data: {'id_estado':id_estado},
        	dataType: "text",
        	success: function(res){
          		$('#municipio').prop('disabled', false);
          		$('#municipio').html(res);
          		$("#municipio").find('option').attr("selected",false) ;
          		$('#municipio option[value="'+id_municipio+'"]').attr('selected', 'selected');
        	},error:function(res){
          		$('#errorModal').modal('show');
        	}
      	});
	}
	function crearRequisicion(){
		var datos = $("#datos").serialize();
		datos += "&otro_nivel_escolar="+$("#otro_nivel").val();
		datos += "&sueldo_variable="+$("#variable").val();
		datos += "&variable_monto="+$("#variable_cantidad").val();
		datos += "&otras_prestaciones="+$("#otra_prestacion").val();
		datos += "&id_cliente="+$("#idCliente").val();
        var habilidades = "";
        var total_habilidades = $(".habilidad").length;
      	if(total_habilidades > 0){
        	for(var i = 1; i <= total_habilidades / 2; i++){
				habilidades += $("#hab"+i).val()+",";
				habilidades += $("#porcentaje_hab"+i).val()+"@@";
			}
        }
        var idiomas = "";
        var total_idiomas = $(".idioma").length;
      	if(total_idiomas > 0){
        	for(var i = 1; i <= total_idiomas / 2; i++){
				idiomas += $("#idioma"+i).val()+",";
				idiomas += $("#porcentaje_idioma"+i).val()+"@@";
			}
        }
		var total = $('.obligado').filter(function(){
      		return !$(this).val();
    	}).length;

	    if(total > 0){
	      	$(".obligado").each(function() {
		        var element = $(this);
		        if (element.val() == "") {
		          	element.addClass("requerido");
		          	$("#txt_vacios").text("Hay campos vacíos");
		          	$('#vacios').css("display", "block");
		          	setTimeout(function(){
		            	$('#vacios').fadeOut();
		          	},5000);
		        }
		        else{
		          	element.removeClass("requerido");
		        }
	      	});
	    }
	    else{
	    	//console.log(datos);
	    	$.ajax({
	      		url: '<?php echo base_url('Reclutamiento/crearRequisicion'); ?>',
	      		method: "POST",  
	            data: {'datos':datos,'habilidades':habilidades,'idiomas':idiomas}, 
	            dataType: "text",
	      		beforeSend: function() {
	            	$('.loader').css("display","block");
	          	},
	      		success: function(res)
	      		{
      				setTimeout(function(){
	              		$('.loader').fadeOut();
	            	},2000);
	            	localStorage.setItem("success", 1);
	            	location.reload();

	      		},error:function(res)
	      		{
	        		//$('#errorModal').modal('show');
	      		}
	    	});
	    }
	}
	function searchCliente(){
      	$("#nombre").autocomplete({
        source: "<?php echo site_url('Reclutamiento/matchCliente'); ?>", // path to the get_birds method

        select: function(event, ui) {
              $('#nombre').val(ui.item.label);
              //$('#idCliente').val(ui.item.value);
              return false;
          },
          focus: function(event, ui) {
            $("#nombre").val(ui.item.label);
            $('#idCliente').val(ui.item.value);
            return false;
          }
      	});
	}
  	function regresar(){
  		$("#listado").css('display','block');
  		$("#btn_regresar").css('display','none'); 
  		$("#formulario").css('display','none');
  	}
  	$("#an_refLab1_recontratacion").change(function(){
  		var valor = $(this).val();
  		if(valor != -1){
  			$("#an_refLab1_motivo").addClass('obligado');
  			$("#an_refLab1_motivo").prop('disabled',false);
  			$("#an_refLab1_motivo").val("");
  		}
  		else{
  			$("#an_refLab1_motivo").removeClass('obligado');
  			$("#an_refLab1_motivo").prop('disabled',true);
  			$("#an_refLab1_motivo").val("");
  		}
  	});
  	//Acepta solo numeros en los input
  	$(".solo_numeros").on("input", function(){
	    var valor = $(this).val();
	    $(this).val(valor.replace(/[^0-9]/g, ''));
	});
	//Limpiar inputs
	$(".personal_obligado, .check_obligado, .familia_obligado, .estudios_obligado, #trabajo_inactivo, .refPer_obligado").focus(function(){
		$(this).removeClass("requerido");
	});
</script>