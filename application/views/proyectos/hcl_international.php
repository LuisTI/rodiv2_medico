<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<title>Candidates | RODI</title>
	<link rel="stylesheet" href="<?php echo base_url() ?>css/candidato/bootstrap.min.css">
	<link rel="stylesheet" href="<?php echo base_url() ?>css/candidato/style.css">
	<link rel="stylesheet" href="<?php echo base_url() ?>css/candidato/bootstrap-datetimepicker.min.css">
	<link rel="stylesheet" href="<?php echo base_url() ?>css/candidato/skins/all.css">
	<link rel="icon" type="image/jpg" href="<?php echo base_url() ?>img/favicon.jpg" sizes="64x64">
	<!-- Google Font -->
	<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
	<script src="https://kit.fontawesome.com/fdf6fee49b.js"></script>
	<!-- DataTable -->
	<link rel="stylesheet" href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
	<script src="https://kit.fontawesome.com/fdf6fee49b.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
</head>
<body>
	<div id="exito" class="alert alert-success mensaje" style='display:none;'>
      	<strong>¡Success!</strong> The form has been send succesfully.
  	</div>
  	<div id="empty" class="alert alert-danger mensaje" style='display:none;'>
      	<strong>¡Attention!</strong> There are empty required fields.
  	</div>
  	<div id="no_file" class="alert alert-danger mensaje" style='display:none;'>
      	<strong>¡Attention!</strong> Please select files.
  	</div>
  	<div id="no_check" class="alert alert-danger mensaje" style='display:none;'>
      	<strong>¡Attention!</strong> Please check the privacy notice.
  	</div>
	<div class="modal fade" id="errorModal" role="dialog">
	  	<div class="modal-dialog">
	    	<div class="modal-content">
	      		<div class="modal-header">
	        		<button type="button" class="close" data-dismiss="modal">&times;</button>
	        		<h4 class="modal-title">¡Something is wrong!</h4>
	      		</div>
	      		<div class="modal-body">
	        		<p class="text-red">Contact the admin please.</p>
	    		</div>
		    	<div class="modal-footer">
		      		<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
		    	</div>
	  		</div>
		</div>
	</div>
	<div class="modal fade" id="mensajeModal" role="dialog" data-backdrop="static" data-keyboard="false">
	  	<div class="modal-dialog modal-lg">
	    	<div class="modal-content">
	      		<div class="modal-header">
	      			<h4 class="modal-title">Form completed</h4>
	      		</div>
	      		<div class="modal-body">
	      			<p>The next step is to upload your documents. When you have them all. You must access to this link with your same credentials in order to upload your documents in pdf, jpg or png formats (max. 2MB each)</p>
	    		</div>
	  		</div>
		</div>
	</div>
	<div class="modal fade" id="nondisclosureModal" role="dialog" data-backdrop="static" data-keyboard="false">
	  	<div class="modal-dialog modal-xl">
	    	<div class="modal-content">
	      		<div class="modal-header">
	      			<h4 class="modal-title">Privacy notice</h4>
	        		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			          <span aria-hidden="true">&times;</span>
			        </button>
	      		</div>
	      		<div class="modal-body">
	      			<p>In connection with your employment or application for employment, RO & DI GLOBAL MARKETING S DE RL DE CV (hereinafter The Company) is required to collect, receive and administer certain personal data including, but not limited to:<br>a) Name, address, telephone number, e-mail address, photographs/video of your current address and other contact information;<br>b) Nationality, citizenship, federal taxpayer registry, social security number and marital status.<br>c) Information regarding your current and previous employment, current and previous positions, work permit, work experience and salary, INFONAVIT, IMMS and other public institution history.<br>d) Skills and abilities, including language proficiency, education, including degrees obtained and institutions attended; and <br>e) Data on family members, spouse and children, as the case may be, and any other information that may be necessary in relation to the present, among others. Such personal data may be collected at any time by the Company or any other subsidiary or affiliated entities of The Company. You are hereby informed that some of the personal data that will be collected may be considered as Sensitive Personal Data in accordance with the Law on the Protection of Personal Data in the possession of Individuals.</b></p>
	      			<p>The company may use your information and personal data for various purposes and for business activities such as the following: <br>a) To provide you with personnel placement services, to process jobs or temporary assignments for you, or for us to send you with a contractor. B) To evaluate whether you are qualified for a position or function.<br>c) To contact you regarding available positions or services we offer.<br>d) To inform our customers and business partners about our services. <br>e) To carry out research and analysis, as well as to generate job profiles and structures to increase and improve productivity, as well as for any other purposes that The Company deems necessary for the improvement of working conditions, and to resolve or defend complaints and legal claims.</p>
	      			<p>The Company may share and/or transfer your personal information and data to its affiliated companies and national subsidiaries and to third parties, including, but not limited to, customers, suppliers, advisors, consultants and national business partners. We may also share information: <br>a) With customers who may have available employment opportunities or who have an interest in hiring our candidates or employees.<br>b) When such information is requested or reasonably required by any governmental authority or other governmental official responsible for law enforcement, or when required by law or in response to legal process. <br>c) When reasonably necessary to conduct an investigation into suspected or actual illegal activities. <br> <b>The company will treat your personal information and data as confidential and will maintain preventive measures aimed at protecting it against loss, misuse, unauthorized access, alteration or destruction, not to disclose it for any purpose other than that established with this Privacy Notice.</b></p>
	      			<p>The Company communicates its guidelines and rules on privacy and security to our employees, customers and suppliers. <br> By signing this Privacy Notice, you acknowledge that you understand and accept the collection and transmission of your information and personal data by The Company as outlined in this Privacy Notice.  In compliance with the provisions of the <b>Federal Law for the Protection of Personal Data in Possession of Individuals </b> we inform you of the following: <br> Company Address: Benito Juárez # 5693, Col. Santa María del Pueblito, Zapopan, Jalisco <br>Contact Information: VISITOR Tel. (33)14044054 <br>ARCO Rights: According to the <b> Federal Law for the Protection of Personal Data in Possession of Individuals</b> You have the right to exercise at any time your rights of access, rectification, cancellation and opposition (the "ARCO Rights") of your information, through a written request addressed to the VISITOR, who may request for your protection and benefit, documentation that accredits corrections to the data in case you request rectification of them.</p>
	      			<p>You have the right to access to your personal data that we have and to the details of the treatment of the same as long as you contact us in the period of time that the information stays in the company understood until the information is sent to the contracting party (that from that moment is eliminated of our data base) or if it is not sent you will have 10 working days after granting the information, since after this period of time all related information is erased. However, the company is not responsible for the use made by the contractor of the socioeconomic study once it receives the information, so it is necessary to contact the contractor directly. You can also cancel them when you consider that they are not required for any of the purposes indicated in this privacy notice, are being used for non-consented purposes or have terminated the contractual relationship or service, or oppose the treatment of them for specific purposes. You are hereby informed that you may not limit the use of your personal data when such use is required for the fulfilment of any obligation on the part of The Companies.</p><br>
	      			<p>Sincerely <br>RO & DI GLOBAL MARKETING S DE RL DE CV <br>VISITOR</p>
	      			<div class="row">
	      				<div class="col-md-3 offset-md-6">
	      					<button type="button" class="btn btn-primary" data-dismiss="modal">I agree</button>
	      				</div>
	      			</div>
	    		</div>
	  		</div>
		</div>
	</div>
	<header>
		<nav class="navbar navbar-expand-lg navbar-light bg-rodi">
		  	<a class="navbar-brand" href="#">
		  		<img src="<?php echo base_url() ?>/img/favicon.jpg" width="32" height="32" class="d-inline-block align-top">
		  		<?php echo $this->session->userdata('nombre')." ".$this->session->userdata('paterno')." ".$this->session->userdata('materno'); ?>
		  	</a>
		  	<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
		    <span class="navbar-toggler-icon"></span>
		  	</button>
		  	<div class="collapse navbar-collapse" id="navbarNavDropdown">
			    <ul class="navbar-nav ml-auto">
			    	<li class="nav-item dropdown">
				        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-user icon"></i>
				          <?php echo $this->session->userdata('correo'); ?>
				        </a>
				        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
				          <a class="dropdown-item" id="closeSession" href="<?php echo base_url(); ?>index.php/Login/logout">Sign out</a>
				        </div>
				    </li>
			    </ul>
		  	</div>
		</nav>
	</header>
	<div class="loader" style="display: none;"></div>
	<section class="contenido">
		<div class="container">
			<div class="row">
				<div class="col-lg-6 offset-lg-3">
					<div class="div_titulo">
						<p class="titulo">Register Form</p><br><p>* All fields are required</p>
					</div>
				</div>
			</div>
			<div class="formulario">
				<form method="post" id="datos" enctype="multipart/form-data">
					<div class="row">
						<div class="col-12">
							<p class='text-center p-3 mb-2 bg-danger text-white'>Personal Data</p><hr>
					        <div class="row">
				        		<div class="col-12">
				        			<label>Name *</label>
				        			<input type="text" class="form-control" name="nombre" id="nombre" value="<?php echo $this->session->userdata('nombre'); ?>" disabled>
				        			<br>
				        		</div>
				        		<div class="col-12">
				        			<label>First lastname *</label>
				        			<input type="text" class="form-control" name="paterno" id="paterno" value="<?php echo $this->session->userdata('paterno'); ?>" disabled>
				        			<br>
				        		</div>
				        		<div class="col-12">
				        			<label>Second lastname</label>
				        			<input type="text" class="form-control" name="materno" id="materno" value="<?php echo $this->session->userdata('materno'); ?>" disabled>
				        			<br>
				        		</div>
					        </div>
						</div>
					</div>
					<div class="row">
		        		<div class="col-6">
		        			<label>Birthdate *</label>
		        			<input type="text" class="form-control obligado" name="fecha_nacimiento" id="fecha_nacimiento" placeholder="dd/mm/yyyy">
		        			<br>
		        		</div>
		        		<div class="col-6">
		        			<label>Job Position Requested: *</label>
		        			<input type="text" class="form-control obligado" name="puesto" id="puesto">
		        			<br>
		        		</div>
		        	</div>
		        	<div class="row">
		        		<div class="col-6">
		        			<label>Nationality: *</label>
		        			<input type="text" class="form-control obligado" name="nacionalidad" id="nacionalidad">
		        			<br>
		        		</div>
		        		<div class="col-6">
		        			<label>Gender: *</label>
		        			<select name="genero" id="genero" class="form-control obligado">
					            <option value="">Select</option>
					            <option value="Male">Male</option>
					            <option value="Female">Female</option>
				          	</select>
				          	<br>
		        		</div>		
			        </div>
			        <div class="row">
			        	<div class="col-12">
		        			<label>Address *</label>
		        			<input type="text" class="form-control obligado" name="domicilio" id="domicilio" placeholder="Street, number, neighborhood, zip code and city">
		        			<br>
		        		</div>
		        	</div>
		        	<div class="row">
		        		<div class="col-6">
		        			<label>Country *</label>
		        			<select name="pais" id="pais" class="form-control obligado">
					            <option value="">Select</option>
					            <?php foreach ($paises as $pais) {?>
					                <option value="<?php echo $pais->nombre; ?>"><?php echo $pais->nombre; ?></option>
					            <?php } ?>
				          	</select>
				          	<br>
		        		</div>
		        		<div class="col-6">
		        			<label>Marital Status *</label>
		        			<select name="civil" id="civil" class="form-control obligado">
					            <option value="-1">Select</option>
					            <option value="1">Married</option>
					            <option value="2">Single</option>
					            <option value="3">Divorced</option>
					            <option value="4">Free Union</option>
					            <option value="5">Widowed</option>
					            <option value="6">Separated</option>
				          	</select>
				          	<br>
		        		</div>
			        </div>
			        <div class="row">
		        		<div class="col-6">
		        			<label>Mobile Number *</label>
		        			<input type="text" class="form-control obligado" name="telefono" id="telefono" maxlength="16" value="<?php echo $this->session->userdata('celular'); ?>">
		        			<br>
		        		</div>
		        		<div class="col-6">
		        			<label>Home Number </label>
		        			<input type="text" class="form-control" name="tel_casa" id="tel_casa" maxlength="18">
		        			<br>
		        		</div>
		        	</div>
		        	<div class="row">
		        		<div class="col-6">
		        			<label>Number to leave Messages </label>
		        			<input type="text" class="form-control" name="tel_otro" id="tel_otro" maxlength="18">
		        			<br>
		        		</div>
			        </div>
				</form>
				<form id="datos3">
					<div class="row">
						<div class="col-lg-12">
							<p class='text-center p-3 mb-2 bg-danger text-white'>Address History </p>
							<?php 
							if($this->session->userdata('proyecto') == 150 || $this->session->userdata('proyecto') == 151 || $this->session->userdata('proyecto') == 152 || $this->session->userdata('proyecto') == 154){ ?>
								<p>TYPE IN YOUR ADDRESSES OF THE LAST 10 YEARS, STARTING WITH THE CURRENT ONE.</p>
							<?php
							}
							if($this->session->userdata('proyecto') == 153){ ?>
								<p>TYPE IN YOUR ADDRESSES OF THE LAST 7 YEARS, STARTING WITH THE CURRENT ONE.</p>
							<?php
							}
							?>
							
							<hr>
						</div>
					</div>
					<p class="tipo_title">Current address</p>
					<div class="row">
						<div class="col-md-6">
							<label>Period *</label>
		        			<input type="text" class="form-control es_dom obligado" name="h1_periodo" id="h1_periodo">
		        			<br>
						</div>
						<div class="col-md-6">
							<label>Cause of departure *</label>
		        			<input type="text" class="form-control es_dom obligado" name="h1_causa" id="h1_causa" value="N/A" readonly>
		        			<br>
						</div>
					</div>
					<div class="row">
			        	<div class="col-md-12">
		        			<label>Address *</label>
		        			<input type="text" class="form-control  es_dom obligado" name="h1_domicilio" id="h1_domicilio" placeholder="Street, number, neighborhood, zip code and city (If possible)" readonly>
		        			<br>
		        		</div>
		        	</div>
		        	<div class="row">
		        		<div class="col-md-6">
		        			<label>Country *</label>
		        			<input type="text" class="form-control es_dom obligado" name="h1_pais" id="h1_pais" readonly>
				          	<br>
		        		</div>
			        </div>
			        <div id="div_domicilios"></div>
					<div class="row">
						<div class="col-md-4 offset-md-5">
							<a href="javascript:void(0)" class="btn btn-secondary" onclick="generarDomicilio()">Generate other address</a><br><br>
						</div>
					</div>
					<br>
					<div class="row">
						<div class="col-12">
							<p class='text-center p-3 mb-2 bg-danger text-white'>Studies Record </p>
							<p>TYPE YOUR HIGHEST STUDIES</p>
							<hr>
						</div>
					</div>
					<div class="row">
						<div class="col-12">
							<label>Highest studies *</label>
		        			<select name="estudios" id="estudios" class="form-control obligado">
					            <option value="-1">Select</option>
					            <?php foreach ($studies as $st) {?>
					                <option value="<?php echo $st->id; ?>"><?php echo $st->nombre; ?></option>
					            <?php } ?>
				          	</select>
				          	<br>
						</div>
					</div>
					<div class="row">
						<div class="col-6">
							<label for="estudios_periodo">Period *</label>
		        			<input type="text" class="form-control obligado" name="estudios_periodo" id="estudios_periodo">
		        			<br>
						</div>
						<div class="col-6">
							<label for="estudios_escuela">Institute *</label>
		        			<input type="text" class="form-control obligado" name="estudios_escuela" id="estudios_escuela">
		        			<br>
						</div>
					</div>
					<div class="row">
						<div class="col-6">
							<label for="estudios_ciudad">City *</label>
		        			<input type="text" class="form-control obligado" name="estudios_ciudad" id="estudios_ciudad">
		        			<br>
						</div>
						<div class="col-6">
							<label for="estudios_certificado">Certificate Obtained *</label>
		        			<input type="text" class="form-control obligado" name="estudios_certificado" id="estudios_certificado">
		        			<br>
						</div>
					</div>
					<div class="row">
						<div class="col-12">
							<p class='text-center p-3 mb-2 bg-danger text-white'>Professional references </p>
						</div>
					</div>
					<p class="tipo_title">First reference</p>
					<div class="row">
						<div class="col-sm-12 col-md-6">
							<label>Name *</label>
		        			<input type="text" class="form-control obligado" name="refpro1_nombre" id="refpro1_nombre">
		        			<br>
						</div>
						<div class="col-sm-12 col-md-6">
		        			<label>Phone *</label>
		        			<input type="text" class="form-control obligado" name="refpro1_telefono" id="refpro1_telefono" maxlength="16">
		        			<br>
		        		</div>
		        	</div>
		        	<div class="row">
		        		<div class="col-sm-12 col-md-6">
			        		<label>Time to know her/him *</label>
		        			<input type="text" class="form-control obligado" name="refpro1_tiempo" id="refpro1_tiempo">
		        			<br>
			        	</div>
			        	<div class="col-sm-12 col-md-6">
			        		<label>How did you meet her/him? *</label>
		        			<input type="text" class="form-control obligado" name="refpro1_conocido" id="refpro1_conocido">
		        			<br>
			        	</div>
					</div>
					<div class="row">
						<div class="col-sm-12 col-md-6">
			        		<label>What position had her/him? *</label>
		        			<input type="text" class="form-control obligado" name="refpro1_puesto" id="refpro1_puesto">
		        			<br>
			        	</div>
					</div>
					<br>
					<p class="tipo_title">Second reference</p>
					<div class="row">
						<div class="col-sm-12 col-md-6">
							<label>Name *</label>
		        			<input type="text" class="form-control obligado" name="refpro2_nombre" id="refpro2_nombre">
		        			<br>
						</div>
						<div class="col-sm-12 col-md-6">
		        			<label>Phone *</label>
		        			<input type="text" class="form-control obligado" name="refpro2_telefono" id="refpro2_telefono" maxlength="16">
		        			<br>
		        		</div>
		        	</div>
		        	<div class="row">
		        		<div class="col-sm-12 col-md-6">
			        		<label>Time to know her/him *</label>
		        			<input type="text" class="form-control obligado" name="refpro2_tiempo" id="refpro2_tiempo">
		        			<br>
			        	</div>
			        	<div class="col-sm-12 col-md-6">
			        		<label>How did you meet her/him? *</label>
		        			<input type="text" class="form-control obligado" name="refpro2_conocido" id="refpro2_conocido">
		        			<br>
			        	</div>
					</div>
					<div class="row">
						<div class="col-sm-12 col-md-6">
			        		<label>What position had her/him? *</label>
		        			<input type="text" class="form-control obligado" name="refpro2_puesto" id="refpro2_puesto">
		        			<br>
			        	</div>
					</div>
					<br>
					<div class="row">
						<div class="col-12">
							<p class='text-center p-3 mb-2 bg-danger text-white'>Labor References </p>
							<?php 
							if($this->session->userdata('proyecto') == 150 || $this->session->userdata('proyecto') == 151 || $this->session->userdata('proyecto') == 153){ ?>
								<p>TYPE IN YOUR EMPLOYMENTS OF THE LAST 7 YEARS, STARTING WITH THE LAST ONE.</p>
							<?php
							}
							if($this->session->userdata('proyecto') == 152 || $this->session->userdata('proyecto') == 154){ ?>
								<p>TYPE IN YOUR EMPLOYMENTS OF THE LAST 5 YEARS, STARTING WITH THE LAST ONE.</p>
							<?php
							}
							?>
							<hr>
						</div>
					</div>
					<p class="tipo_title">Reference #1</p>
					<div class="row">
						<div class="col-6">
							<label>Company </label>
		        			<input type="text" class="form-control reflab obligado" name="reflab1_empresa" id="reflab1_empresa" >
		        			<br>
						</div>
						<div class="col-6">
							<label>Address </label>
		        			<input type="text" class="form-control reflab obligado" name="reflab1_direccion" id="reflab1_direccion" >
		        			<br>
						</div>
					</div>
					<div class="row">
						<div class="col-6">
		        			<label>Entry Date </label>
		        			<input type="text" class="form-control reflab obligado fecha_reflab" name="reflab1_entrada" id="reflab1_entrada" placeholder="dd/mm/yyyy">
		        			<br>
		        		</div>
		        		<div class="col-6">
		        			<label>Exit Date </label>
		        			<input type="text" class="form-control reflab obligado fecha_reflab" name="reflab1_salida" id="reflab1_salida" placeholder="dd/mm/yyyy">
		        			<br>
		        		</div>
		        	</div>
		        	<div class="row">
		        		<div class="col-6">
		        			<label>Phone </label>
		        			<input type="text" class="form-control reflab" name="reflab1_telefono" id="reflab1_telefono" maxlength="18">
		        			<br>
		        		</div>
		        		<div class="col-6">
							<label>Initial Job Position </label>
		        			<input type="text" class="form-control reflab obligado" name="reflab1_puesto1" id="reflab1_puesto1" >
		        			<br>
						</div>
					</div>
					<div class="row">
						<div class="col-6">
							<label>Last Job Position </label>
		        			<input type="text" class="form-control reflab obligado" name="reflab1_puesto2" id="reflab1_puesto2" >
		        			<br>
						</div>
						<div class="col-6">
			        		<label>Initial Salary </label>
		        			<input type="text" class="form-control solo_numeros reflab obligado" name="reflab1_salario1" id="reflab1_salario1" maxlength="8">
		        			<br>
			        	</div>
			        </div>
			        <div class="row">
			        	<div class="col-6">
			        		<label>Last Salary </label>
		        			<input type="text" class="form-control solo_numeros reflab obligado" name="reflab1_salario2" id="reflab1_salario2" maxlength="8">
		        			<br>
			        	</div>
			        	<div class="col-6">
							<label>Immediate Boss Name </label>
		        			<input type="text" class="form-control reflab obligado" name="reflab1_bossnombre" id="reflab1_bossnombre" >
		        			<br>
						</div>
					</div>
					<div class="row">
						<div class="col-6">
							<label>Immediate Boss Email </label>
		        			<input type="text" class="form-control reflab" name="reflab1_bosscorreo" id="reflab1_bosscorreo" >
		        			<br>
						</div>
						<div class="col-6">
							<label>Boss's Job Position </label>
		        			<input type="text" class="form-control reflab obligado" name="reflab1_bosspuesto" id="reflab1_bosspuesto" >
		        			<br>
						</div>
					</div>
					<div class="row">
						<div class="col-6">
							<label>Cause of Separation </label>
		        			<input type="text" class="form-control reflab obligado" name="reflab1_separacion" id="reflab1_separacion" >
		        			<br>
						</div>
					</div>
					<div id="div_referencias"></div>
					<div class="row">
						<div class="col-md-4 offset-md-5">
							<a href="javascript:void(0)" class="btn btn-secondary" onclick="generarReferencia()">Generate other labor reference</a><br><br>
						</div>
					</div>
					<br>
					<div class="row">
						<div class="col-lg-12">
							<p class='text-center p-3 mb-2 bg-danger text-white'>Break(s) in Employment </p>
							<p>IF YOU HAD A BREAK DURING THE LAST 7 YEARS EMPLOYMENTS, PLEASE EXPLAIN THE REASON, INDICATE PERIOD AND WHICH ACTIVITIES DID YOU DO MEANWHILE. IF NOT TYPE NA</p>
							<hr>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<label for="trabajo_inactivo">Period, Reason and Activities *</label>
		        			<textarea class="form-control obligado" name="trabajo_inactivo" id="trabajo_inactivo" rows="3" placeholder="Break(s) in Employment"></textarea>
		        			<br>
						</div>
					</div>
					<br>
					<div class="row">
						<div class="col-lg-12">
							<p class='text-center p-3 mb-2 bg-danger text-white'>Your Comments</p>
							<p>IF YOU HAVE ANY COMMENT ABOUT YOUR DOCUMENTS OR IF YOU WANT TO ADD ANY INFORMATION WHICH COULD HELP US DURING THE PROCESS, PLEASE LET US KNOW</p>
							<hr>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-12">
							<textarea name="obs" id="obs" class="form-control obligado" rows="5" placeholder="Your comments"></textarea>
		        			<br><br><br>
						</div>
					</div>
					<div class="row">
						<div class="col-md-8">
							<label>Please read this <a href="javascript:void(0)" data-toggle="modal" data-target="#nondisclosureModal">Non-disclosure agreement</a></label><br><br>
                            <label class="contenedor_check">I declare that I have read and fully agree with everything in this Non-disclosure agreement
                            <input type="checkbox" name="acepto" id="acepto" value="1">
                            <span class="checkmark"></span>
                            </label>
                            <br><br>
                        </div>
					</div>
					<div class="row">
						<div class="col-md-4 offset-md-5">
							<!--a id="sendForm" href="javascript:void(0)">Send form and finish</a-->
							<button id="sendForm">Send form and finish</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</section>
	<script src="<?php echo base_url() ?>js/candidato/jquery-3.3.1.min.js"></script>
	<script src="<?php echo base_url() ?>js/candidato/bootstrap.min.js"></script>
	<script src="<?php echo base_url() ?>js/candidato/bootstrap-datetimepicker.min.js"></script>
	<script src="<?php echo base_url() ?>js/candidato/custom_international.js"></script>
	<script src="<?php echo base_url() ?>js/candidato/icheck.js"></script>
	<script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
	<!-- InputMask -->
	<script src="<?php echo base_url(); ?>assets/plugins/input-mask/jquery.inputmask.js"></script>
	<script src="<?php echo base_url(); ?>assets/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
	<script src="<?php echo base_url(); ?>assets/plugins/input-mask/jquery.inputmask.extensions.js"></script>
	<script>
    $(document).ready(function(){
  		$('#fecha_nacimiento').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' });
  		$(".fecha_reflab").inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' });
  		var fnacimiento = '<?php echo $this->session->userdata('fecha') ?>';
    	if(fnacimiento != "" && fnacimiento != null && fnacimiento != "0000-00-00" && fnacimiento != "00/00/0000"){
    		$("#fecha_nacimiento").val(fnacimiento).trigger('change');
    	}
    	$("#domicilio").change(function(){
	    	var valor = $(this).val();
	    	$("#h1_domicilio").val(valor);
	    });
	    $("#pais").change(function(){
	      	var pais = $(this).val();
	      	if(pais != ''){
	        	$('#h1_pais').val(pais)
	      	}
	      	else{
        		$('#h1_pais').val('')
	      	}
	    });
	  	$("#estado").change(function(){
	      	var id_estado = $(this).val();
	      	if(id_estado != -1){
	        	$.ajax({
	          		url: '<?php echo base_url('index.php/Candidato/getMunicipios'); ?>',
	          		method: 'POST',
	          		data: {'id_estado':id_estado},
	          		dataType: "text",
	          		success: function(res)
	          		{
	            		$('#municipio').prop('disabled', false);
	            		$('#municipio').html(res);
	          		}
	        	});
	      	}
	      	else{
        		$('#municipio').prop('disabled', true);
	        	$('#municipio').append($("<option selected></option>").attr("value",'').text("Selecciona"));
	      	}
	    });
		//Crear registro
		$('#datos3').on('submit', function(e){  
           	e.preventDefault();
           	var totalVacios = $('.obligado').filter(function(){
	      		return !$(this).val();
	    	}).length;

		    if(totalVacios > 0){
		      	$(".obligado").each(function() {
			        var element = $(this);
			        if (element.val() == "") {
			          	element.addClass("requerido");
			          	$('#empty').css("display", "block");
			          	setTimeout(function(){
			            	$('#empty').fadeOut();
			          	},5000);
			        }
			        else{
			          	element.removeClass("requerido");
			        }
		      	});
		    }
		    else{
		    	if($('#acepto').is(':checked')){
		           	var data = $("#datos").serialize();
		           	var data3 = $(this).serialize();
		           
					var trabajos = "";
			        var total_trabajos = $(".reflab").length;
			      	if(total_trabajos > 0){
			        	for(var i = 1; i <= total_trabajos / 13; i++){
							trabajos += $("#reflab"+i+"_empresa").val()+"__";
							trabajos += $("#reflab"+i+"_direccion").val()+"__";
							trabajos += $("#reflab"+i+"_entrada").val()+"__";
							trabajos += $("#reflab"+i+"_salida").val()+"__";
							trabajos += $("#reflab"+i+"_telefono").val()+"__";
							trabajos += $("#reflab"+i+"_puesto1").val()+"__";
							trabajos += $("#reflab"+i+"_puesto2").val()+"__";
							trabajos += $("#reflab"+i+"_salario1").val()+"__";
							trabajos += $("#reflab"+i+"_salario2").val()+"__";
							trabajos += $("#reflab"+i+"_bossnombre").val()+"__";
							trabajos += $("#reflab"+i+"_bosscorreo").val()+"__";
							trabajos += $("#reflab"+i+"_bosspuesto").val()+"__";
							trabajos += $("#reflab"+i+"_separacion").val()+"@@";
						}
			        }
			        var doms = "";
			        var total_doms = $(".es_dom").length;
			      	if(total_doms > 0){
			        	for(var i = 1; i <= total_doms / 4; i++){
							doms += $("#h"+i+"_periodo").val()+"__";
							doms += $("#h"+i+"_causa").val()+"__";
							doms += $("#h"+i+"_domicilio").val()+"__";
							doms += $("#h"+i+"_pais").val()+"@@";
						}
			        }	                      
		            $.ajax({  
		                url:"<?php echo base_url('Candidato/formHCLInternational'); ?>",   
		                method: "POST",  
		                data: {'datos':data,'complementos':data3,'trabajos':trabajos,'doms':doms},
		                dataType: "text",
		                success:function(res){
		                	$("#mensajeModal").modal("show");
	                		setTimeout(function(){
		                  		$("#mensajeModal").modal("hide");
		                    	window.location.href = "<?php echo base_url(); ?>index.php/Login/logout";
		                	},15000);  
		                }  
		            }); 
		        }
		        else{
		          	$("#no_check").css("display","block");
		          	setTimeout(function(){
		            	$("#no_check").fadeOut();
		          	}, 4000);
		        }   
		    }
		});
        	
    });
	var num_ref = 1;
	function generarReferencia(){
		num_ref++;
		var item = "";
		item += '<p class="tipo_title">Reference #'+num_ref+'</p><div class="row"><div class="col-6"><label>Company </label><input type="text" class="form-control reflab obligado" name="reflab'+num_ref+'_empresa" id="reflab'+num_ref+'_empresa" ><br></div><div class="col-6"><label>Address </label><input type="text" class="form-control reflab obligado" name="reflab'+num_ref+'_direccion" id="reflab'+num_ref+'_direccion" ><br></div></div><div class="row"><div class="col-6"><label>Entry Date </label><input type="text" class="form-control reflab obligado fecha_reflab" name="reflab'+num_ref+'_entrada" id="reflab'+num_ref+'_entrada" placeholder="dd/mm/yyyy"><br></div><div class="col-6"><label>Exit Date </label><input type="text" class="form-control reflab obligado fecha_reflab" name="reflab'+num_ref+'_salida" id="reflab'+num_ref+'_salida" placeholder="dd/mm/yyyy"><br></div></div><div class="row"><div class="col-6"><label>Phone </label><input type="text" class="form-control reflab" name="reflab'+num_ref+'_telefono" id="reflab'+num_ref+'_telefono" maxlength="18"><br></div><div class="col-6"><label>Initial Job Position </label><input type="text" class="form-control reflab obligado" name="reflab'+num_ref+'_puesto1" id="reflab'+num_ref+'_puesto1" ><br></div></div><div class="row"><div class="col-6"><label>Last Job Position </label><input type="text" class="form-control reflab obligado" name="reflab'+num_ref+'_puesto2" id="reflab'+num_ref+'_puesto2" ><br></div><div class="col-6"><label>Initial Salary </label><input type="text" class="form-control solo_numeros reflab obligado" name="reflab'+num_ref+'_salario1" id="reflab'+num_ref+'_salario1" maxlength="8"><br></div></div><div class="row"><div class="col-6"><label>Last Salary </label><input type="text" class="form-control solo_numeros reflab obligado" name="reflab'+num_ref+'_salario2" id="reflab'+num_ref+'_salario2" maxlength="8"><br></div><div class="col-6"><label>Immediate Boss Name </label><input type="text" class="form-control reflab obligado" name="reflab'+num_ref+'_bossnombre" id="reflab'+num_ref+'_bossnombre" ><br></div></div><div class="row"><div class="col-6"><label>Immediate Boss Email </label><input type="text" class="form-control reflab" name="reflab'+num_ref+'_bosscorreo" id="reflab'+num_ref+'_bosscorreo" ><br></div><div class="col-6"><label>Boss\'s Job Position </label><input type="text" class="form-control reflab obligado" name="reflab'+num_ref+'_bosspuesto" id="reflab'+num_ref+'_bosspuesto" ><br></div></div><div class="row"><div class="col-6"><label>Cause of Separation </label><input type="text" class="form-control reflab obligado" name="reflab'+num_ref+'_separacion" id="reflab'+num_ref+'_separacion" ><br></div></div><br>';
		item += '<script>$(".fecha_reflab").inputmask("dd/mm/yyyy", { "placeholder": "dd/mm/yyyy" })<\/script>';
		$("#div_referencias").append(item);
	}
	var dom = 1;
	function generarDomicilio(){
		dom++;
		var item = "";
		item += '<p class="tipo_title">Previous address #'+dom+'</p><div class="row"><div class="col-md-6"><label>Period *</label><input type="text" class="form-control es_dom obligado" name="h'+dom+'_periodo" id="h'+dom+'_periodo"><br></div><div class="col-md-6"><label>Cause of departure *</label><input type="text" class="form-control es_dom obligado" name="h'+dom+'_causa" id="h'+dom+'_causa"><br></div></div><div class="row"><div class="col-md-12"><label>Address *</label><input type="text" class="form-control  es_dom obligado" name="h'+dom+'_domicilio" id="h'+dom+'_domicilio" placeholder="Street, number, neighborhood, zip code and city (If possible)"><br></div></div><div class="row"><div class="col-md-6"><label>Country *</label><select name="h'+dom+'_pais" id="h'+dom+'_pais" class="form-control es_dom obligado"><option value="">Select</option><?php foreach ($paises as $p) {?><option value="<?php echo $p->nombre; ?>"><?php echo $p->nombre; ?></option><?php } ?></select><br></div></div><br>';
		$("#div_domicilios").append(item);
	}
	</script>
	

</body>
</html>