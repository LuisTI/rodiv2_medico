<!DOCTYPE html>
<html class="html-subcliente">
    
  <meta charset="UTF-8">
	<title><?php echo strtoupper($this->session->userdata('subcliente')); ?> | RODI</title>
	<link rel="stylesheet" href="<?php echo base_url() ?>css/bootstrap.min.css">
	<link rel="stylesheet" href="<?php echo base_url() ?>css/subcliente.css">
	<!-- Google Font -->
	<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
	<script src="https://kit.fontawesome.com/fdf6fee49b.js"></script>
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
	<!-- Select Bootstrap -->
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/css/bootstrap-select.min.css">
	<!-- Sweetalert 2 -->
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/sweetalert2@10.12.7/dist/sweetalert2.min.css">
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/css/bootstrap-select.min.css">
	<link rel="icon" type="image/jpg" href="<?php echo base_url() ?>img/favicon.jpg" sizes="64x64">

  </head>
  <body >
    <!--Modal--> 
    <div class="modal fade" id="newModal" role="dialog" data-backdrop="static" data-keyboard="false">
			<div class="modal-dialog modal-lg">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title">Registrar candidato</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body">
	        	<div class="row">
	        		<div class="col-sm-12 col-md-4 col-lg-4">
	        			<label>Nombre(s) *</label>
	        			<input type="text" class="form-control obligado" name="nombre" id="nombre" onkeyup="document.getElementById(this.id).value=document.getElementById(this.id).value.toUpperCase()">
	        			<br>
	        		</div>
	        		<div class="col-sm-12 col-md-4 col-lg-4">
	        			<label>Apellido paterno *</label>
	        			<input type="text" class="form-control obligado" name="paterno" id="paterno" onkeyup="document.getElementById(this.id).value=document.getElementById(this.id).value.toUpperCase()">
	        			<br>
	        		</div>
	        		<div class="col-sm-12 col-md-4 col-lg-4">
	        			<label>Apellido materno</label>
	        			<input type="text" class="form-control obligado" name="materno" id="materno" onkeyup="document.getElementById(this.id).value=document.getElementById(this.id).value.toUpperCase()">
	        			<br>
	        		</div>			    
	        	</div>
	        	<div class="row">
							<div class="col-sm-12 col-md-4 col-lg-4">
								<label>Puesto *</label>
								<select name="puesto" id="puesto" class="form-control obligado">
									<option value="">Selecciona</option>
									<?php
                	foreach ($puestos as $p) { ?>
                  	<option value="<?php echo $p->id; ?>"><?php echo $p->nombre; ?></option>
                	<?php
                	} ?>
								</select>
								<br>
							</div>
	        		<div class="col-sm-12 col-md-4 col-lg-4">
	        			<label>Correo electrónico</label>
	        			<input type="email" class="form-control" name="correo" id="correo" onkeyup="document.getElementById(this.id).value=document.getElementById(this.id).value.toLowerCase()">
	        			<br>
	        		</div>
	        		<div class="col-md-4">
	        			<label>Tel. Celular *</label>
	        			<input type="text" class="form-control obligado" name="celular" id="celular" maxlength="16">
	        			<br>
	        		</div>
						</div>
						<h4 class="text-center">Selecciona los estudios que requiere el candidato:</h4><br><br>
	        	<div class="row">
	        		<div class="col-sm-12 col-md-4 col-lg-4">
	        			<label>Socioeconómico *</label>
								<select name="socio" id="socio" class="form-control">
									<option value="">Selecciona</option>
									<option value="1">Si</option>
									<option value="0">No</option>
								</select>
								<br>
							</div>
							<div class="col-sm-12 col-md-4 col-lg-4">
	        			<label>Antidoping *</label>
								<select name="antidoping" id="antidoping" class="form-control">
									<option value="">Selecciona</option>
									<option value="1">Si</option>
									<option value="0">No</option>
								</select>
								<br>
							</div>
							<div class="col-sm-12 col-md-4 col-lg-4">
	        			<label>Psicométrico *</label>
								<select name="psicometrico" id="psicometrico" class="form-control">
									<option value="">Selecciona</option>
									<option value="1">Si</option>
									<option value="0">No</option>
								</select>
								<br>
							</div>
						</div>
						<div class="row">
	        		<div class="col-sm-12 col-md-4 col-lg-4">
	        			<label>Médico *</label>
								<select name="medico" id="medico" class="form-control">
									<option value="">Selecciona</option>
									<option value="1">Si</option>
									<option value="0">No</option>
								</select>
								<br>
							</div>
						</div>
						<div class="row">
							<div class="col-12">
								<label>Examen antidoping</label>
								<select name="examen" id="examen" class="form-control" disabled>
									<option value="" selected>Selecciona</option>
									<?php
									foreach ($drogas as $d) { ?>
										<option value="<?php echo $d->id; ?>"><?php echo $d->nombre . " (" . $d->conjunto . ")"; ?></option>
									<?php
									} ?>
								</select>
								<br>
							</div>
						</div>
	        	<div class="row">
							<div class="col-12">
								<label>¿Requiere algo más para el candidato?</label>
								<textarea class="form-control" name="otro_requisito" id="otro_requisito" rows="3"></textarea>
								<br>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-12 col-md-6 col-lg-6">
								<label>Cargar CV o solicitud de empleo del candidato</label>
								<input type="file" id="cv" name="cv" class="form-control" accept=".pdf, .jpg, .jpeg, .png" multiple><br>
							</div>
						</div>
						<div id="msj_error" class="alert alert-danger hidden"></div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        		<button type="button" class="btn btn-success" onclick="registrar()">Registrar</button>
					</div>
				</div>
			</div>
    </div>
		<div class="modal fade" id="avancesModal" role="dialog" data-backdrop="static" data-keyboard="false">
	  	<div class="modal-dialog modal-lg modal-dialog-centered" role="document">
		    <div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title">Mensajes de avances del candidato: <br><span class="nombreCandidato"></span> </h4>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body">
						<div id="div_avances"></div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
					</div>
		    </div>
	 		</div>
		</div>
		<div class="modal fade" id="statusModal" role="dialog" data-backdrop="static" data-keyboard="false">
	  	<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
		    <div class="modal-content">
		      	<div class="modal-header">
		        	<h4 class="modal-title">Estatus del proceso del candidato: <br><span class="nombreCandidato"></span></h4>
			        <button type="button" class="close" data-dismiss="modal" aria-label="Cerrar">
			          <span aria-hidden="true">&times;</span>
			        </button>
		      	</div>
		      	<div class="modal-body">
		      		<div id="div_status"></div>
		      	</div>
		      	<div class="modal-footer">
			        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
		      	</div>
		    </div>
	 		</div>
		</div>
    <!--NavBar-->
    <header>
    	<nav class="navbar navbar-expand-lg navbar-light bg-light" id="menu">
			  <a class="navbar-brand text-light" href="#">
			   <img src="<?php echo base_url() ?>img/favicon.jpg" class="space">
			    <?php echo $this->session->userdata('nombre')." ".$this->session->userdata('paterno'); ?>
			  </a>
			  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				</button>
			  <div class="collapse navbar-collapse" id="navbarSupportedContent">
			    <ul class="navbar-nav ml-auto">
			      <li class="nav-item active">
			        <a class="nav-link text-light font-weight-bold" href="javascript:void(0)"  data-toggle="modal" data-target="#newModal"><i class="fas fa-plus-circle"></i> Registrar candidato</a>
			      </li>
			      <li class="nav-item">
			        <a class="nav-link text-light font-weight-bold" href="<?php echo base_url(); ?>Login/logout"><i class="fas fa-sign-out-alt"></i> Cerrar sesión</a>
			      </li>
			    </ul>
			  </div>
			</nav>
    </header>

		<!-- Valores Generales -->
		<input type="hidden" name="idSubcliente" id="idSubcliente" value="<?php echo $this->session->userdata('idsubcliente'); ?>">
		<input type="hidden" name="idCliente" id="idCliente" value="<?php echo $this->session->userdata('idcliente'); ?>">
    <!--Cuerpo-->
		<div class="loader" style="display: none;"></div>
		<div class="contenedor mt-5 my-5">
      <p class="text-center">Versión 2.0</p>
			<select name="buscador" id="buscador" class="form-control selectpicker" data-live-search="true">
				<option value="0">VER TODOS</option>
				<?php
				if ($candidatos) {
					foreach ($candidatos as $can) { ?>
						<option value="<?php echo $can->id; ?>"><?php echo $can->candidato; ?></option>
				<?php 
					}
				} ?>
			</select>
		</div>
    <div class="contenedor">
      <div class="row">
        <?php 
        if($candidatos != "" && $candidatos != null){
	        foreach($candidatos as $c){
	          //$proyecto = ($c->proyecto == "" || $c->proyecto == null)? "":$c->proyecto;
	          $cliente = $c->cliente." / ".$c->subcliente;
	          $e = new DateTime($c->fecha_alta);
	          $fecha = $e->format('d/m/Y H:h');
						//Color tarjeta
						$color_tarjeta = ($c->socioeconomico == 1)? 'color_socio':'color_antidoping';
						//Color icono avances
						$color_avances = ($c->idAvance != null)? '':'gris';
						//Estatus proceso
						$idEstudios = ($c->idEstudios != null)? $c->idEstudios:0;
						$idSociales = ($c->idSociales != null)? $c->idSociales:0;
						$idPersonales = ($c->idPersonales != null)? $c->idPersonales:0;
						$idLaborales = ($c->idLaborales != null)? $c->idLaborales:0;
						$idLegales = ($c->idLegales != null)? $c->idLegales:0;
						$socio = $c->socioeconomico;
						//Psicometria
						if($c->psicometrico == 1){
							if($c->archivo != null){
								$color_psicometria = 'azul';
								$href_psicometria = base_url()."_psicometria/".$c->archivo;
								$alerta_psicometria = 'alerta_descarga';
							}
							else{
								$color_psicometria = 'amarillo';
								$href_psicometria = "javascript:void(0)";
								$alerta_psicometria = '';
							}
						}
						else{
							$color_psicometria = 'gris';
							$href_psicometria = "javascript:void(0)";
							$alerta_psicometria = '';
						}
						//Antidoping
						if($c->tipo_antidoping == 1){
							if($c->doping_hecho == 1){
								if($c->fecha_resultado != null){
									if($c->resultado_doping == 1){
										$res_doping = '<div><form id="pdfForm' . $c->idDoping . '" action="'.base_url('Doping/createPDF').'" method="POST"><a href="javascript:void(0);" data-toggle="tooltip" title="Descargar resultado" id="pdfDoping" data-id="' . $c->idDoping . '" class="fa-tooltip icono rojo alerta_descarga"><i class="fas fa-eye-dropper"></i></a><input type="hidden" name="idDop" id="idDop' . $c->idDoping . '" value="' . $c->idDoping . '"></form></div>';
									}
									else{
										$res_doping = '<div><form id="pdfForm' . $c->idDoping . '" action="'.base_url('Doping/createPDF').'" method="POST"><a href="javascript:void(0);" data-toggle="tooltip" title="Descargar resultado" id="pdfDoping" data-id="' . $c->idDoping . '" class="fa-tooltip icono verde alerta_descarga"><i class="fas fa-eye-dropper"></i></a><input type="hidden" name="idDop" id="idDop' . $c->idDoping . '" value="' . $c->idDoping . '"></form></div>';
									}
								}
								else{
									$res_doping = '<a href="javascript:void(0);" data-toggle="tooltip" class="fa-tooltip icono naranja"><i class="fas fa-eye-dropper"></i></a>';
								}
							}
							else{
								$res_doping = '<a href="javascript:void(0);" data-toggle="tooltip" class="fa-tooltip icono amarillo"><i class="fas fa-eye-dropper"></i></a>';
							}
						}
						else{
							$res_doping = '<a href="javascript:void(0);" data-toggle="tooltip" class="fa-tooltip icono gris"><i class="fas fa-eye-dropper"></i></a>';
						}
						//Reporte final
						if($c->status == 2){
							switch($c->status_bgc){
								case 1:
									$color_final = 'verde';
									break;
								case 2:
									$color_final = 'rojo';
									break;
								case 3:
									$color_final = 'amarillo';
									break;
							}
							$res_final = '<div style="display: inline-block;"><form id="pdf'.$c->id.'" action="'.base_url('Cliente_General/crearPDF').'" method="POST"><a href="javascript:void(0);" data-toggle="tooltip" title="Descargar documento final" id="pdfFinal" data-id="' . $c->id . '" class="fa-tooltip icono '.$color_final.' alerta_descarga"><i class="fas fa-file-pdf"></i></a><input type="hidden" name="idPDF" id="idPDF'.$c->id.'" value="'.$c->id.'"></form></div>';
						}
						else{
							$res_final = '<a href="javascript:void(0);" data-toggle="tooltip" class="fa-tooltip icono gris"><i class="fas fa-file-pdf"></i></a>';
						}
	        ?>
	        <div class="col-sm-12 col-md-12 col-lg-6 ">
	          <div class="card carta mt-3 <?php echo $color_tarjeta ?> " id='<?php echo $c->id; ?>'>
	            <div class="card-body">
	              <div class="row">
	                <div class="col-sm-12 col-md-4 col-lg-4 mr text-center">
	                  <img src="<?php echo base_url() ?>/img/user.png" class="profile" width="140" height="140">
	                </div>
	                <div class="col-sm-12 col-md-8 col-lg-8 mr-text">
	                  <div class="text-center">
	                    <h5 class="card-title my-1"><b><?php echo $c->candidato?></b></h5>
	                    <p><?php echo $cliente?></p>
	                    <p><b><?php echo "Alta: ".$fecha ?></b></p>
	                  </div>
	                  <div class="row mt-1">
											<div class="col-2 mr-7 text-center">
												<?php 
												if($socio == 1){ ?>
													<a href="javascript:void(0)" data-toggle="tooltip" title="Mensajes de avances" class="fa-tooltip icono <?php echo $color_avances; ?> " onclick="verAvances('<?php echo $c->candidato ?>',<?php echo $c->id ?>)"><i class="fas fa-comment-dots"></i></a>
												<?php 
												} ?>
											</div>
											<div class="col-2 mr-7 text-center">
												<?php 
												if($socio == 1){ ?>
													<a href="javascript:void(0)" data-toggle="tooltip" title="Estatus proceso" class="fa-tooltip icono" onclick="verEstatus('<?php echo $c->candidato ?>',<?php echo $c->visitador ?>,<?php echo $idEstudios ?>,<?php echo $idSociales ?>,<?php echo $idPersonales ?>,<?php echo $idLaborales ?>,<?php echo $idLegales ?>)"><i class="fas fa-eye"></i></a>
												<?php 
												} ?>
											</div>
											<div class="col-2 mr-7 text-center">
												<?php 
												if($socio == 1){ ?>
													<a href="<?php echo $href_psicometria; ?>" data-toggle="tooltip" download="" title="Psicometria" class="fa-tooltip icono <?php echo $alerta_psicometria; ?> <?php echo $color_psicometria; ?> "><i class="fas fa-brain"></i></a>
												<?php 
												} ?>
											</div>
											<div class="col-2 mr-7 text-center">
												<?php echo $res_doping; ?>
											</div>
											<div class="col-2 mr-7 text-center">
												<?php
												if($socio == 1){
													 echo $res_final; 
												}?>
											</div>
	                  </div>
	                </div>
	              </div>
	            </div>
	          </div>
	        </div>
        <?php 
					}
				}
				else{ ?>
					<div class="div_item_contenedor mt-5">
						<div class="col-md-12">
							<div class="div_item">
								<p class="text-center">Sin registro de candidatos</p>
							</div>
						</div>
					</div>
		<?php 
				}
		?>
      </div>
		</div>
    
		<button id="btnRegistrar" data-toggle="modal" data-target="#newModal"><i class="fas fa-plus"></i></button>

		<script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>
		<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous"></script>
		<script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/js/bootstrap-select.min.js"></script>
		<!-- Sweetalert 2 -->
		<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10.12.7/dist/sweetalert2.js"></script>
		<!-- Bootstrap Select -->
		<script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/js/bootstrap-select.min.js"></script>
		<script>
			$(document).ready(function(){
				var msj = localStorage.getItem("success");
				if (msj == 1) {
					Swal.fire({
						position: 'center',
						icon: 'success',
						title: 'El candidato fue agregado correctamente',
						showConfirmButton: false,
						timer: 2500
					})
					localStorage.removeItem("success");
				}
				$('.alerta_descarga').click(function(){
					Swal.fire({
						position: 'center',
						icon: 'success',
						title: 'Si cuenta con internet el archivo se descargará en breve',
						showConfirmButton: false,
						timer: 4000
					})
				});
				$('a[id^=pdfDoping]').click(function(){
					var id = $(this).data('id')
					$('#pdfForm'+id).submit();
				})
				$('a[id^=pdfFinal]').click(function(){
					var id = $(this).data('id')
					$('#pdf'+id).submit();
				})
				$('#buscador').change(function(){
					var opcion = $(this).val();
					if(opcion == 0){
						$('.carta').css('display','block');
					}
					else{
						$(".carta").css('display','none');
						$('#'+opcion).css('display','block');
					}
				})
				$('#antidoping').change(function(){
					var opcion = $(this).val();
					var id_subcliente = $("#idSubcliente").val();
					var id_cliente = $("#idCliente").val();
					if(opcion == 1){
						$("#examen").prop('disabled',false);
						$.ajax({
							url: '<?php echo base_url('Doping/getPaqueteSubcliente'); ?>',
							method: 'POST',
							data: {
								'id_subcliente': id_subcliente,
								'id_cliente': id_cliente,
								'id_proyecto': 0
							},
							beforeSend: function() {
								$('.loader').css("display", "block");
							},
							success: function(res) {
								setTimeout(function() {
									$('.loader').fadeOut();
								}, 200);
								if (res != "") {
									$('#examen').val(res);
									$("#examen").prop('disabled', false);
									$("#examen").addClass('obligado');
								} else {
									$('#examen').val('');
									$("#examen").prop('disabled', false);
									$("#examen").addClass('obligado');
								}
							}
						});
					}
					else{
						$("#examen").val('');
						$("#examen").prop('disabled',true);
					}
				})
				$("#newModal").on("hidden.bs.modal", function() {
					$("#newModal input, #newModal select, #newModal textarea").val('');
					$("#newModal #examen").prop('disabled',true);
					$("#newModal #examen").val('');
					$("#newModal #msj_error").css('display', 'none');
				})
			})
			function registrar() {
				var proceso = 1;
				var datos = new FormData();
				datos.append('nombre', $("#nombre").val());
				datos.append('paterno', $("#paterno").val());
				datos.append('materno', $("#materno").val());
				datos.append('correo', $("#correo").val());
				datos.append('celular', $("#celular").val());
				datos.append('puesto', $("#puesto").val());
				datos.append('socio', $("#socio").val());
				datos.append('antidoping', $("#antidoping").val());
				datos.append('psicometrico', $("#psicometrico").val());
				datos.append('medico', $("#medico").val());
				datos.append('examen', $("#examen").val());
				datos.append('proceso', proceso);
				datos.append('otro', $("#otro_requisito").val());

				var num_files = document.getElementById('cv').files.length;
				if (num_files > 0) {
					datos.append("hay_cvs", 1);
					for (var x = 0; x < num_files; x++) {
						datos.append("cvs[]", document.getElementById('cv').files[x]);
					}
				} else {
					datos.append("hay_cvs", 0);
				}
				$.ajax({
					url: '<?php echo base_url('Subclientes_Panel/registrar'); ?>',
					type: 'POST',
					data: datos,
					contentType: false,
					cache: false,
					processData: false,
					beforeSend: function() {
						$('.loader').css("display", "block");
					},
					success: function(res) {
						var data = JSON.parse(res);
						if (data.codigo === 1) {
							localStorage.setItem("success", 1);
							location.reload();
						} else {
							setTimeout(function() {
								$('.loader').fadeOut();
							}, 200);
							$("#newModal #msj_error").css('display', 'block').html(data.msg);
						}
					}
				});
			}
			function verAvances(candidato,id){
				$('.nombreCandidato').text(candidato)
				$.ajax({
					url: '<?php echo base_url('Candidato/viewAvances'); ?>',
					type: 'post',
					data: {'id_candidato':id,'espanol':1},
					success : function(res){ 
						$("#div_avances").html(res);
					}
				});
				$("#avancesModal").modal("show");
			}
			function verEstatus(candidato,visitador,idEstudios,idSociales,idPersonales,idLaborales,idLegales){
				var salida = "";
				var visitado = (visitador == 0)? "<tr><th>Documentación</th><th>En proceso</th></tr><tr><th>Datos del grupo familiar</th><th>En proceso</th></tr><tr><th>Egresos mensuales</th><th>En proceso</th></tr><tr><th>Habitación y medio ambiente</th><th>En proceso</th></tr><tr><th>Referencias vecinales</th><th>En proceso</th></tr>":"<tr><th>Documentación</th><th>Terminado</th></tr><tr><th>Datos del grupo familiar</th><th>Terminado</th></tr><tr><th>Egresos mensuales</th><th>Terminado</th></tr><tr><th>Habitación y medio ambiente</th><th>Terminado</th></tr><tr><th>Referencias vecinales</th><th>Terminado</th></tr>";

				var estudios = (idEstudios == 0)? "<tr><th>Historial académico </th><th>En proceso</th></tr>":"<tr><th>Historial académico </th><th>Terminado</th></tr>";
				var sociales = (idSociales == 0)? "<tr><th>Antecedentes sociales </th><th>En proceso</th></tr>":"<tr><th>Antecedentes sociales </th><th>Terminado</th></tr>";
				var personales = (idPersonales == 0)? "<tr><th>Referencias personales </th><th>En proceso</th></tr>":"<tr><th>Referencias personales </th><th>Terminado</th></tr>";
				var laborales = (idLaborales == 0)? "<tr><th>Antecedentes laborales </th><th>En proceso</th></tr>":"<tr><th>Antecedentes laborales </th><th>Terminado</th></tr>";
				var legales = (idLegales == 0)? "<tr><th>Investigación legal </th><th>En proceso</th></tr>":"<tr><th>Investigación legal </th><th>Terminado</th></tr>";

				salida += '<table class="table table-striped">';
				salida += '<thead>';
				salida += '<tr>';
				salida += '<th scope="col">Concepto</th>';
				salida += '<th scope="col">Estatus</th>';
				salida += '</tr>';
				salida += '</thead>';
				salida += '<tbody>';
				salida += visitado;
				salida += estudios;
				salida += sociales;
				salida += personales;
				salida += laborales;
				salida += legales;
				salida += '</tbody>';
				salida += '</table>';
				$('.nombreCandidato').text(candidato)
				$("#div_status").html(salida);
				$("#statusModal").modal("show");
			}
		</script>
       
	</body>
    
</html>