<!DOCTYPE html>
<html>

<head>
	<title>LAR Nueva Contraseña</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<!-- CSS -->
	<?php echo link_tag("css/login.css"); ?>
	<!-- Google Font -->
	<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
	<!-- Favicon -->
  <link rel="icon" type="image/png" href="<?php echo base_url(); ?>img/mini-logo.png" />

</head>

<body>

	<!-- Login form -->
	<div class="flex-container">
		<div class="flex-item">
			<div class="logo">
				<img src="<?php echo base_url();?>img/lar-logo.png">
			</div>
			<p><b>Su nueva contraseña ha sido enviada a su correo, favor de revisar su bandeja</b></p>
		</div>	
	</div>
	
	
	</script>
	<!--- JQuery -->
	<script type="text/javascript" src="<?php echo base_url();?>jquery/jquery-3.3.1.min.js"></script>
	<!-- Bootstrap 4 -->
	<script src="https://npmcdn.com/tether@1.2.4/dist/js/tether.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js" integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn" crossorigin="anonymous"></script>

</body>

</html>