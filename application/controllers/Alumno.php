<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Alumno extends CI_Controller{

	function __construct(){
		parent::__construct();
	}
	function getMunicipios(){
		$id_estado = $_POST['id_estado'];
		$data['municipios'] = $this->alumno_model->getMunicipios($id_estado);
		$salida = "<option value=''>Selecciona</option>";
		if($data['municipios']){
			foreach ($data['municipios'] as $row){
				$salida .= "<option value='".$row->id."'>".$row->nombre."</option>";
			} 
	        echo $salida;
	    }
	    else{
	    	echo $salida;
	    }
	}
	function registrarAlumno(){
        /*$this->form_validation->set_rules('nombre', 'Nombre', 'required|trim|callback_alpha_space_only');
        $this->form_validation->set_rules('paterno', 'Apellido paterno', 'required|trim|callback_alpha_space_only');
        $this->form_validation->set_rules('materno', 'Apellido materno', 'required|trim|callback_alpha_space_only');
        $this->form_validation->set_rules('calle', 'Calle', 'required');
        $this->form_validation->set_rules('exterior', 'No. Exterior', 'required');
        $this->form_validation->set_rules('colonia', 'Colonia', 'required');
        $this->form_validation->set_rules('estado', 'Estado', 'required|numeric');
        $this->form_validation->set_rules('municipio', 'Municipio', 'required|numeric');
        $this->form_validation->set_rules('cp', 'Codigo postal', 'required|numeric');
        
        $this->form_validation->set_message('required','El campo %s es obligatorio');
        //$this->form_validation->set_message('alpha','El campo %s debe estar compuesto solo por letras');
        //$this->form_validation->set_message('valid_email','El campo %s debe ser un email válido');
        $this->form_validation->set_message('numeric','El campo %s debe ser numérico');
        //$this->form_validation->set_message('min_length','El campo %s no es válido');
        //$this->form_validation->set_message('max_length','El campo %s no es válido');
        //$this->form_validation->set_message('less_than','El campo %s no es válido');
        //$this->form_validation->set_message('greater_than','El campo %s no es válido');

        if($this->form_validation->run() != TRUE){ //Si la validación es incorrecta
            echo validation_errors();
        }
        if($this->form_validation->run() == TRUE){*/
            date_default_timezone_set('America/Mexico_City');
            $date = date('Y-m-d H:i:s');
            $id_usuario = $this->session->userdata('id');
            $cadena = $this->input->post('data');
        	parse_str($cadena, $dato);

        	if($dato['personal'] == 2){
        		$data = array(
	                'creacion' => $date,
	                'edicion' => $date,
	                'id_usuario' => $id_usuario,
	                'id_cliente' => $dato['id_cliente'],
	                'id_subcliente' => $dato['subcliente'],
	                'nombre' => ucwords(strtolower($dato['nombre'])),
	                'paterno' => ucwords(strtolower($dato['paterno'])),
	                'materno' => ucwords(strtolower($dato['materno'])),
	                'correo' => $dato['correo'],
	                'celular' => $dato['celular'],
	                'telefono' => $dato['fijo'],
	                'calle' => $dato['calle'],
	                'exterior' => $dato['exterior'],
	                'interior' => $dato['interior'],
	                'colonia' => $dato['colonia'],
	                'id_estado' => $dato['estado'],
	                'id_municipio' => $dato['municipio'],
	                'cp' => $dato['cp']
	            );
	            $this->alumno_model->registrarAlumno($data);

	            echo $salida = 1;
        	}
        	else{
        		echo $salida = 0;
        	}           
            
        //}
    }

    /************************************************ Rules Validate Form ************************************************/

    //Regla para nombres con espacios
    function alpha_space_only($str){
        if (!preg_match("/^[a-zA-Z ]+$/",$str)){
            $this->form_validation->set_message('alpha_space_only', 'El campo %s debe estar compuesto solo por letras y espacios y no debe estar vacío');
            return FALSE;
        }
        else{
            return TRUE;
        }
    }
	

}