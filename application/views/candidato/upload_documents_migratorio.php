<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<title>Upload documents | RODI</title>
	<link rel="stylesheet" href="<?php echo base_url() ?>css/candidato/bootstrap.min.css">
	<link rel="stylesheet" href="<?php echo base_url() ?>css/candidato/style.css">
	<link rel="stylesheet" href="<?php echo base_url() ?>css/candidato/bootstrap-datetimepicker.min.css">
	<link rel="stylesheet" href="<?php echo base_url() ?>css/candidato/skins/all.css">
	<link rel="icon" type="image/jpg" href="<?php echo base_url() ?>img/favicon.jpg" sizes="64x64">
	<!-- Google Font -->
	<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
	<script src="https://kit.fontawesome.com/fdf6fee49b.js"></script>
	<!-- DataTable -->
	<link rel="stylesheet" href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
	<script src="https://kit.fontawesome.com/fdf6fee49b.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
</head>
<body>
	<div class="modal fade" id="errorModal" role="dialog">
	  	<div class="modal-dialog">
	    	<div class="modal-content">
	      		<div class="modal-header">
	        		<button type="button" class="close" data-dismiss="modal">&times;</button>
	        		<h4 class="modal-title">¡Something is wrong!</h4>
	      		</div>
	      		<div class="modal-body">
	        		<p class="text-red">Contact the admin please.</p>
	    		</div>
		    	<div class="modal-footer">
		      		<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
		    	</div>
	  		</div>
		</div>
	</div>
	<div class="modal fade" id="mensajeModal" role="dialog" data-backdrop="static" data-keyboard="false">
	  	<div class="modal-dialog modal-lg modal-dialog-centered">
	    	<div class="modal-content">
	      		<div class="modal-header">
	      			<h4 class="modal-title">Success!</h4>
	        		<!--button type="button" class="close" data-dismiss="modal" aria-label="Close">
			          <span aria-hidden="true">&times;</span>
			        </button-->
	      		</div>
	      		<div class="modal-body">
	      			<p>Your documents have been uploaded successfully if we need any further documentacion or information we will contact you please, be aware.</p>
	    		</div>
	  		</div>
		</div>
	</div>
	<header>
		<nav class="navbar navbar-expand-lg navbar-light bg-rodi">
		  	<a class="navbar-brand" href="#">
		  		<img src="<?php echo base_url() ?>/img/favicon.jpg" width="32" height="32" class="d-inline-block align-top">
		  		<?php echo $this->session->userdata('nombre')." ".$this->session->userdata('paterno')." ".$this->session->userdata('materno'); ?>
		  	</a>
		  	<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
		    <span class="navbar-toggler-icon"></span>
		  	</button>
		  	<div class="collapse navbar-collapse" id="navbarNavDropdown">
			    <ul class="navbar-nav ml-auto">
			    	<li class="nav-item dropdown">
				        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-user icon"></i>
				          <?php echo $this->session->userdata('correo'); ?>
				        </a>
				        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
				          <a class="dropdown-item" id="closeSession" href="<?php echo base_url(); ?>index.php/Login/logout">Sign out</a>
				        </div>
				    </li>
			    </ul>
		  	</div>
		</nav>
	</header>
	<div class="loader" style="display: none;"></div>
	<section class="contenido">
		<div class="container">
			<div class="row">
				<div class="col-lg-6 offset-lg-3">
					<div class="div_titulo">
						<p class="titulo">Upload your documents</p>
					</div>
				</div>
			</div>
			<div class="formulario">
				<!--form method="post" id="documentos" enctype="multipart/form-data"-->
					<div class="row">
						<div class="col-lg-12">
							<p class='text-center p-3 mb-2 bg-danger text-white'>Verify and upload the following documents in pdf, jpg or png formats (Max. 2MB each)
							</p><hr>
					        <div class="row">
								<div class="col-md-6">
									<label>Upload your professional licence or highest studies certificate</label>
									<?php 
										if(in_array(7, $docs_candidato) || in_array(10, $docs_candidato)){ ?>
											<div id="estudios_completado">
				                            	<div class="alert alert-success mensaje"><strong>Document uploaded succesfully</strong></div>
				                        	</div>
									<?php
										}
										else{ ?>
											<div id="estudios_completado">
				                            	<input id="doc_estudios" class="obligado"  type="file" name="doc_estudios" accept=".pdf, .jpg, .jpeg, .png" multiple><br><br>
				                            	<button type="button" class="btn btn-primary" onclick="subirEstudios()">Upload file</button><br><br>
				                        	</div>

									<?php	}

									?>
								</div>
								<div class="col-md-6">
									<label>Upload your non-criminal background letter (Mexican only)</label>
									<?php 
										if(in_array(12, $docs_candidato)){ ?>
											<div id="antecedentes_completado">
				                            	<div class="alert alert-success mensaje"><strong>Document uploaded succesfully</strong></div>
				                        	</div>
									<?php
										}
										else{ ?>
											<div id="antecedentes_completado">
				                            	<input id="doc_criminal" type="file" name="doc_criminal" accept=".pdf, .jpg, .jpeg, .png"><br><br>
				                            	<button type="button" class="btn btn-primary" onclick="subirAntecedentes()">Upload file</button><br><br>
				                        	</div>
									<?php	}

									?>
								</div>
							</div>
							<hr>
							<div class="row">
								<div class="col-md-6">
									<label>Upload your ID (IFE, INE or Immigration ID)</label>
									<?php 
										if(in_array(3, $docs_candidato)){ ?>
											<div id="ine_completado">
				                            	<div class="alert alert-success mensaje"><strong>Document uploaded succesfully</strong></div>
				                        	</div>
									<?php
										}
										else{ ?>
											<div id="ine_completado">
				                            	<input id="doc_ine" class="obligado" type="file" name="doc_ine" accept=".pdf, .jpg, .jpeg, .png"><br><br>
				                            	<button type="button" class="btn btn-primary" onclick="subirId()">Upload file</button><br><br>
				                        	</div>
									<?php	}

									?>
								</div>
								<div class="col-md-6">
									<label>Upload your non-disclosure agreement</label><br>
									<?php 
										if(in_array(8, $docs_candidato)){ ?>
											<div id="aviso_completado">
				                            	<div class="alert alert-success mensaje"><strong>Document uploaded succesfully</strong></div>
				                        	</div>
									<?php
										}
										else{ ?>
											<div id="aviso_completado">
				                            	<input id="doc_aviso" class="obligado"  type="file" name="doc_aviso" accept=".pdf, .jpg, .jpeg, .png"><br><br>
				                            	<button type="button" class="btn btn-primary" onclick="subirAviso()">Upload file</button><br><br>
				                        	</div>
									<?php	}

									?>
								</div>
							</div>
							<hr>
							<div class="row">
								<div class="col-md-6">
									<label>Upload your IMSS report (in case you have the report)</label>
									<?php 
										if(in_array(9, $docs_candidato)){ ?>
											<div id="semanas_cotizadas_completado">
				                            	<div class="alert alert-success mensaje"><strong>Document uploaded succesfully</strong></div>
				                        	</div>
									<?php
										}
										else{ ?>
											<div id="semanas_cotizadas_completado">
				                            	<input id="doc_semanas" type="file" name="doc_semanas" accept=".pdf, .jpg, .jpeg, .png" multiple><br><br>
				                            	<button type="button" class="btn btn-primary" onclick="subirReporte()">Upload file</button><br><br>
				                        	</div>
									<?php	}

									?>
								</div>
								<div class="col-md-6">
									<label>Upload your Passport (in case you have the document)</label>
									<?php 
										if(in_array(14, $docs_candidato)){ ?>
											<div id="pasaporte_completado">
				                            	<div class="alert alert-success mensaje"><strong>Document uploaded succesfully</strong></div>
				                        	</div>
									<?php
										}
										else{ ?>
											<div id="pasaporte_completado">
				                            	<input id="doc_pasaporte" type="file" name="doc_pasaporte" accept=".pdf, .jpg, .jpeg, .png" multiple><br><br>
				                            	<button type="button" class="btn btn-primary" onclick="subirPasaporte()">Upload file</button><br><br>
				                        	</div>
									<?php	}

									?>
								</div>
							</div>
							<div class="row">
								<div class="col-md-6">
									<label>Upload your Migratory form FM, FM2 or FM3 (in case you have the document)</label>
									<?php 
										if(in_array(20, $docs_candidato)){ ?>
											<div id="forma_completado">
				                            	<div class="alert alert-success mensaje"><strong>Document uploaded succesfully</strong></div>
				                        	</div>
									<?php
										}
										else{ ?>
											<div id="forma_completado">
				                            	<input id="doc_forma" type="file" name="doc_forma" accept=".pdf, .jpg, .jpeg, .png" multiple><br><br>
				                            	<button type="button" class="btn btn-primary" onclick="subirForma()">Upload file</button><br><br>
				                        	</div>
									<?php	}

									?>
								</div>
							</div>
							<hr>
							<div class="row">
								<div class="col-md-6">
		                            <label class="contenedor_check">I agree to have read, signed and uploaded the <a href="<?php echo base_url()."privacy_notice/non-disclosure.pdf" ?>" target="_blank">non-disclosure agreement</a> / <a href="<?php echo base_url()."privacy_notice/privacy_notice.pdf" ?>" target="_blank">aviso de privacidad</a>.
	                                <input type="checkbox" name="acepto" id="acepto" value="1">
	                                <span class="checkmark"></span>
		                            </label>
		                            <br><br>
		                        </div>
							</div>
							<div class="row">
								<div class="col-md-6 offset-md-3">
									<div id="mensaje" class="alert alert-danger mensaje" style='display:none;'>
								      	<strong><span id="texto_mensaje"></span></strong>
								  	</div>
								  	<br>
								</div>

							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-4 offset-md-5">
							<button class="btn btn-danger" id="concluirDocumentacion">Finish the upload of documents</button>
						</div>
					</div>
				<!--/form-->					
			</div>
		</div>
	</section>
	<script src="<?php echo base_url() ?>js/candidato/jquery-3.3.1.min.js"></script>
	<script src="<?php echo base_url() ?>js/candidato/bootstrap.min.js"></script>
	<script src="<?php echo base_url() ?>js/candidato/candidato.js"></script>

	<script src="<?php echo base_url() ?>js/candidato/bootstrap-datetimepicker.min.js"></script>
	<script src="<?php echo base_url() ?>js/candidato/icheck.js"></script>
	<script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
	<script>
		var id_candidato = "<?php echo $id_candidato; ?>";
		var nombre = "<?php echo $nombre; ?>";
        var paterno = "<?php echo $paterno; ?>";
        var prefijo = id_candidato+"_"+nombre+""+paterno;
	    $(document).ready(function(){
			//Crear registro
			$('#doc_estudios, #doc_criminal, #doc_ine, #doc_aviso, #doc_pasaporte').change(function(){  
	           	$(this).removeClass("requerido");
			});
			//Limpiar inputs
			$(".obligado").focus(function(){
				$(this).removeClass("requerido");
			});
			$("#concluirDocumentacion").click(function(){
				var totalVacios = $('.obligado').filter(function(){
		      		return !$(this).val();
		    	}).length;

			    if(totalVacios > 0){
			      	$(".obligado").each(function() {
				        var element = $(this);
				        if (element.val() == "") {
				          	element.addClass("requerido");
				          	$("#texto_mensaje").text("Please upload all required documents");
			    			$("#doc_estudios").addClass("requerido");
			          		$("#mensaje").css("display","block");
				          	setTimeout(function(){
				            	$('#mensaje').fadeOut();
				          	},5000);
				        }
				        else{
				          	element.removeClass("requerido");
				        }
			      	});
			    }
			    else{
			    	if($('#acepto').is(':checked')){
			    		$.ajax({
				      		url: '<?php echo base_url('Candidato/finalizarDocumentos'); ?>',
				      		method: 'POST',
				      		data: {'id_candidato':id_candidato},
				      		dataType: "text",
				      		success: function(res)
				      		{
				      			if(res == 1){
				      				$("#mensajeModal").modal('show');
				      				setTimeout(function(){
						            	$('#mensajeModal').modal('hide');
						            	window.location.href = "<?php echo base_url(); ?>index.php/Login/logout";
						          	},15000);
				      			}
				      			else{
				      				$("#texto_mensaje").text("Please upload all required documents");
					          		$("#mensaje").css("display","block");
						          	setTimeout(function(){
						            	$('#mensaje').fadeOut();
						          	},5000);
				      			}

				      		}
				    	});
					}
					else{
						$("#texto_mensaje").text("Please check the non-disclosure agreement");
			          	$("#mensaje").css("display","block");
			          	setTimeout(function(){
			            	$("#mensaje").fadeOut();
			          	}, 4000);
					}
			    }
			});
	    });
	    function subirEstudios(){
	    	if($("#doc_estudios").val() == ""){
	    		$("#texto_mensaje").text("Select your professional licence or studies certificate");
	    		$("#doc_estudios").addClass("requerido");
	          	$("#mensaje").css("display","block");
	          	setTimeout(function(){
	            	$("#mensaje").fadeOut();
	          	}, 4000);
		    }
		    else{
		    	var docs = new FormData();
		    	var num_files = document.getElementById('doc_estudios').files.length;
	            for(var x = 0; x < num_files; x++) {
	                docs.append("estudios[]", document.getElementById('doc_estudios').files[x]);
	            }
			   	//var est = $("#doc_estudios")[0].files[0];
			   	//docs.append("estudios", est);
	            docs.append("id_candidato", id_candidato);
	            docs.append("prefijo", prefijo);
	            $.ajax({  
	                url:"<?php echo base_url('Candidato/subirEstudiosCandidato'); ?>",   
	                method: "POST",  
	                data: docs,  
	                contentType: false,  
	                cache: false,  
	                processData:false,  
	                dataType: "json",
	                beforeSend: function(){
	                	$('.loader').css("display","block");
	              	},
	                success:function(res){
	                	setTimeout(function(){
	                  		$('.loader').fadeOut();
	                  		$("#estudios_completado").empty();
	                		$("#estudios_completado").html('<div class="alert alert-success mensaje"><strong>Document uploaded succesfully</strong></div>');
	                	},400);
	                }  
	            });    
		    }
	    }
	    function subirAntecedentes(){
	    	if($("#doc_criminal").val() == ""){
	    		$("#texto_mensaje").text("Select your non-criminal background letter");
	    		$("#doc_criminal").addClass("requerido");
	          	$("#mensaje").css("display","block");
	          	setTimeout(function(){
	            	$("#mensaje").fadeOut();
	          	}, 4000);
		    }
		    else{
			   	var docs = new FormData();
			   	var est = $("#doc_criminal")[0].files[0];
			   	docs.append("criminal", est);
	            docs.append("id_candidato", id_candidato);
	            docs.append("prefijo", prefijo);
	            $.ajax({  
	                url:"<?php echo base_url('Candidato/subirAntecedenteCandidato'); ?>",   
	                method: "POST",  
	                data: docs,  
	                contentType: false,  
	                cache: false,  
	                processData:false,  
	                dataType: "json",
	                beforeSend: function(){
	                	$('.loader').css("display","block");
	              	},
	                success:function(res){
	                	setTimeout(function(){
	                  		$('.loader').fadeOut();
	                  		$("#antecedentes_completado").empty();
	                		$("#antecedentes_completado").html('<div class="alert alert-success mensaje"><strong>Document uploaded succesfully</strong></div>');
	                	},400);
	                }  
	            });    
		    }
	    }
	    function subirId(){
	    	if($("#doc_ine").val() == ""){
	    		$("#texto_mensaje").text("Select your ID (IFE, INE or Passport)");
	    		$("#doc_ine").addClass("requerido");
	          	$("#mensaje").css("display","block");
	          	setTimeout(function(){
	            	$("#mensaje").fadeOut();
	          	}, 4000);
		    }
		    else{
			   	var docs = new FormData();
			   	var ine = $("#doc_ine")[0].files[0];
			   	docs.append("ine", ine);
	            docs.append("id_candidato", id_candidato);
	            docs.append("prefijo", prefijo);
	            $.ajax({  
	                url:"<?php echo base_url('Candidato/subirIneCandidato'); ?>",   
	                method: "POST",  
	                data: docs,  
	                contentType: false,  
	                cache: false,  
	                processData:false,  
	                dataType: "json",
	                beforeSend: function(){
	                	$('.loader').css("display","block");
	              	},
	                success:function(res){
	                	setTimeout(function(){
	                  		$('.loader').fadeOut();
	                  		$("#ine_completado").empty();
	                		$("#ine_completado").html('<div class="alert alert-success mensaje"><strong>Document uploaded succesfully</strong></div>');
	                	},400);
	                }  
	            });    
		    }
	    }
	    function subirAviso(){
	    	if($("#doc_aviso").val() == ""){
	    		$("#texto_mensaje").text("Select your non-disclosure agreement");
	    		$("#doc_aviso").addClass("requerido");
	          	$("#mensaje").css("display","block");
	          	setTimeout(function(){
	            	$("#mensaje").fadeOut();
	          	}, 4000);
		    }
		    else{
			   	var docs = new FormData();
			   	var ine = $("#doc_aviso")[0].files[0];
			   	docs.append("aviso", ine);
	            docs.append("id_candidato", id_candidato);
	            docs.append("prefijo", prefijo);
	            $.ajax({  
	                url:"<?php echo base_url('Candidato/subirAvisoCandidato'); ?>",   
	                method: "POST",  
	                data: docs,  
	                contentType: false,  
	                cache: false,  
	                processData:false,  
	                dataType: "json",
	                beforeSend: function(){
	                	$('.loader').css("display","block");
	              	},
	                success:function(res){
	                	setTimeout(function(){
	                  		$('.loader').fadeOut();
	                  		$("#aviso_completado").empty();
	                		$("#aviso_completado").html('<div class="alert alert-success mensaje"><strong>Document uploaded succesfully</strong></div>');
	                	},400);
	                }  
	            });    
		    }
	    }
	    function subirReporte(){
	    	if($("#doc_semanas").val() == ""){
	    		$("#texto_mensaje").text("Select your imss report");
	    		$("#doc_semanas").addClass("requerido");
	          	$("#mensaje").css("display","block");
	          	setTimeout(function(){
	            	$("#mensaje").fadeOut();
	          	}, 4000);
		    }
		    else{
		    	var docs = new FormData();
		    	var num_files = document.getElementById('doc_semanas').files.length;
	            for(var x = 0; x < num_files; x++) {
	                docs.append("semanas[]", document.getElementById('doc_semanas').files[x]);
	            }
			   	//var est = $("#doc_estudios")[0].files[0];
			   	//docs.append("estudios", est);
	            docs.append("id_candidato", id_candidato);
	            docs.append("prefijo", prefijo);
	            $.ajax({  
	                url:"<?php echo base_url('Candidato/subirReporteIMSSCandidato'); ?>",   
	                method: "POST",  
	                data: docs,  
	                contentType: false,  
	                cache: false,  
	                processData:false,  
	                dataType: "json",
	                beforeSend: function(){
	                	$('.loader').css("display","block");
	              	},
	                success:function(res){
	                	setTimeout(function(){
	                  		$('.loader').fadeOut();
	                  		$("#semanas_cotizadas_completado").empty();
	                		$("#semanas_cotizadas_completado").html('<div class="alert alert-success mensaje"><strong>Document uploaded succesfully</strong></div>');
	                	},400);
	                }  
	            });    
		    }
	    }
	    function subirPasaporte(){
	    	if($("#doc_pasaporte").val() == ""){
	    		$("#texto_mensaje").text("Select your passport");
	    		$("#doc_pasaporte").addClass("requerido");
	          	$("#mensaje").css("display","block");
	          	setTimeout(function(){
	            	$("#mensaje").fadeOut();
	          	}, 4000);
		    }
		    else{
			   	var docs = new FormData();
			   	var pasaporte = $("#doc_pasaporte")[0].files[0];
			   	docs.append("pasaporte", pasaporte);
	            docs.append("id_candidato", id_candidato);
	            docs.append("prefijo", prefijo);
	            $.ajax({  
	                url:"<?php echo base_url('Candidato/subirPasaporte'); ?>",   
	                method: "POST",  
	                data: docs,  
	                contentType: false,  
	                cache: false,  
	                processData:false,  
	                dataType: "json",
	                beforeSend: function(){
	                	$('.loader').css("display","block");
	              	},
	                success:function(res){
	                	setTimeout(function(){
	                  		$('.loader').fadeOut();
	                  		$("#aviso_completado").empty();
	                		$("#aviso_completado").html('<div class="alert alert-success mensaje"><strong>Document uploaded succesfully</strong></div>');
	                	},400);
	                }  
	            });    
		    }
	    }
	    function subirForma(){
	    	if($("#doc_forma").val() == ""){
	    		$("#texto_mensaje").text("Select your Migratory form");
	    		$("#doc_forma").addClass("requerido");
	          	$("#mensaje").css("display","block");
	          	setTimeout(function(){
	            	$("#mensaje").fadeOut();
	          	}, 4000);
		    }
		    else{
			   	var docs = new FormData();
			   	var forma = $("#doc_forma")[0].files[0];
			   	docs.append("forma", forma);
	            docs.append("id_candidato", id_candidato);
	            docs.append("prefijo", prefijo);
	            $.ajax({  
	                url:"<?php echo base_url('Candidato/subirFormaMigratoria'); ?>",   
	                method: "POST",  
	                data: docs,  
	                contentType: false,  
	                cache: false,  
	                processData:false,  
	                dataType: "json",
	                beforeSend: function(){
	                	$('.loader').css("display","block");
	              	},
	                success:function(res){
	                	setTimeout(function(){
	                  		$('.loader').fadeOut();
	                  		$("#aviso_completado").empty();
	                		$("#aviso_completado").html('<div class="alert alert-success mensaje"><strong>Document uploaded succesfully</strong></div>');
	                	},400);
	                }  
	            });    
		    }
	    }
	</script>
</body>
</html>