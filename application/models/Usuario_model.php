<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usuario_model extends CI_Model{

	//Consulta si el usuario que quiere loguearse existe; regresa sus datos en dado caso que exista
	function existeUsuario($correo, $pass){
    	$this->db
    	->select('u.id, u.correo, u.nombre, u.paterno, u.nuevo_password, u.id_rol, rol.nombre as rol, u.logueado as loginBD')
    	->from('usuario as u')
    	->join('rol', 'rol.id = u.id_rol')
    	->where('u.correo', $correo)
    	->where('u.password', $pass)
        //->where('u.id_rol !=', 3)
    	->where('u.status', 1)
    	->where('u.eliminado', 0);

        $consulta = $this->db->get();
        $resultado = $consulta->row();
        return $resultado;
	}
    //Consulta si el usuario-cliente que quiere loguearse existe; regresa sus datos en dado caso que exista
    function existeUsuarioCliente($correo, $pass){
        $this->db
        ->select('u.id, u.correo, u.nombre, u.paterno, u.nuevo_password, u.id_cliente, cl.nombre as cliente, u.logueado as loginBD')
        ->from('usuario_cliente as u')
        ->join('cliente as cl', 'cl.id = u.id_cliente')
        ->where('u.correo', $correo)
        ->where('u.password', $pass)
        ->where('u.status', 1)
        ->where('u.eliminado', 0);

        $consulta = $this->db->get();
        $resultado = $consulta->row();
        return $resultado;
    }
    function existeUsuarioSubcliente($correo, $pass){
        $this->db
        ->select('u.id, u.correo, u.nombre, u.paterno, u.nuevo_password, u.id_cliente, cl.nombre as cliente, u.id_subcliente, sub.nombre as subcliente, u.logueado as loginBD, sub.tipo_acceso')
        ->from('usuario_subcliente as u')
        ->join('subcliente as sub', 'sub.id = u.id_subcliente')
        ->join('cliente as cl', 'cl.id = sub.id_cliente')
        ->where('u.correo', $correo)
        ->where('u.password', $pass)
        ->where('u.status', 1)
        ->where('u.eliminado', 0);

        $consulta = $this->db->get();
        $resultado = $consulta->row();
        return $resultado;
    }
    //Obtiene los tipos de formularios
    function getTipoForms(){
        $this->db
        ->select('*')
        ->from('tipo_formulario')
        ->where('status', 1)
        ->where('eliminado', 0);

        $query = $this->db->get();
        if($query->num_rows() > 0){
          return $query->result();
        }else{
          return FALSE;
        }
    }
    function getPermisos($id){
        $this->db
        ->select('p.*, c.nombre as nombreCliente, c.icono, c.url')
        ->from('usuario_permiso as up')
        ->join('permiso as p', 'p.id = up.id_permiso')
        ->join('cliente as c','c.id = p.id_cliente')
        ->where('p.id_subcliente', 0)
        ->where('up.id_usuario', $id)
        ->order_by('c.nombre','ASC');

        $query = $this->db->get();
        if($query->num_rows() > 0){
            return $query->result();
        }
        else{
            return FALSE;
        }
    }
    function getPermisosSubclientes($id){
        $this->db
        ->select('p.*, c.nombre as nombreCliente, c.icono, sub.url, sub.nombre as nombreSubcliente, p.id_subcliente')
        ->from('usuario_permiso as up')
        ->join('permiso as p', 'p.id = up.id_permiso')
        ->join('cliente as c','c.id = p.id_cliente')
        ->join('subcliente as sub','sub.id = p.id_subcliente')
        ->where('p.id_subcliente !=', 0)
        ->where('up.id_usuario', $id)
        ->order_by('c.nombre','ASC');

        $query = $this->db->get();
        if($query->num_rows() > 0){
            return $query->result();
        }
        else{
            return FALSE;
        }
    }
    function deleteToken($id_candidato){
        $this->db
        ->set('token', NULL)
        ->where('id', $id_candidato)
        ->update('candidato', $candidato);
    }
    function getModulos($id_rol){
        $this->db
        ->select('rolop.*')
        ->from('rol_operaciones as rolop')
        //->join('rol as rol', 'rol.id = rolop.id_rol')
        //->join('operaciones as op', 'op.id = rolop.id_operaciones')
        //->join('modulo as m','m.id = op.id_modulo')
        ->where('rolop.id_rol', $id_rol);

        $query = $this->db->get();
        if($query->num_rows() > 0){
            return $query->result();
        }
        else{
            return FALSE;
        }
    }
    function getDatosUsuario($id_usuario){
        $this->db
        ->select('u.correo, u.nombre, u.paterno, u.id_rol')
        ->from('usuario as u')
        ->where('u.id', $id_usuario);

        $consulta = $this->db->get();
        $resultado = $consulta->row();
        return $resultado;
    }
    function getUsuarios(){
        $this->db
        ->select('u.id, u.nombre, u.paterno, u.id_rol')
        ->from('usuario as u')
        ->where('u.status', 1)
        ->where('u.eliminado', 0)
        ->where_in('u.id_rol', [2,9])
        ->order_by('u.nombre','ASC');

        $query = $this->db->get();
        if($query->num_rows() > 0){
            return $query->result();
        }
        else{
            return FALSE;
        }
    }
}