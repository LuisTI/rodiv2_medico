<?php
defined('BASEPATH') OR exit('No direct script access allowed');

function fecha_ingles_bd($date){
	$aux = explode('/', $date);
	$fecha = $aux[2].'-'.$aux[0].'-'.$aux[1];
	return $fecha;
}
function fecha_espanol_bd($date){
	$aux = explode('/', $date);
    $fecha = $aux[2].'-'.$aux[1].'-'.$aux[0];
    return $fecha;
}
function fecha_espanol_frontend($date){
    $aux = explode('-', $date);
    $fecha = $aux[2].'/'.$aux[1].'/'.$aux[0];
    return $fecha;
}
function fecha_sinhora_espanol_bd($date){
    $f = explode(' ', $date);
    $aux = explode('-', $f[0]);
    $fecha = $aux[2].'/'.$aux[1].'/'.$aux[0];
    return $fecha;
}
function fecha_hora_espanol_bd($date){
    $time = explode(' ', $date);
    $aux = explode('/', $time[0]);
    $fecha = $aux[2].'-'.$aux[1].'-'.$aux[0].' '.$time[1].':00';
    return $fecha;
}
function fecha_sinhora_ingles_front($date){
    $f = explode(' ', $date);
    $aux = explode('-', $f[0]);
    $fecha = $aux[1].'/'.$aux[2].'/'.$aux[0];
    return $fecha;
}
function calculaEdad($fechanacimiento){
    list($ano,$mes,$dia) = explode("-",$fechanacimiento);
    $ano_diferencia  = date("Y") - $ano;
    $mes_diferencia = date("m") - $mes;
    $dia_diferencia   = date("d") - $dia;
    if ($mes_diferencia < 0 || ($mes_diferencia == 0 && $dia_diferencia < 0 || $mes_diferencia < 0))
      $ano_diferencia--;
    return $ano_diferencia;
}
function formatoFecha($f){
    date_default_timezone_set('America/Mexico_City');
    $numeroDia = date('d', strtotime($f));
    $mes = date('F', strtotime($f));
    $anio = date('Y', strtotime($f));
    $meses_ES = array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
    $meses_EN = array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
    $nombreMes = str_replace($meses_ES, $meses_EN, $mes);

    return $nombreMes." ".$numeroDia.", ".$anio;
}
function fecha_sinhora_espanol_front($date){
    $f = explode(' ', $date);
    $aux = explode('-', $f[0]);
    $fecha = $aux[2].'/'.$aux[1].'/'.$aux[0];
    return $fecha;
}
function formatoFechaEspanol($f){
    date_default_timezone_set('America/Mexico_City');
    $numeroDia = date('d', strtotime($f));
    $mes = date('F', strtotime($f));
    $anio = date('Y', strtotime($f));
    $meses_ES = array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
    $meses_EN = array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
    $nombreMes = str_replace($meses_EN, $meses_ES, $mes);

    return $nombreMes." ".$numeroDia.", ".$anio;
}
