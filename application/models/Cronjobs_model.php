<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cronjobs_model extends CI_Model{
	 /*----------------------------------------*/
    /*  Espanol
    /*----------------------------------------*/
	function getCandidatos(){
		//$clientes = array(4,95,16,54);
		//$clientes = array(6,88,86,47,42,91,58,93,39.83,87);
		$this->db
	    ->select('c.id as idCandidato, c.fecha_alta, f.id as idFin, f.creacion as fecha_final')
	    ->from('candidato as c')
	    ->join('candidato_finalizado as f','f.id_candidato = c.id','left')
	    ->join('cliente as cl','cl.id = c.id_cliente')
	    ->like('cl.url', 'control')
	    ->where('c.id_tipo_proceso', 1)
	    ->where('c.eliminado', 0);

	    $query = $this->db->get();
	    if($query->num_rows() > 0){
	    	return $query->result();
	    }else{
	      	return FALSE;
	    }
	}
	function getFechasFestivas(){
		$this->db
	    ->select('*')
	    ->from('fechas_festivas')
	    ->order_by('fecha','ASC');

	    $query = $this->db->get();
	    if($query->num_rows() > 0){
	    	return $query->result();
	    }else{
	      	return FALSE;
	    }
	}
	function registroDiasFinalizado($datos, $idFin){
		$this->db
        ->where('id', $idFin)
        ->update('candidato_finalizado', $datos);
	}
	function registroDiasEnProceso($datos, $id_candidato){
        $this->db
        ->where('id', $id_candidato)
        ->update('candidato', $datos);
    }
    /*----------------------------------------*/
    /*  Ingles
    /*----------------------------------------*/
    function getCandidatosIngles(){
			$this->db
	    ->select('c.id as idCandidato, c.fecha_alta, f.id as idFin, f.creacion as fecha_final')
	    ->from('candidato as c')
	    ->join('candidato_bgc as f','f.id_candidato = c.id','left')
	    ->join('cliente as cl','cl.id = c.id_cliente')
	    ->where('cl.ingles', 1)
	    ->where('cl.id', 1)
	    ->where('c.id_tipo_proceso', 1)
	    ->where('c.eliminado', 0);

	    $query = $this->db->get();
	    if($query->num_rows() > 0){
	    	return $query->result();
	    }else{
	      	return FALSE;
	    }
		}
	function registroDiasFinalizadoIngles($datos, $idFin){
		$this->db
        ->where('id', $idFin)
        ->update('candidato_bgc', $datos);
	}
	/*----------------------------------------*/
    /*  Ingles Tipo 2
    /*----------------------------------------*/
    function getCandidatosInglesTipo2(){
    	$clientes = array(3,77);
		$this->db
	    ->select('c.id as idCandidato, c.fecha_alta, f.id as idFin, f.creacion as fecha_final')
	    ->from('candidato as c')
	    ->join('candidato_bgc as f','f.id_candidato = c.id','left')
	    ->join('cliente as cl','cl.id = c.id_cliente')
        ->join('candidato_pruebas as pru','pru.id_candidato = c.id')
	    ->where('cl.ingles', 1)
	    ->where_in('cl.id', $clientes)
	    ->where('c.eliminado', 0)
	    ->where('pru.socioeconomico', 1);

	    $query = $this->db->get();
	    if($query->num_rows() > 0){
	    	return $query->result();
	    }else{
	      	return FALSE;
	    }
	}
	/*----------------------------------------*/
	/*  Candidatos procesos 3, 4 y 5
	/*----------------------------------------*/
    function getCandidatosProcesosEspecificos(){
    	$procesos = array(3,4,5);
			$this->db
	    ->select('c.id as idCandidato, c.fecha_alta, f.id as idFin, f.creacion as fecha_final')
	    ->from('candidato as c')
	    ->join('candidato_bgc as f','f.id_candidato = c.id','left')
	    ->join('cliente as cl','cl.id = c.id_cliente')
			->join('candidato_pruebas as pru','pru.id_candidato = c.id')
	    ->where('c.eliminado', 0)
	    ->where('pru.socioeconomico', 1)
	    ->where_in('c.id_tipo_proceso', $procesos);

	    $query = $this->db->get();
	    if($query->num_rows() > 0){
	    	return $query->result();
	    }else{
	      	return FALSE;
	    }
		}
		/*----------------------------------------*/
		/*  Avances proceso UST
		/*----------------------------------------*/
			function getCandidatosUST(){
				$this->db
				->select('c.id as idCandidato, c.fecha_contestado, est.comentarios as estudios_comentarios, verdoc.comentarios as docs_comentarios, persona.id as idFamiliar, refper.id as idRefPer, verlab.id as idVerLaboral, verest.id as idVerEstudios, estatuslab.id as idEstatusLaboral, verpenal.id as idVerPenal, doc.id as idDocumento')
				->from('candidato as c')
				->join('candidato_estudios as est','est.id_candidato = c.id',"left")
				->join('verificacion_documento as verdoc','verdoc.id_candidato = c.id',"left")
				->join('candidato_persona as persona','persona.id_candidato = c.id',"left")
				->join('candidato_ref_personal as refper','refper.id_candidato = c.id',"left")
				->join('verificacion_ref_laboral as verlab','verlab.id_candidato = c.id',"left")
				->join('verificacion_estudios as verest','verest.id_candidato = c.id',"left")
				->join('status_ref_laboral as estatuslab','estatuslab.id_candidato = c.id',"left")
				->join('verificacion_penales as verpenal','verpenal.id_candidato = c.id',"left")
				->join('candidato_documento as doc','doc.id_candidato = c.id','left')
				->join('cliente as cl','cl.id = c.id_cliente')
				->where('cl.id', 1)
				->where('c.id_tipo_proceso', 1)
				->where('c.eliminado', 0)
				->group_by('c.id');

				$query = $this->db->get();
				if($query->num_rows() > 0){
					return $query->result();
				}else{
						return FALSE;
				}
			}
			function getDocumentosObligatoriosUST($id_candidato){
				$this->db
				->select('doc.id_tipo_documento')
				->from('candidato_documento as doc')
				->where('doc.id_candidato', $id_candidato);

				$query = $this->db->get();
				if($query->num_rows() > 0){
					return $query->result();
				}else{
					return FALSE;
				}
			}
			function cleanAvance($idCandidato){
				$this->db
				->where('id_candidato', $idCandidato)
				->delete('avance_porcentaje');
			}
			function actualizarAvance($porcentaje, $idCandidato){
				$this->db
				->set('id_candidato',$idCandidato)
				->set('porcentaje',$porcentaje)
				->insert('avance_porcentaje');
			}
			/*function getCandidatosClientesGeneral(){
				$this->db
				->select('c.id as idCandidato, c.fecha_contestado, est.comentarios as estudios_comentarios, verdoc.comentarios as docs_comentarios, persona.id as idFamiliar, refper.id as idRefPer, verlab.id as idVerLaboral, verest.id as idVerEstudios, estatuslab.id as idEstatusLaboral, verpenal.id as idVerPenal, doc.id as idDocumento')
				->from('candidato as c')
				->join('candidato_estudios as est','est.id_candidato = c.id',"left")
				->join('verificacion_documento as verdoc','verdoc.id_candidato = c.id',"left")
				->join('candidato_persona as persona','persona.id_candidato = c.id',"left")
				->join('candidato_ref_personal as refper','refper.id_candidato = c.id',"left")
				->join('verificacion_ref_laboral as verlab','verlab.id_candidato = c.id',"left")
				->join('verificacion_estudios as verest','verest.id_candidato = c.id',"left")
				->join('status_ref_laboral as estatuslab','estatuslab.id_candidato = c.id',"left")
				->join('verificacion_penales as verpenal','verpenal.id_candidato = c.id',"left")
				->join('candidato_documento as doc','doc.id_candidato = c.id','left')
				->join('cliente as cl','cl.id = c.id_cliente')
				->where('cl.proceso', 'Español General')
				->where('c.eliminado', 0)
        ->where('c.cancelado', 0)
				->group_by('c.id');

				$query = $this->db->get();
				if($query->num_rows() > 0){
					return $query->result();
				}else{
						return FALSE;
				}
			}*/
}