<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reporte_model extends CI_Model{

    function getSubclientes($id_cliente){
        $this->db
        ->select('*')
        ->from('subcliente')
        ->where('id_cliente', $id_cliente)
        ->where('status', 1)
        ->where('eliminado', 0)
        ->order_by('nombre','ASC');

        $query = $this->db->get();
        if($query->num_rows() > 0){
            return $query->result();
        }else{
            return FALSE;
        }
    }
    function getProyectos($id_cliente){
        $this->db
        ->select('*')
        ->from('proyecto')
        ->where('id_cliente', $id_cliente)
        ->order_by('nombre','ASC');

        $query = $this->db->get();
        if($query->num_rows() > 0){
            return $query->result();
        }else{
            return FALSE;
        }
    }
    function getClientes(){
        $this->db
        ->select('*')
        ->from('cliente')
        ->where('habilitado', 1)
        ->order_by('id','ASC');

        $query = $this->db->get();
        if($query->num_rows() > 0){
            return $query->result();
        }else{
            return FALSE;
        }
    }
    function getUsuarios(){
        $this->db
        ->select('id')
        ->from('usuario')
        ->where('status', 1)
        ->where('eliminado', 0)
        ->order_by('id','ASC');

        $query = $this->db->get();
        if($query->num_rows() > 0){
            return $query->result();
        }else{
            return FALSE;
        }
    }
    function reporteDopingFinalizados($f_inicio, $f_fin, $cliente, $subcliente, $proyecto, $resultado, $lab){
        $filtros = "";
        $filtros .= ($f_inicio != "" && $f_inicio != null) ? "dop.fecha_resultado >= '$f_inicio 00:00:00' " : "";
        $filtros .= ($f_fin != "" && $f_fin != null) ? " AND dop.fecha_resultado <= '$f_fin 23:59:59' " : "";
        $filtros .= ($cliente == "" || $cliente == null || $cliente == 0)? "":" AND dop.id_cliente = ".$cliente;
        $filtros .= ($subcliente == "" || $subcliente == null || $subcliente == 0)? "":" AND dop.id_subcliente = ".$subcliente;
        $filtros .= ($proyecto == "" || $proyecto == null || $proyecto == 0)? "":" AND dop.id_proyecto = ".$proyecto;
        $filtros .= ($resultado == "" || $resultado == null)? "":" AND dop.resultado = ".$resultado;
        $filtros .= ($lab == "" || $lab == null)? "":" AND dop.laboratorio = '$lab' ";

        $query = $this->db
        ->query("SELECT dop.*, CONCAT(c.nombre,' ',c.paterno,' ',c.materno) as candidato, cl.nombre as cliente, sub.nombre as subcliente, pro.nombre as proyecto, paq.nombre as parametros
            FROM doping as dop 
            JOIN candidato as c ON c.id = dop.id_candidato
            JOIN cliente as cl ON cl.id = dop.id_cliente
            LEFT JOIN subcliente as sub ON sub.id = dop.id_subcliente
            LEFT JOIN proyecto as pro ON pro.id = dop.id_proyecto
            JOIN antidoping_paquete as paq ON paq.id = dop.id_antidoping_paquete
            WHERE  ".$filtros."
            ORDER BY dop.fecha_resultado DESC, cl.nombre ASC, dop.codigo_prueba ASC");

            if($query->num_rows() > 0){
                return $query->result();
            }
            else{
                return FALSE;
            }
    }
    function reporteFinalizados_HCL_UST($f_inicio, $f_fin, $cliente, $usuario){
        $filtros = "";
        $filtros .= ($f_inicio != "" && $f_inicio != null) ? "bgc.creacion >= '$f_inicio 00:00:00' " : "";
        $filtros .= ($f_fin != "" && $f_fin != null) ? " AND bgc.creacion <= '$f_fin 23:59:59' " : "";
        $filtros .= ($cliente == "" || $cliente == null || $cliente == 0)? "":" AND c.id_cliente = ".$cliente;
        $filtros .= ($usuario == "" || $usuario == null || $usuario == 0)? "":" AND c.id_usuario = ".$usuario;
        //$filtros .= "AND u.status = 1 AND u.eliminado = 0 AND u.id_rol = 2";
        //$filtros .= " AND pr.socioeconomico = 1 OR pr.id IS NULL ";

        $query = $this->db
        ->query("SELECT c.*, CONCAT(c.nombre,' ',c.paterno,' ',c.materno) as candidato, cl.nombre as cliente, CONCAT(u.nombre,' ',u.paterno) as usuario, bgc.creacion as fecha_final, bgc.tiempo
            FROM candidato as c 
            JOIN candidato_bgc as bgc ON c.id = bgc.id_candidato
            JOIN cliente as cl ON cl.id = c.id_cliente
            JOIN usuario as u ON u.id = c.id_usuario
            LEFT JOIN candidato_pruebas as pr ON pr.id_candidato = c.id
            WHERE  ".$filtros."
            ORDER BY u.nombre ASC, bgc.creacion DESC");

            if($query->num_rows() > 0){
                return $query->result();
            }
            else{
                return FALSE;
            }
    }
    function reporteFinalizados_TATA_WIPRO($f_inicio, $f_fin, $cliente, $usuario){
        $filtros = "";
        $filtros .= ($f_inicio != "" && $f_inicio != null) ? "c.edicion >= '$f_inicio 00:00:00' " : "";
        $filtros .= ($f_fin != "" && $f_fin != null) ? " AND c.edicion <= '$f_fin 23:59:59' " : "";
        $filtros .= ($cliente == "" || $cliente == null || $cliente == 0)? "":" AND c.id_cliente = ".$cliente;
        $filtros .= ($usuario == "" || $usuario == null || $usuario == 0)? "":" AND c.id_usuario = ".$usuario;
        //$filtros .= "AND u.status = 1 AND u.eliminado = 0 AND u.id_rol = 2";
        $filtros .= " AND pr.socioeconomico = 1";

        $query = $this->db
        ->query("SELECT c.*, CONCAT(c.nombre,' ',c.paterno,' ',c.materno) as candidato, cl.nombre as cliente, CONCAT(u.nombre,' ',u.paterno) as usuario, c.edicion as fecha_final
            FROM candidato as c 
            JOIN cliente as cl ON cl.id = c.id_cliente
            JOIN usuario as u ON u.id = c.id_usuario
            JOIN candidato_pruebas as pr ON pr.id_candidato = c.id
            WHERE  ".$filtros."
            ORDER BY u.nombre ASC, c.edicion DESC");

            if($query->num_rows() > 0){
                return $query->result();
            }
            else{
                return FALSE;
            }
    }
    function reporteFinalizados_Espanol($f_inicio, $f_fin, $cliente, $usuario){
        $filtros = "";
        $filtros .= ($f_inicio != "" && $f_inicio != null) ? "f.creacion >= '$f_inicio 00:00:00' " : "";
        $filtros .= ($f_fin != "" && $f_fin != null) ? " AND f.creacion <= '$f_fin 23:59:59' " : "";
        $filtros .= ($cliente == "" || $cliente == null || $cliente == 0)? "":" AND c.id_cliente = ".$cliente;
        $filtros .= ($usuario == "" || $usuario == null || $usuario == 0)? "":" AND c.id_usuario = ".$usuario;
        //$filtros .= "AND u.status = 1 AND u.eliminado = 0 AND u.id_rol = 2";
        $filtros .= " AND pr.socioeconomico = 1";

        $query = $this->db
        ->query("SELECT c.*, CONCAT(c.nombre,' ',c.paterno,' ',c.materno) as candidato, cl.nombre as cliente, CONCAT(u.nombre,' ',u.paterno) as usuario, f.creacion as fecha_final, f.tiempo
            FROM candidato as c 
            JOIN candidato_finalizado as f ON c.id = f.id_candidato
            JOIN cliente as cl ON cl.id = c.id_cliente
            JOIN usuario as u ON u.id = c.id_usuario
            JOIN candidato_pruebas as pr ON pr.id_candidato = c.id
            WHERE  ".$filtros."
            ORDER BY u.nombre ASC, f.creacion DESC");

            if($query->num_rows() > 0){
                return $query->result();
            }
            else{
                return FALSE;
            }
    }
    function reporteDopingGeneral($f_inicio, $f_fin, $cliente, $subcliente, $proyecto){
        $filtros = "";
        $filtros .= ($f_inicio != "" && $f_inicio != null) ? "dop.creacion >= '$f_inicio 00:00:00' " : "";
        $filtros .= ($f_fin != "" && $f_fin != null) ? " AND dop.creacion <= '$f_fin 23:59:59' " : "";
        $filtros .= ($cliente == "" || $cliente == null || $cliente == 0)? "":" AND dop.id_cliente = ".$cliente;
        $filtros .= ($subcliente == "" || $subcliente == null || $subcliente == 0)? "":" AND dop.id_subcliente = ".$subcliente;
        $filtros .= ($proyecto == "" || $proyecto == null || $proyecto == 0)? "":" AND dop.id_proyecto = ".$proyecto;

        $query = $this->db
        ->query("SELECT dop.*, CONCAT(c.nombre,' ',c.paterno,' ',c.materno) as candidato, cl.nombre as cliente, sub.nombre as subcliente, pro.nombre as proyecto, paq.nombre as parametros
            FROM doping as dop 
            JOIN candidato as c ON c.id = dop.id_candidato
            JOIN cliente as cl ON cl.id = dop.id_cliente
            LEFT JOIN subcliente as sub ON sub.id = dop.id_subcliente
            LEFT JOIN proyecto as pro ON pro.id = dop.id_proyecto
            JOIN antidoping_paquete as paq ON paq.id = dop.id_antidoping_paquete
            WHERE  ".$filtros."
            ORDER BY dop.creacion DESC, cl.nombre ASC, dop.codigo_prueba ASC");

            if($query->num_rows() > 0){
                return $query->result();
            }
            else{
                return FALSE;
            }
    }
}