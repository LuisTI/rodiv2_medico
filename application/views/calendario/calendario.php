 <style>
	#calendar {
		max-width: 800px;
	}
	.col-centered{
		float: none;
		margin: 0 auto;
	}
    </style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<div id="exito" class="alert alert-success fade in mensaje" style='display:none;'>
      	<strong>¡Éxito!</strong> Los documentos han sido actualizados correctamente.
  	</div>
  	<div id="exitoEstudios" class="alert alert-success fade in mensaje" style='display:none;'>
      	<strong>¡Éxito!</strong> La verificación de estudios ha sido finalizada correctamente.
  	</div>
  	<div id="exitoLaborales" class="alert alert-success fade in mensaje" style='display:none;'>
      	<strong>¡Éxito!</strong> La verificación de las referencias laborales ha sido finalizada correctamente.
  	</div>
  	<div id="exitoPenales" class="alert alert-success fade in mensaje" style='display:none;'>
      	<strong>¡Éxito!</strong> La verificación de los antecedentes no penales ha sido finalizada correctamente.
  	</div>
  	<div id="exitoCandidato" class="alert alert-success fade in mensaje" style='display:none;'>
      	<strong>¡Éxito!</strong> Los datos del candidato han sido actualizados correctamente.
  	</div>
  	<div id="vacios" class="alert alert-danger fade in mensaje" style='display:none;'>
      	<strong>¡Atención!</strong><span id="txt_vacios"></span>
  	</div>
  	<div id="exitoFinalizado" class="alert alert-success fade in mensaje" style='display:none;'>
      	<strong>¡Éxito!</strong> El proceso del candidato ha finalizado correctamente.
  	</div>
  	<div id="mensaje" class="alert alert-success in mensaje" style='display:none;'>
      	<strong>Éxito!</strong><p id="texto_msj"></p>
  	</div>
  	<div class="modal fade" id="newModal" role="dialog" data-backdrop="static" data-keyboard="false">
	  	<div class="modal-dialog modal-lg" role="document">
		    <div class="modal-content">
		      	<div class="modal-header">
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			          <span aria-hidden="true">&times;</span>
			        </button>
			        <h4 class="modal-title">Nuevo Registro</h4>
		      	</div>
		      	<div class="modal-body">
		        	<?php 
					echo form_open('Candidato/addCandidate', 'id="datos"'); ?>
						<div class="row">
							<div class="col-md-8 col-md-offset-2">
								<label for="cliente">Cliente *</label>
			        			<select name="cliente" id="cliente" class="form-control obligado">
						            <option value="">Selecciona</option>
						            <?php foreach ($clientes as $cl) {?>
						                <option value="<?php echo $cl->id; ?>"><?php echo $cl->nombre; ?></option>
						            <?php } ?>
					          	</select>
					          	<br>
							</div>
						</div>
						<div class="row">
							<div class="col-md-8 col-md-offset-2">
								<label for="subcliente">Subcliente o Proveedor *</label>
			        			<select name="subcliente" id="subcliente" class="form-control obligado" disabled>
						            <option value="">Selecciona</option>
					          	</select>
					          	<br><br>
							</div>
						</div>
			        	<div class="row">
			        		<div class="col-md-4">
			        			<label for="nombre">Nombre(s) *</label>
			        			<input type="text" class="form-control obligado" name="nombre" id="nombre" placeholder="Nombre(s)">
			        			<br>
			        		</div>
			        		<div class="col-md-4">
			        			<label for="paterno">Apellido paterno *</label>
			        			<input type="text" class="form-control obligado" name="paterno" id="paterno" placeholder="Apellido Paterno">
			        			<br>
			        		</div>
			        		<div class="col-md-4">
			        			<label for="materno">Apellido materno *</label>
			        			<input type="text" class="form-control obligado" name="materno" id="materno" placeholder="Apellido materno">
			        			<br>
			        		</div>			    
			        	</div>
			        	<div class="row">
			        		<div class="col-md-4">
			        			<label for="correo">Email *</label>
			        			<input type="email" class="form-control obligado" name="correo" id="correo" placeholder="Email" onKeyUp="document.getElementById(this.id).value=document.getElementById(this.id).value.toLowerCase()">
			        			<br>
			        		</div>
			        		<div class="col-md-4">
			        			<label for="celular">Num. Tel. Celular *</label>
			        			<input type="text" class="form-control solo_numeros obligado" name="celular" id="celular" placeholder="Num. Tel. Celular" maxlength="10">
			        			<br>
			        		</div>
			        		<div class="col-md-4">
			        			<label for="fijo">Num. Tel. Casa </label>
			        			<input type="text" class="form-control solo_numeros" name="fijo" id="fijo" placeholder="Num. Tel. Casa" maxlength="10">
			        			<br>
			        		</div>
			        	</div>
			        	<div class="row">
			        		<div class="col-md-4 offset-md-5">
			        			<p class="text-right">¿Qué estudios requiere el candidato?</p>
			        		</div>
			        	</div>
			        	<div class="row">
			        		<div class="col-md-4">
			        			<label for="puesto">Puesto *</label>
			        			<select name="puesto" id="puesto" class="form-control obligado">
						            <option value="">Selecciona</option>
						            <?php foreach ($baterias as $b) {?>
						                <option value="<?php echo $b->id; ?>"><?php echo $b->nombre; ?></option>
						            <?php } ?>
					          	</select>
					          	<br>
			        		</div>
			        		<div class="col-md-2 padding-40 estudio">
			        			<label class="contenedor_check fuente-14">Socioeconómico
	                                <input type="checkbox" name="socio" id="socio">
	                                <span class="checkmark"></span>
	                            </label>
	                            <br>
	                            <label class="contenedor_check fuente-14">Antidoping
	                                <input type="checkbox" name="antidoping" id="antidoping">
	                                <span class="checkmark"></span>
	                            </label>
	                            <br>
			        		</div>
			        		<div class="col-md-2 padding-40 estudio">
			        			<label class="contenedor_check fuente-14">Psicométrico
	                                <input type="checkbox" name="psicometrico" id="psicometrico">
	                                <span class="checkmark"></span>
	                            </label>
	                            <br>
	                            <label class="contenedor_check fuente-14">Médico
	                                <input type="checkbox" name="medico" id="medico">
	                                <span class="checkmark"></span>
	                            </label>
	                            <br>
			        		</div>
			        		<div class="col-md-2 padding-40 estudio">
			        			<label class="contenedor_check fuente-14">Buró de Crédito
	                                <input type="checkbox" name="buro" id="buro">
	                                <span class="checkmark"></span>
	                            </label>
	                            <br>
	                            <label class="contenedor_check fuente-14">Sociolaboral
	                                <input type="checkbox" name="laboral" id="laboral">
	                                <span class="checkmark"></span>
	                            </label>
	                            <br>
			        		</div>
			        	</div>
			        	<div class="row">
			        		<div class="col-md-4">
			        			<p class="text-center msj_error" id="msj_puesto">Elija el puesto para el candidato</p>
					          	<br>
			        		</div>
			        		<div class="col-md-8">
			        			<label for="otro_requisito">¿Requiere algo más para el candidato?</label>
			        			<textarea class="form-control" name="otro_requisito" id="otro_requisito" rows="3" placeholder="Escriba su mensaje"></textarea>
			        			<br>
			        		</div>
			        	</div>
			        	<div id="div_antidoping" class="padding-40">
			        		
			        	</div>
			        	<div id="div_psicometrico" class="padding-40">
			        		
			        	</div>
			        	<div id="div_buro" class="padding-40">
			        		
			        	</div>
			        	<div class="row">
			        		<div class="col-md-4 col-md-offset-4">
			        			<label for="cv">Cargar CV o solicitud de empleo del candidato *</label>
			        			<input type="file" id="cv" name="cv" class="form-control obligado" accept=".pdf, .jpg, .jpeg, .png"><br>
			        		</div>
			        	</div>
			        <?php if(!empty(validation_errors())): ?>
						<div class="alert alert-danger in mensaje">
						  	<?php echo validation_errors(); ?>
						</div>
					<?php endif; ?>
					<div id="alert-msg">
						
					</div>
			        <div id="campos_vacios" class="alert alert-danger">
		      			<p class="msj_error">Hay campos vacíos</p>
		      		</div>
		      		<div id="repetido">
		      			<p class="msj_error">El nombre y/o el email del candidato ya existen</p>
		      		</div>
		      	</div>
		      	<div class="modal-footer">
			        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
			        <button type="submit" class="btn btn-danger" name="submit" id="registrar">Registrar</button>
			        <?php echo form_close(); ?>
		      	</div>
		    </div>
	 	</div>
	</div>
	<div class="modal fade" id="visitaModal" role="dialog" data-backdrop="static" data-keyboard="false">
	  	<div class="modal-dialog modal-lg" role="document">
		    <div class="modal-content">
		      	<div class="modal-header">
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			          <span aria-hidden="true">&times;</span>
			        </button>
			        <h4 class="modal-title">Registro de visita al candidato</h4>
		      	</div>
		      	<div class="modal-body">
		        	<?php 
					echo form_open('Candidato/createVisita', 'id="datos_visita"'); ?>
						<div class="row">
							<div class="col-md-8">
								<h4 id="visita_candidato"></h4>
					          	
					          	<h5 id="visita_cliente"></h5>
					          	<input type="hidden" id="idCandidato">
					          	<input type="hidden" id="idCliente">
					          	<br>
							</div>
						</div>
						<div class="row">
							<div class="col-md-2">
			        			<label for="v_fecha_visita">Fecha de visita *</label>
			        			<input type="text" class="form-control solo_lectura visita_obligado" name="v_fecha_visita" id="v_fecha_visita" placeholder="dd/mm/yyyy" readonly>
			        			<br>
			        		</div>
			        		<div class="col-md-4">
				        		<label for="v_hora_inicial">De *<span id="v_div_hora_inicial"></span></label>
			        			<select name="v_hora_inicial" id="v_hora_inicial" class="form-control visita_obligado" disabled>
			        				<option value="">Selecciona</option>
						            <?php for($i = 0; $i < count($horas_iniciales); $i++) {?>
						                <option value="<?php echo $horas_iniciales[$i]; ?>"><?php echo $horas_iniciales[$i]; ?></option>
						            <?php } ?>
					          	</select>
					          	<br>
				        	</div>
				        	<div class="col-md-4">
				        		<label for="v_hora_final">A *<span id="v_div_hora_final"></span></label>
			        			<select name="v_hora_final" id="v_hora_final" class="form-control visita_obligado" disabled>
			        				<option value="">Selecciona</option>
						            <?php for($i = 0; $i < count($horas_finales); $i++) {?>
						                <option value="<?php echo $horas_finales[$i]; ?>"><?php echo $horas_finales[$i]; ?></option>
						            <?php } ?>
					          	</select>
					          	<br>
				        	</div>
						</div>
			        	<div class="row">
				        	<div class="col-md-4">
			        			<label for="v_calle">Calle *</label>
			        			<input type="text" class="form-control visita_obligado" name="v_calle" id="v_calle" placeholder="Address">
			        			<br>
			        		</div>
			        		<div class="col-md-2">
			        			<label for="v_exterior">Num. Exterior *</label>
			        			<input type="text" class="form-control visita_obligado" name="v_exterior" id="v_exterior" placeholder="Num. Exterior">
			        			<br>
			        		</div>
			        		<div class="col-md-2">
			        			<label for="v_interior">Num. Interior </label>
			        			<input type="text" class="form-control" name="v_interior" id="v_interior" placeholder="Num. Interior">
			        			<br>
			        		</div>
			        		<div class="col-md-4">
			        			<label for="v_colonia">Colonia *</label>
			        			<input type="text" class="form-control visita_obligado" name="v_colonia" id="v_colonia" placeholder="Colonia">
			        			<br>
			        		</div>
				        </div>
				        <div class="row">
				        	<div class="col-md-4">
			        			<label for="v_estado">Estado *</label>
			        			<select name="v_estado" id="v_estado" class="form-control visita_obligado">
						            <option value="">Selecciona</option>
						            <?php foreach ($estados as $e) {?>
						                <option value="<?php echo $e->id; ?>"><?php echo $e->nombre; ?></option>
						            <?php } ?>
					          	</select>
					          	<br>
			        		</div>
			        		<div class="col-md-4">
			        			<label for="v_municipio">Municipio *</label>
			        			<select name="v_municipio" id="v_municipio" class="form-control visita_obligado" disabled>
						            <option value="-1">Selecciona</option>
					          	</select>
					          	<br>
			        		</div>
			        		<div class="col-md-2">
			        			<label for="v_cp">Código Postal *</label>
			        			<input type="text" class="form-control solo_numeros visita_obligado" name="v_cp" id="v_cp" placeholder="Código Postal" maxlength="5">
			        			<br>
			        		</div>
				        </div>
				        <div class="row">
			        		<div class="col-md-3">
			        			<label for="v_celular">Tel. Celular *</label>
			        			<input type="text" class="form-control solo_numeros visita_obligado" name="v_celular" id="v_celular" placeholder="Mobile Number" maxlength="10">
			        			<br>
			        		</div>
			        		<div class="col-md-3">
			        			<label for="v_tel_casa">Tel. Casa </label>
			        			<input type="text" class="form-control solo_numeros" name="v_tel_casa" id="v_tel_casa" placeholder="Num. de Casa" maxlength="10">
			        			<br>
			        		</div>
			        		<div class="col-md-3">
			        			<label for="v_tel_otro">Tel. Oficina  </label>
			        			<input type="text" class="form-control solo_numeros" name="v_tel_otro" id="v_tel_otro" placeholder="Tel. Oficina" maxlength="10">
			        			<br>
			        		</div>
				        </div>
						<div id="alert-msg">
							
						</div>
				        <div id="campos_vacios" class="alert alert-danger">
			      			<p class="msj_error">Hay campos vacíos</p>
			      		</div>
			      		<div id="repetido">
			      			<p class="msj_error">El nombre y/o el email del candidato ya existen</p>
			      		</div>
		      	</div>
		      	<div class="modal-footer">
			        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
			        <button type="submit" class="btn btn-danger" name="submit" id="visitar">Registrar</button>
			        <?php echo form_close(); ?>
		      	</div>
		    </div>
	 	</div>
	</div>
	<div class="modal fade" id="verModal" role="dialog" data-backdrop="static" data-keyboard="false">
	  	<div class="modal-dialog modal-dialog-centered">
	    	<div class="modal-content">
	      		<div class="modal-header">
	      			<h4 class="modal-title" id="titulo_accion"></h4>
	        		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			          <span aria-hidden="true">&times;</span>
			        </button>
	      		</div>
	      		<div class="modal-body">
	      			<h4 id="nombre_candidato"></h4><br>
	        		<p class="" id="motivo"></p><br>
	    		</div>
		    	<div class="modal-footer">
		      		<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		    	</div>
	  		</div>
		</div>
	</div>
	<section class="content-header">
      	<h1 class="titulo_seccion">Cliente<small><?php echo $cliente; ?></small></h1>
    	<a class="btn btn-app" id="btn_regresar" class="btn btn-app" data-toggle="modal" data-target="#newModal">
    		<i class="fas fa-plus-circle"></i> <span>Nuevo candidato</span>
  		</a>
    </section>
    
    <div class="loader" style="display: none;"></div>
    <section class="content" id="listado">
	  	<!-- Page Content -->
    <div class="container">

        <div class="row">
            <div class="col-lg-12 text-center">
                <div id="calendar" class="col-centered">
                </div>
            </div>
        </div>
        <!-- /.row -->
		
		<!-- Modal -->
		<div class="modal fade" id="ModalAdd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		  <div class="modal-dialog" role="document">
			<div class="modal-content">
			<form class="form-horizontal" method="POST" action="addEvent.php">
			
			  <div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Agregar Evento</h4>
			  </div>
			  <div class="modal-body">
				
				  <div class="form-group">
					<label for="title" class="col-sm-2 control-label">Titulo</label>
					<div class="col-sm-10">
					  <input type="text" name="title" class="form-control" id="title" placeholder="Titulo">
					</div>
				  </div>
				  <div class="form-group">
					<label for="color" class="col-sm-2 control-label">Color</label>
					<div class="col-sm-10">
					  <select name="color" class="form-control" id="color">
									  <option value="">Seleccionar</option>
						  <option style="color:#0071c5;" value="#0071c5">&#9724; Azul oscuro</option>
						  <option style="color:#40E0D0;" value="#40E0D0">&#9724; Turquesa</option>
						  <option style="color:#008000;" value="#008000">&#9724; Verde</option>						  
						  <option style="color:#FFD700;" value="#FFD700">&#9724; Amarillo</option>
						  <option style="color:#FF8C00;" value="#FF8C00">&#9724; Naranja</option>
						  <option style="color:#FF0000;" value="#FF0000">&#9724; Rojo</option>
						  <option style="color:#000;" value="#000">&#9724; Negro</option>
						  
						</select>
					</div>
				  </div>
				  <div class="form-group">
					<label for="start" class="col-sm-2 control-label">Fecha Inicial</label>
					<div class="col-sm-10">
					  <input type="text" name="start" class="form-control" id="start" >
					</div>
				  </div>
				  <div class="form-group">
					<label for="end" class="col-sm-2 control-label">Fecha Final</label>
					<div class="col-sm-10">
					  <input type="text" name="end" class="form-control" id="end" >
					</div>
				  </div>
				
			  </div>
			  <div class="modal-footer">
				<button type="button" class="btn btn-info" data-dismiss="modal">Cerrar</button>
				<button type="submit" class="btn btn-primary">Guardar</button>
			  </div>
			</form>
			</div>
		  </div>
		</div>
		
		
		
		<!-- Modal -->
		<div class="modal fade" id="ModalEdit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		  <div class="modal-dialog" role="document">
			<div class="modal-content">
			<form class="form-horizontal" method="POST" action="editEventTitle.php">
			  <div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Modificar Evento</h4>
			  </div>
			  <div class="modal-body">
				
				  <div class="form-group">
					<label for="title" class="col-sm-2 control-label">Titulo</label>
					<div class="col-sm-10">
					  <input type="text" name="title" class="form-control" id="title" placeholder="Titulo">
					</div>
				  </div>
				  <div class="form-group">
					<label for="color" class="col-sm-2 control-label">Color</label>
					<div class="col-sm-10">
					  <select name="color" class="form-control" id="color">
						  <option value="">Seleccionar</option>
						  <option style="color:#0071c5;" value="#0071c5">&#9724; Azul oscuro</option>
						  <option style="color:#40E0D0;" value="#40E0D0">&#9724; Turquesa</option>
						  <option style="color:#008000;" value="#008000">&#9724; Verde</option>						  
						  <option style="color:#FFD700;" value="#FFD700">&#9724; Amarillo</option>
						  <option style="color:#FF8C00;" value="#FF8C00">&#9724; Naranja</option>
						  <option style="color:#FF0000;" value="#FF0000">&#9724; Rojo</option>
						  <option style="color:#000;" value="#000">&#9724; Negro</option>
						  
						</select>
					</div>
				  </div>
				    <div class="form-group"> 
						<div class="col-sm-offset-2 col-sm-10">
						  <div class="checkbox">
							<label class="text-danger"><input type="checkbox"  name="delete"> Eliminar Evento</label>
						  </div>
						</div>
					</div>
				  
				  <input type="hidden" name="id" class="form-control" id="id">
				
				
			  </div>
			  <div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
				<button type="submit" class="btn btn-primary">Guardar</button>
			  </div>
			</form>
			</div>
		  </div>
		</div>

    </div>
    <!-- /.container -->
	</section>
	
</div>
<!-- /.content-wrapper -->	
<script>
	var id = '<?php echo $this->uri->segment(3) ?>';
	var url = '<?php echo base_url('Cliente/intelaGet?id='); ?>'+id;
  	$(document).ready(function(){
  		var date = new Date();
       	var yyyy = date.getFullYear().toString();
       	var mm = (date.getMonth()+1).toString().length == 1 ? "0"+(date.getMonth()+1).toString() : (date.getMonth()+1).toString();
       	var dd  = (date.getDate()).toString().length == 1 ? "0"+(date.getDate()).toString() : (date.getDate()).toString();
		
		$('#calendar').fullCalendar({
			plugins: [ 'timeGrid' ],
			header: {
				 language: 'es',
				left: 'prev,next today',
				center: 'title',
				right: 'month,basicWeek,basicDay',

			},
			defaultDate: yyyy+"-"+mm+"-"+dd,
			editable: true,
			eventLimit: true, // allow "more" link when too many events
			selectable: true,
			selectHelper: true,			
			select: function(start, end) {
				
				$('#ModalAdd #start').val(moment(start).format('YYYY-MM-DD HH:mm:ss'));
				$('#ModalAdd #end').val(moment(end).format('YYYY-MM-DD HH:mm:ss'));
				$('#ModalAdd').modal('show');
			},
			eventRender: function(event, element) {
				element.bind('dblclick', function() {
					$('#ModalEdit #id').val(event.id);
					$('#ModalEdit #title').val(event.title);
					$('#ModalEdit #color').val(event.color);
					$('#ModalEdit').modal('show');
				});
			},
			eventDrop: function(event, delta, revertFunc) { // si changement de position

				edit(event);

			},
			eventResize: function(event,dayDelta,minuteDelta,revertFunc) { // si changement de longueur

				edit(event);

			},
 			eventClick: function(event) {
 				window.open('https://www.google.com/maps/place/'+event.descripcion, '_blank');
			},
			events: [
			<?php foreach($visitas as $event): 
			
				$start = explode(" ", $event->fecha_inicio);
				$end = explode(" ", $event->fecha_fin);
				if($start[1] == '00:00:00'){
					$start = $start[0];
				}else{
					$start = $event->start;
				}
				if($end[1] == '00:00:00'){
					$end = $end[0];
				}else{
					$end = $event->end;
				}
			?>
				{
					id: '<?php echo $event->id; ?>',
					title: '<?php echo $event->descripcion; ?>',
					start: '<?php echo $start; ?>',
					end: '<?php echo $end; ?>',
					color: '<?php echo $event->color; ?>'
				},
			<?php endforeach; ?>
			]
		});
		
		function edit(event){
			start = event.start.format('YYYY-MM-DD HH:mm:ss');
			if(event.end){
				end = event.end.format('YYYY-MM-DD HH:mm:ss');
			}else{
				end = start;
			}
			
			id =  event.id;
			
			Event = [];
			Event[0] = id;
			Event[1] = start;
			Event[2] = end;
			
			$.ajax({
			 url: 'editEventDate.php',
			 type: "POST",
			 data: {Event:Event},
			 success: function(rep) {
					if(rep == 'OK'){
						alert('Evento se ha guardado correctamente');
					}else{
						alert('No se pudo guardar. Inténtalo de nuevo.'); 
					}
				}
			});
		}
  		
	    $("#fecha_nacimiento, #refLab1_entrada, #refLab1_salida, #refLab2_entrada, #refLab2_salida").datetimepicker({
	  		minView: 2,
	    	format: "dd/mm/yyyy",
	    	startView: 4,
	    	autoclose: true,
	    	todayHighlight: true,
	    	pickerPosition: "bottom-left",
	    	forceParse: false
  		});
  		$("#visitaModal").on("hidden.bs.modal", function(){
	    	$("input, select").val("");

	    });
	    $('[data-toggle="tooltip"]').tooltip();
	    $("#newModal").on("hidden.bs.modal", function(){
	      	$("input, select").removeClass("requerido");
	      	$("input, select").val("");
	      	$("#msj_puesto").css('display','none');
	      	
	    });
  	
	
	  	//Acepta solo numeros en los input
	  	$(".solo_numeros").on("input", function(){
		    var valor = $(this).val();
		    $(this).val(valor.replace(/[^0-9]/g, ''));
		});
		//Limpiar inputs
		$(".personal_obligado, .check_obligado, .familia_obligado, .estudios_obligado, #trabajo_inactivo, .refPer_obligado").focus(function(){
			$(this).removeClass("requerido");
		});
		//Se cambia el idioma al español;
		$.fn.datetimepicker.dates['en'] = {
		    days: ["Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado", "Domingo"],
		    daysShort: ["Dom", "Lun", "Mar", "Mie", "Jue", "Vie", "Sab", "Dom"],
		    daysMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa", "Do"],
		    months: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
		    monthsShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"],
		    meridiem: '',
		    today: "Hoy"
		};
	});
</script>